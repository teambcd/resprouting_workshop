##########################################################################################
## Transform species information in PFT vs RS types										##
##########################################################################################
##########################################################################################

##########################################################################################
## Set-up file locations and parameters													##
##########################################################################################
transform_Species_PFT_query_into_required_table_script_info <- function () {
	source("libs/g.test.r")
	source("libs/bar_plot_functions.r")
    
	## define table inputs
	filename_plants		 	<<- "data/RS_syndrome_for_woody_PFTs.csv"	
	filename_climate 		<<- 'outputs/Site_bioclimate.csv'
	life_table 				<<- 'pft_definitions/life_form.csv'
	leaf_table 				<<- 'pft_definitions/leaf_form.csv'
	phen_table 				<<- 'pft_definitions/phenology.csv'
	clmr_table 				<<- 'pft_definitions/climate_range.csv'
    
    ## output table
    filename_out			<<-'outputs/PFT_RS_fractions2.csv'
	
	## pft info ##
	ntraits 				<<- 5
    species_throshold 		<<- 10
    
    ## plot definitions ##
    legend_location 		<<- c('topmiddle')
    y_max 					<<- 100
    beside 					<<- TRUE
    
    ## Stat test definitions
    test_using_g_not_chi 	<<- TRUE
    test_using_got_not_toi	<<- FALSE
}


##########################################################################################
## Gubbins																				##
##########################################################################################
transform_Species_PFT_query_into_required_table <- function() {
	transform_Species_PFT_query_into_required_table_script_info()
    
    ## making sure the pft definitiosn cover all attributes in DB quiery
    update_pft_definitions()
	
	## load plant breadown and climate tables
	pdata=read.csv(filename_plants)
	
	## find pft abundances
	cdata=find_pft_abundances(pdata)
    
    ##Write trait abundances for each pft into csv
    write_table(cdata)

	##Remove PFTS with less that the threshold value of species.
	cdata=remome_unwanted_pfts(cdata,species_throshold)
	
	## perform the stats test to determine if pfts RS proportions are different.
	compare_pfts_(cdata)

	## Sandy doesn't want RS any more...
    cdata=cdata[-which(rownames(cdata) %in% c("non-resprouter")),]
    
    mymat=t(cut_but_maintain_data_frame(cdata))
	#colnames(mymat)[4]="Under-\nground"
	#colnames(mymat)[3]="Basal/\nCollar"
	#colnames(mymat)[2]="Epi-\ncormic"
	mar=par("mar")
	mar[4]=9
	plot_barchart(t(mymat),beside=beside,ymax=y_max,axes=TRUE,mar=mar)

	## Clean up after itself and spit out the % traits used in the plot.
	rm(list=setdiff(ls(), "cdata"))
	return(cdata)
	
	
}
##########################################################################################
## Lovely functions																		##
##########################################################################################
##--------------------------------------------------------------------------------------##
## PFT definitions are stores in the csv tables whose paths are at the top of the 		##
## script. This makes sure all the species are listed in those tables, and allows you	##
## to input new species via the R-shell 												##
##--------------------------------------------------------------------------------------##
update_pft_definitions <- function () {
    pdata=read.csv(filename_plants)
    
    life_choice=unique(pdata$Life.form)
    leaf_choice=unique(pdata$Leaf.form)
    phen_choice=unique(pdata$Phenology)
    clim_choice=unique(pdata$Climatic.Range)
    
    add_to_table_unclassified_traits(life_choice,life_table)
    add_to_table_unclassified_traits(leaf_choice,leaf_table)
    add_to_table_unclassified_traits(phen_choice,phen_table)
    add_to_table_unclassified_traits(clim_choice,clmr_table)
    
}
## Used above. Adds unknown species to the csv files via shell input. Probably could do
## with some tidying
add_to_table_unclassified_traits <- function(attribute,filename_lookup) {
    
    lookup <<- read.table(filename_lookup,
        sep=",",header=TRUE,stringsAsFactors = FALSE)
    lookup_old=lookup
    
    choices=colnames(lookup)[-1]
    
    nchoices=0
    choices_display=""
    for (i in choices) {
        nchoices=nchoices+1
        choices_display=paste(choices_display,nchoices,".","\t",i,"\n")
    }
    
    ## initial classification
    l=dim(lookup)
    s=length(attribute)
    
    classified=rep(FALSE,s)    
    
    for (i in 1:l[1]) {     
        found=attribute==lookup[i,1]
        classified[found]=TRUE
    }    
    
    ## classify the rest
    indexs=which(classified==FALSE)    
    indexs=unique(attribute[indexs])
    
    p=l[1];
    for (i in indexs) {
        if (is.na(i) || i=='NA') {
            next
        }
        p=p+1
        lookup[p,1]=i
        lookup[p,2:(nchoices+1)]=0
        test_more=TRUE
        
        print(paste("what classification is: ",i,"?",sep=""))  
        cat(choices_display)        
        
        while(test_more) {
            no=as.numeric(readline(
                "enter corrisponding number(0 for no more): "))
                        
            no-test_number_enterted(no,nchoices,0)
            if (no==0) {
                test_more=FALSE
            } else {
                lookup[p,no+1]=1
            }
        }
    }
    
    write.csv(lookup,filename_lookup,row.names=FALSE)
}

test_number_enterted <-function(no,maxno,minno) {
    test=TRUE
    
    while (test) {
        if (is.numeric(no)==FALSE) {
            no=readline("not a number. try again: ")
        } else if (no>maxno) {
            no=readline("number too large. try again: ")
        } else if (no<minno) {
            no=readline("number too small. try again: ")
        } else {
            test=FALSE
        }
    }
    return(no)
}
##--------------------------------------------------------------------------------------##
## Takes the raw abundance data and groups it into % of trait for each PFT defined in	##
## them csv tables again																##
##--------------------------------------------------------------------------------------##
find_pft_abundances <- function(pdata) {
	
	pftn=convert_plant_table_into_pfts(pdata)
	npfts=length(colnames(life[1,2:dim(life)[2]]))-1

	cdata=data.frame(matrix(NA, nrow = ntraits+1, ncol = npfts+1))
	colnames(cdata)=colnames(life[1,2:dim(life)[2]])
	rownames(cdata)=c(colnames(pdata[1,3:7]),'total')
	
	for (pft in 1:npfts) {
		test=(pftn==pft)*1
		test[which(is.na(test))]=0		
      
		for (trait in 1:ntraits) {
			cdata[trait,pft]=sum(test*pdata[,trait+2],na.rm=TRUE)
		}
        
		total=sum(test,na.rm=TRUE)
		if (total>0) {
			cdata[,pft]=100*cdata[,pft]/total
			cdata[ntraits+1,pft]=total
		}
	}
	
	return(cdata)
}

convert_plant_table_into_pfts <-function(pdata) {

	life <<- read.csv(life_table)	
	leaf=read.csv(leaf_table)
	phen=read.csv(phen_table)
	clmr=read.csv(clmr_table)	
	npfts=dim(life)[2]
    
	a1=match(pdata$Life.form,life[,1])
	a2=match(pdata$Leaf.form,leaf[,1])
	a3=match(pdata$Phenology,phen[,1])
	a4=match(pdata$Climatic.Range,clmr[,1])
	
	npoints=length(a1)
	
	pfts=matrix(0,npoints)
	pftsA=matrix(0,npoints)
	pftsB=matrix(0,npoints)
    
	for (pft in 1:(npfts-1)) {
		pfti=pft+1
        print(life[1,pfti])
        if (life[1,pfti]==1) {
        
            test=life[a1,pfti]+leaf[a2,pfti]+phen[a3,pfti]+clmr[a4,pfti]
            pftsA[which(test==4)]=pft
        } else {
            test=life[a1,pfti]
            pftsB[which(test==1)]=pft
        }
	}
	
    pfts[pftsB>0]=pftsB[pftsB>0]
    pfts[pftsA>0]=pftsA[pftsA>0]
	return(pfts)	
}
##--------------------------------------------------------------------------------------##
## writing all % RS types per pft to csvs												##
##--------------------------------------------------------------------------------------##
write_table <- function(cdata) {
    cdata=reorder_pfts_into_correct_order_for_plotting(cdata,0)
    write.csv(cdata[[1]],output_table_name('-woody'))	
	write.csv(cdata[[2]],output_table_name('-herb'))
    
}
##--------------------------------------------------------------------------------------##
## Removes unwanted pfts and															##
##	pfts with less than the set number of species in the database						##
##--------------------------------------------------------------------------------------##
remome_unwanted_pfts <- function(cdata,threshold=0) {
	
	rowname_trees=c('Apical', 'Epicorminc','Basal/Collar',
					   'Underground','non-resprouter','total')

	# tree data	
	cdata=data.frame(
		TBE=cdata$TBE,
		TBD=cdata$TBD,
		tNE=cdata$tNE,
		tBE=cdata$tBE,
		tBD=cdata$tBD,
        "tuft"=cdata$tuft.trees, 
		"scaled"=cdata$scaled.tree,
        "shrub"=cdata$shrub,
		"dwarf"=cdata$dwarf.shrub,check.names=FALSE)
	rownames(cdata)=rowname_trees
    
    cdata=remove_pfts_with_less_than_n_species(cdata,threshold)

	return(cdata)
    
}
## As on the tin. Used in function above
remove_pfts_with_less_than_n_species <- function(cdata,nt=0) {

    rnames=rownames(cdata)
    
    tots=cdata['total',]
    tots[is.na(tots)]=0
 
    
    drops=colnames(cdata)[tots<nt]
    
    dropping="dropping: "
    totals="which had: "
    for (i in drops) {
        dropping=paste(dropping,i,"; ",sep="")
        totals=paste(totals,tots[[i]],"; ",sep="")
    }
    totals=paste(totals,"species respectivly")
    
    print(dropping)
    print(totals)    
    
    keeps=colnames(cdata)[tots>=nt]
    cdata=cdata[,(names(cdata) %in% keeps)]

    if (is.null(dim(cdata))) {
        cdata=data.frame(cdata)
        colnames(cdata)=keeps
        rownames(cdata)=rnames        
        cdata$.=NaN
    }
    
    return(cdata)    
}
##--------------------------------------------------------------------------------------##
## Peforms either g-test or chi-squared test to see if pfts RS trait % are different	##
##--------------------------------------------------------------------------------------##
compare_pfts_ <- function(cdata) {
    PFTs=colnames(cdata)
    npft=length(PFTs)
    ntypes=length(cdata)[1]-2
    types=rownames(cdata)[1:ntypes]
    
    pvs=matrix(0,npft*ntypes,npft)

    rownames(pvs)=paste(rep(PFTs,each=ntypes),rep(types,ntypes))
    colnames(pvs)=PFTs
    
    nr=0
	for (i in PFTs) {
		for (type in 1:ntypes) {
			nr=nr+1
			for (j in PFTs) {
				#fraction of trees with RS type multiplied by total number of trees
				X=find_g_test_X_or_Y(cdata[[i]],ntypes,type)
				Y=find_g_test_X_or_Y(cdata[[j]],ntypes,type)

				test_matrix=cbind(X,Y)

				if (test_using_g_not_chi) {
					if (test_using_got_not_toi) {
						pvs[nr,j]=g.test(X,p=Y)$p.value
					} else {
						pvs[nr,j]=g.test(test_matrix)$p.value
					}
				} else {      
					if (test_using_got_not_toi) {
						pvs[nr,j]=chisq.test(X,Y)$p.value
					} else {          
						test_matrix[rowSums(test_matrix)==0,1]=0.0001
						pvs[nr,j]=chisq.test(test_matrix)$p.value
					}
				}
			}
		}
    }
    write.csv(pvs,output_table_name('-liklihoods'))
    
}

find_g_test_X_or_Y <- function(cdata,ntypes,type) {
	X=cdata[1:ntypes]*cdata[length(cdata)]/100
	X=c(X[type],sum(X[c(1:type,type:ntypes)]))
	X[is.na(X)]=0
	return(X)
}
##--------------------------------------------------------------------------------------##
## Get things ready for bar-plotting													##
##--------------------------------------------------------------------------------------##
cut_but_maintain_data_frame <- function(cdata) {
    cnames=colnames(cdata)
    rnames=rownames(cdata)
    
    a=dim(cdata)
    
    cdata=cdata[1:a[1]-1,]
    if (is.null(dim(cdata))) {
        cdata=data.frame(cdata)
        colnames(cdata)=cnames
        rownames(cdata)=rnames[1:a[1]-1]
    }
    
    return(cdata)
}

##########################################################################################
## Not used at the munite but still a lovely functions									##
##########################################################################################
remove_missing_info <- function(pdata) {

    test=(is.na(pdata$Resprouter))+(is.na(pdata$Non.resprouter))    
    pdata=pdata[which(test!=2),]
    
    test=(is.na(pdata$Resprouter))+(pdata$Non.resprouter==0)
    pdata=pdata[which(test!=2),]
    
    test=(is.na(pdata$Non.resprouter))+(pdata$Resprouter==0)
    pdata=pdata[which(test!=2),]
    
    test=(pdata$Resprouter!=pdata$Non.resprouter)
    pdata=pdata[which(test),]

    pdatai=pdata
    for (i in 3:6) {
        pdatai[which(is.na(pdata[,i])),i]=0
    }
    
    test=(pdatai$Resprouter==1)+
        (pdatai$Apical.resprouter==0)+
        (pdatai$Epicormic.resprouter==0)+
        (pdatai$Basal.collar.resprouter==0)+
        (pdatai$Underground.resprouter==0)
    pdata=pdata[which(test!=5),]
    return(pdata)
   
}