script_info <- function () {
	
	source("libs/bar_plot_functions.r")
	source("plotting/Group_Species_and_RS2PFT.r")
	source('libs/return_multiple_from_functions.r')

  	rm(list=ls())
  	## define table inputs
  	filename_plants <<- 'data/RSvsNR_no_annuals.csv'	
  	life_table <<- 'pft_definitions/life_form.csv'
  	leaf_table <<- 'pft_definitions/leaf_form.csv'
  	phen_table <<- 'pft_definitions/phenology.csv'
  	clmr_table <<- 'pft_definitions/climate_range.csv'
  
  	## output table
	filename_out<<-'outputs/PFT_RS_fractions-herbs.csv'

	## pft info ##
	species_throshold <<- 10

	## plot definitions ##
	legend_location <<- c('topright','topright')
	test_using_g_to_chi<<-TRUE
}

fill_pft <- function (pft,pftn,total,cdata,pdata) {
	test=(pftn==pft)

	total[pft]=sum(test,na.rm=TRUE)
	cdata[1,pft]=sum(pdata$Resprouter[test])/total[pft]
	cdata[2,pft]=sum(pdata$Resprouter[test]==0)/total[pft]
	cdata[3,pft]=total[pft]
	
	return(list(cdata,total))
}

Group_Species_and_RS2PFT <- function()
{	
	## contain required information t run these functions##
	script_info()

	## making sure the pft definitiosn cover all attributes in DB quiery
	update_pft_definitions()

	## load plant breadown and climate tables
	pdata=read.csv(filename_plants)

	## find pft abundances
	c(pftn,grass_annual):=convert_plant_table_into_pfts(pdata)
	npfts=length(colnames(life[1,2:dim(life)[2]]))-1

	cdata=data.frame(matrix(NA, nrow = 3, ncol = npfts+3))
	colnames(cdata)=c(colnames(life[1,2:dim(life)[2]]),'annual.grass','perennial.grass')
	rownames(cdata)=c('RS','NR','total')
	
	total=data.frame(t(rep(0,npfts+3)))
	colnames(total)=colnames(cdata)
  
	for (pft in 1:npfts) {
		c(cdata,total):=fill_pft(pft,pftn,total,cdata,pdata)
	}

	c(cdata,total):=fill_pft(pft+2,grass_annual,total,cdata,pdata)
	c(cdata,total):=fill_pft(pft+3,grass_annual,total,cdata,pdata)
	

	##Write tables
	write_table(cdata)
	compare_pfts(cdata,total,species_throshold)


	cdata=reorder_pfts_into_correct_order_for_plotting(cdata,species_throshold)####
  ##===========================================================##
  ## Plot   

	mymat=as.matrix(cdata[1:2,])    

	#mymat=mymat[,1:13]

	
	mar=mar0=par("mar")
    mar[4]=0
    par(mar=mar) 

  	barplot(mymat*100,col=c("#333333","#DDDDDD"),
            legend.text=rownames(mymat),las=1,
            args.legend=list(x=1,y=130,bty='n'),
            ylim=c(0,100),axes = TRUE,cex.names = par("cex.axis"))
	mtext("(a)",side=3,adj=0,cex=1.5*par("cex"),line=1)
    mar=par("mar")
    mar[2]=0
    par(mar=mar)        
	plot.new()

	labs=c(' resprouter',"",'non-resprouter')
	legend(x="topright",legend=labs, col=c("#000000","white","#000000"),
         pch=22,horiz = FALSE,ncol=1,bty="n",xpd=TRUE,pt.cex=1.85*par("cex"),cex=par("cex"))
	legend(x="topright",legend=labs, col=c("#333333","white","#DDDDDD"),
         pch=15,horiz = FALSE,ncol=1,bty="n",xpd=TRUE,pt.cex=1.6*par("cex"),cex=par("cex"))

  par(mar=mar0)
  	#plot_barchart((mymat*100),beside=FALSE,figname="figs/fig4a.pdf",fwidth=8,
	#	fheight=5.5,ymax=100,axes=TRUE,mar=c(3,3.5,1,5),cex_all=0.75,legendX=15,cex_legend=(1/0.75))
		
		
  	return(cdata)
}

update_pft_definitions <- function () {
	  pdata=read.csv(filename_plants)

	  life_choice=unique(pdata$Life.form)
	  leaf_choice=unique(pdata$Leaf.form)
	  phen_choice=unique(pdata$Phenology)
	  clim_choice=unique(pdata$Climatic.Range)
  
	  add_to_table_unclassified_traits(life_choice,life_table)
	  add_to_table_unclassified_traits(leaf_choice,leaf_table)
	  add_to_table_unclassified_traits(phen_choice,phen_table)
	  add_to_table_unclassified_traits(clim_choice,clmr_table)
}


add_to_table_unclassified_traits <- function(attribute,filename_lookup) {
  
	lookup <<- read.table(filename_lookup,
						sep=",",header=TRUE,stringsAsFactors = FALSE)
	lookup_old=lookup

	choices=colnames(lookup)[-1]

	nchoices=0
	choices_display=""
	for (i in choices) {
    	nchoices=nchoices+1
    	choices_display=paste(choices_display,nchoices,".","\t",i,"\n")
  	}
  
	## initial classification
	l=dim(lookup)
	s=length(attribute)

	classified=rep(FALSE,s)    

	for (i in 1:l[1]) {     
		found=attribute==lookup[i,1]
		classified[found]=TRUE
	}    
  
  ## classify the rest
	indexs=which(classified==FALSE)    
	indexs=unique(attribute[indexs])

	p=l[1];
	for (i in indexs) {
    	if (is.na(i) || i=='NA') {
      		next
    }
    
    p=p+1
    lookup[p,1]=i
    lookup[p,2:(nchoices+1)]=0
    test_more=TRUE
    
    print(paste("what classification is: ",i,"?",sep=""))  
    cat(choices_display)        
    
    while(test_more) {
      	no=as.numeric(readline(
        	"enter corrisponding number(0 for no more): "))
      
      	no-test_number_enterted(no,nchoices,0)
      	if (no==0) {
        	test_more=FALSE
      	} else {
        	lookup[p,no+1]=1
      	}
    	}
  	}
  
  	write.csv(lookup,filename_lookup,row.names=FALSE)
}

test_number_enterted <-function(no,maxno,minno) {
	test=TRUE

	while (test) {
		if (is.numeric(no)==FALSE) {
			no=readline("not a number. try again: ")
		} else if (no>maxno) {
			no=readline("number too large. try again: ")
		} else if (no<minno) {
			no=readline("number too small. try again: ")
		} else {
			test=FALSE
		}
  	}
  
  	return(no)
}

convert_plant_table_into_pfts <-function(pdata) {
  
	life 	<<- read.csv(life_table)	
	leaf	=	read.csv(leaf_table)
	phen	=	read.csv(phen_table)
	clmr	=	read.csv(clmr_table)	
	npfts	=	dim(life)[2]
	
	a1		=	match(pdata$Life.form,life[,1])
	a2		=	match(pdata$Leaf.form,leaf[,1])
	a3		=	match(pdata$Phenology,phen[,1])
	a4		=	match(pdata$Climatic.Range,clmr[,1])
	
	npoints	=	length(a1)
	
	pfts	=	matrix(0,npoints)
	pftsA	=	matrix(0,npoints)
	pftsB	=	matrix(0,npoints)
  	
  	grass_annual = matrix(0,npoints)
	for (pft in 1:(npfts-1)) {
		pfti=pft+1
		print(life[1,pfti])
		if (life[1,pfti]==1) {
	  		test=life[a1,pfti]+leaf[a2,pfti]+phen[a3,pfti]+clmr[a4,pfti]
	  		pftsA[which(test==4)]=pft
		} else {
	  		test=life[a1,pfti]
	  		pftsB[which(test==1)]=pft
		}
	}
	
	test=pdata$Grass.perenniality=='Annual'
	grass_annual[test]=pft+1
	
	test=pdata$Grass.perenniality=='Perennial'
	grass_annual[test]=pft+2
  	
	pfts[pftsB>0]=pftsB[pftsB>0]
	pfts[pftsA>0]=pftsA[pftsA>0]
	##test if to many pfts
	return(list(pfts,grass_annual))
  
}

reorder_pfts_into_correct_order_for_plotting <- function(cdata,threshold=0) {

	cdata=data.frame(
		TBE=cdata$TBE,
		TBD=cdata$TBD,
		tNE=cdata$tNE,
		tBE=cdata$tBE,
		tBD=cdata$tBD,
		"tuft"=cdata$tuft.trees, 
		"scale"=cdata$scaled.tree,
		"shrub"=cdata$shrub,
		"dwarf"=cdata$dwarf.shrub,
		"C4"=cdata$C4.grass,
		"C3"=cdata$C3.grass,
		"forb"=cdata$perennial.forb,
		"annual\nforb"=cdata$other.herb,
		#"annual\ngrass"=cdata$annual.grass,
		#"perennial\ngrass"=cdata$perennial.grass,
		check.names=FALSE)

	rownames(cdata)=c('RS','NR','total')

	cdata=remove_pfts_with_less_than_n_species(cdata,threshold)
	return(cdata)
  
}

remove_pfts_with_less_than_n_species <- function(cdata,nt=0) {
  
	rnames=rownames(cdata)

	tots=cdata['total',]
	tots[is.na(tots)]=0


	drops=colnames(cdata)[tots<nt]

	dropping="dropping: "
	totals="which had: "
	for (i in drops) {
		dropping=paste(dropping,i,"; ",sep="")
    	totals=paste(totals,tots[[i]],"; ",sep="")
  	}
	totals=paste(totals,"species respectivly")

	print(dropping)
	print(totals)    

	keeps=colnames(cdata)[tots>=nt]
	cdata=cdata[,(names(cdata) %in% keeps)]

	if (is.null(dim(cdata))) {
		cdata=data.frame(cdata)
		colnames(cdata)=keeps
		rownames(cdata)=rnames        
		cdata$.=NaN
  	}
  
  	return(cdata)    
}

cut_but_maintain_data_frame <- function(cdata) {

	cnames=colnames(cdata)
	rnames=rownames(cdata)

	a=dim(cdata)

	cdata=cdata[1:a[1]-1,]
	if (is.null(dim(cdata))) {
		cdata=data.frame(cdata)
		colnames(cdata)=cnames
		rownames(cdata)=rnames[1:a[1]-1]
  	}
  
  	return(cdata)
}



compare_pfts <- function(cdata,total,threshold=0) {
	source("libs/drop_end.r")
	source("libs/g.test.r")
	cdata=remove_pfts_with_less_than_n_species(cdata,threshold)
    PFTs=colnames(cdata)
    npft=length(PFTs)

    pvs=matrix(0,npft,npft)
    rownames(pvs)=PFTs
    colnames(pvs)=PFTs
    cdata[is.na(cdata)]=0
    
    for (i in PFTs) {
        for (j in PFTs) {
            X=drop_end(cdata[[i]])*as.integer(total[i])
            Y=drop_end(cdata[[j]])*as.integer(total[j])

            #if ((sum(X==0)+sum(Y==0))>0) {
            
            	#pvs[i,j]=NaN
            #} else {
            	tbl=cbind(X,Y)
            	
            	if (test_using_g_to_chi) {
            		
                	pvs[i,j]=g.test(tbl)$p.value
            	} else {                
                	#test_matrix[rowSums(test_matrix)==0,1]=0.0001
                	pvs[i,j]=chisq.test(tbl)$p.value
                }
            #}
        }
    }
    write.csv(pvs,output_table_name('-liklihoods'))
    
}
###################################################################
## Scripts for plotting                                          ##
###################################################################
calculate_width_or_plots <- function(cdata) {   
	plot_widths=c()
	a=length(cdata)
	for (i in 1:a) {    
    	plot_widths=c(plot_widths,dim(cdata[[i]])[2])
  	}
  
	plot_widths[2]=plot_widths[2]*0.6
	nbars=sum(plot_widths)
	plot_widths=plot_widths/nbars
	plot_widths[a+1]=0.1

	return(plot_widths)
  
}

setup_plot_layout <-function(a,plot_widths) {
  
	plot_areas=matrix(c(1:a,0),1,a+1,byrow=TRUE)
	layout(plot_areas,widths=plot_widths,heights=c(1,1,1,1))
	par(mar=c(3,2,1,0))
  
}


write_table <- function(cdata) {

  	write.csv(cdata,output_table_name('-all'))	
  
}

output_table_name <- function(string) {
  	filename= paste(substr(filename_out,1,nchar(filename_out)-4), string,
                  substr(filename_out,nchar(filename_out)-3,nchar(filename_out)),sep="")
  
  	return(filename)
}
