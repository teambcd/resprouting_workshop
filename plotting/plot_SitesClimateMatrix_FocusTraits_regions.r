plot_SitesClimateMatrix_FocusTraits_regions <-function() {
	source("plotting/plot_SitesClimateMatrix_FocusTraits.r")
	LocVars					<<-	c(	'lat','lon')


	lon_range				<<-	matrix(c(
								112.5,160, 	# All Australia
    							128,133.2,    	# North region
    							148.5,152,     	# East region
    							114.5,119),  	# West region            
    							2)
    
	lat_range 				<<- matrix(c(
								-44,-10.75,  	# All Australia
    							-14.5,-12, 		# North region
    							-37.5,-32, 		# East region
    							-35.5,-29),  	# West region
     							2)
     							
    region_name=c('Aus','NT','NSW','WA')

	 
	for (i in 1:dim(lon_range)[2]) 
		plot_SitesClimateMatrix_FocusTraits(lon_range[,i],lat_range[,i],region_name[i])
     							
}
