source("plotting/plot_ClimateMatrix_FocusSites.R")

filename_plot	<<- 'figs/climate_space2'

xy_held			<<-FALSE

## Define what climate variables want plotting and how they should be labelled
xClimVars 		<<- matrix(c('MAT','PP'),2,1)
yClimVars 		<<- matrix(c('MAP','PC'),2,1)
xClimVars_labs 	<<- list(	expression(paste("MAT (",degree,"C)",sep="")))
yClimVars_labs 	<<- list(	"MAP (mm/year)")
ClimVars_labs	<<-matrix(c(expression(paste("fAPAR"[Phi]," vs. ","fAPAR"[Theta],sep="")),
				 		    expression(paste("Pr"[Phi]," vs. ","Pr"[Theta],sep=""))),2,1)

## All of these are polar				 		    
xClimVars_polar <<- matrix(c(FALSE,TRUE),2,1)
cex_general		<<- 1.33

oma				<<- c(0.5, 4, 4,0.5)
plot_ClimateMatrix_FocusSites()