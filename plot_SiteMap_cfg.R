##############################################################################
## script set up                                                           ##
##############################################################################
## libraries ##
source("plotting/plot_topomaps_with_regions.R")

## define inputs ##  
Filenames_in 	<<- c(	'outputs/Site_RS_abundance.csv')

Filenames_out 	<<- c(	'SiteMap.png')


## define plots outputs ##
fig_path 		<<- 	'figs/'

     
## which traits need plotting?
traits			<<- c(	'MAP')     
## limits of file 1 for each attrbite
## Abundances
limits1			<- list(1) # Resprouter
limits_all		<<-list(limits1)
units_label		<<-	c('')
col_choices		<<- c( 'red')

nplots_across 	<<- 	1
top_blank 		<<- 	FALSE    

    
# choose the colors here to indicate five levels of abundance for traits
plot_random 	<<- FALSE
plot_density 	<<- FALSE
cex_scaling		<<- 0.2
test_draw_box	<<- FALSE

desaturate_map_colours  <<- FALSE
test_abundance_gradient <<- FALSE

plot_topomaps_with_regions()

alts=c(0,100,200,300,400,600,800,1000,1200)
nalts=length(alts)
terrain_cols=terrain_hcl(10)

par(mar=c(10,30,10,30))
#image(x=matrix(1:nalts,nalts,1),y=1,z=matrix(1:nalts,nalts,1),col=terrain_cols[1:nalts],xaxt='n',yaxt='n',xlab='',ylab='')
image(y=matrix(1:nalts,nalts,1),x=1,z=matrix(1:nalts,1,nalts),col=terrain_cols[1:nalts],xaxt='n',yaxt='n',xlab='',ylab='')
polygon(c(0.6,1.4,1.4,0.6),c(.5,.5,nalts+.5,nalts+.5),lwd=cex[1],xpd=TRUE)
text(y=(1:(nalts+1))-.5,x=rep(1.45,nalts),c(alts,paste(alts[nalts],'+')),xpd=TRUE,cex=cex[1],adj=0)
text(x=0.3,y=nalts/2,"altitude (m)",cex=cex[1]*1.2,xpd=TRUE,srt=90)

dev.off()