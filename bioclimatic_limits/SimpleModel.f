	      program Simple Model NPP and Rh
              implicit none

*************************************************************
* A programme to calculated NPP and Rh based on a simple model
* built by Knorr et al. (1995)
* NPP is effected by fAPAR,PAR,light use efficiency (epsilon),
* and alpha
* Rh is effected by a ratio(beta), temperature-dependent Q10, 
* alpha
*************************************************************

* define variables used

	integer	ll,i,m,maxgrd
	real	fAPAR(12),PAR(12),alpha(12),temp(12),lat,lon,epsilon,beta
	real	NPPC3(12),annNPPC3
    real    epsilonC3
	real	xlon,xlat

* unit for epsilon is mol C/ mol photon

	parameter(maxgrd=571290)
	parameter(epsilonC3=0.085*0.5) !0.5 is roughly the ratio of GPP:NPP

	real ci,gammastar25,k
	real gammastar(12)
	parameter(ci=0.8*390)
	parameter(gammastar25=42.75,k=0.0512)


* call and open input data (lon, lat, fAPAR/PAR/alpha/temperature data for each month)

	open(10,file='data/temp.txt',status='old')
	open(11,file='data/fAPAR.txt',status='old')
	open(12,file='outputs/mPAR_0C.txt',status='old')
	open(13,file='outputs/malpha.txt',status='old')	

* open output file
	
	open(20,file='outputs/mNPP.txt',status='unknown')
	open(21,file='outputs/aNPP.txt',status='unknown')


*************************** process for each grid

	do 100 ll=1,maxgrd

*		set values as zero

		annNPPC3=0.
		annNPPC4=0.

* read input data 
* fAPAR is taken by EVI in this study 

		read(10,*) lat,lon,(temp(i),i=1,12)
		read(11,*) lat,lon,(fAPAR(i),i=1,12)
		read(13,*) xlat,xlon,(alpha(i),i=1,12)
		read(12,*) xlat,xlon,(PAR(i),i=1,12)

* calculated monthly NPP and RH for C3 and C4 plants, and sum the annual value as well
* Unit for PAR is mol photon/m2/year, and unit for NPP is gC/year

		do m=1,12
			if(temp(m).lt.-99.0.or.fAPAR(m).lt.0.0.or.alpha(m).lt.
     %			-99.0.or.PAR(m).lt.-99.0)then
				NPPC3(m)=-999.9
				annNPPC3=-999.9
			else
				gammastar(m)=gammastar25*exp(k*(temp(m)-25))

				NPPC3(m)=epsilonC3*fAPAR(m)*PAR(m)*alpha(m)*12
     %				   *((ci-gammastar(m))/(ci+2*gammastar(m)))

				annNPPC3=annNPPC3+NPPC3(m)
				
		    endif
          enddo

* write the data into output file

	write(20,123)xlat,xlon,(NPPC3(i),i=1,12),annNPPC3
	write(22,123)xlat,xlon,annNPPC3

123	format(2f9.3,13f10.2)

100	continue


	end

