
common_attributes <- function()
{
    source("../libs/put_raster_in_netcdf.r")
    source("../libs/convert_xyz_to_raster.r")
    
    convertion_descp <<- "Bioclimatic limits calculated by Wang Han, 
        using 'main' and 'Bioclimate' in /bioclimatic_limits directory from this repo. 
        Converted from csv to netcdf for Doug Kelley"
}
    

convert_GDD0 <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="outputs/GDD0.txt"
    
    # Define output file
	filename_out="outputs/GDD0.nc"
    varname="GDD0"
    units='degree C'
    long_name="growing degree days at base 0"
    data_description="GDD climatology"
    data_source="calculated by Bioclimate.f in bitbucket repo:
        Resprouting_workshop/bioclimatic_limits,
        which calculates GDD from ANUCLIM
        monthly temp climateology, Jan 1970 -Dec 1999"
    
    cdata=convert_xyz_to_raster(filename_in)

    put_raster_in_netcdf(cdata,filename=filename_out,
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,units=units,
                                data_description=data_description)    
}


convert_MI <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="outputs/MI.txt"
    
    # Define output file
	filename_out="outputs/Moisture_index.nc"
    varname="MI"
    long_name="moisture index"
    data_description="mean annual precipitaion/equilibrum
        evapotranspiration"
    data_source="calculated by Bioclimate.f in bitbucket repo:
        Resprouting_workshop/bioclimatic_limits,
        which calculates GDD from ANUCLIM
        monthly temp, fractional sunshine hours and precipitation
        climateology, Jan 1970 -Dec 1999"
    
    cdata=convert_xyz_to_raster(filename_in)

    put_raster_in_netcdf(cdata,filename=filename_out,
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,
                                data_description=data_description)    
}
  
convert_PAR0 <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="outputs/PAR0.txt"
    
    # Define output file
	filename_out="outputs/PAR0.nc"
    varname="PAR0"
    long_name="photosythetically active radiation"
    data_description="par during growing season i.e. on days with temp>0"
    data_source="calculated by Bioclimate.f in bitbucket repo:
        Resprouting_workshop/bioclimatic_limits,
        which calculates GDD from ANUCLIM
        monthly temp, fractional sunshine hours and precipitation
        climateology, Jan 1970 -Dec 1999"
    
    cdata=convert_xyz_to_raster(filename_in)

    put_raster_in_netcdf(cdata,filename=filename_out,
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,
                                data_description=data_description)    
}  

convert_annual_alpha <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="outputs/yralpha.txt"
    
    # Define output file
	filename_out="outputs/alpha_annual.nc"
    varname="alpha"
    long_name="alpha"
    data_description="Actual evapotranspiration/equilibrum
        evapotranspiration"
    data_source="calculated by Bioclimate.f in bitbucket repo:
        Resprouting_workshop/bioclimatic_limits,
        which calculates GDD from ANUCLIM
        monthly temp, fractional sunshine hours and precipitation
        climateology, Jan 1970 -Dec 1999"
    
    cdata=convert_xyz_to_raster(filename_in)

    put_raster_in_netcdf(cdata,filename=filename_out,
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,
                                data_description=data_description)    
}        
        
convert_alpha <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="outputs/monthly_alpha.txt"
    
    # Define output file
	filename_out="outputs/alpha"
    varname="alpha"
    long_name="alpha"
    data_description="Actual evapotranspiration/equilibrum
        evapotranspiration"
    data_source="calculated by Bioclimate.f in bitbucket repo:
        Resprouting_workshop/bioclimatic_limits,
        which calculates GDD from ANUCLIM
        monthly temp, fractional sunshine hours and precipitation
        climateology, Jan 1970 -Dec 1999"
    
    for (i in c('min','max')) {
    	cdata=convert_xyz_to_raster(filename_in,annual_or_seasonal=i)

    	put_raster_in_netcdf_basic(cdata,filename=paste(filename_out,"-",i,".nc",sep=""),
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,
                                data_description=data_description)   
    } 
    i='conc'
    cdata=convert_xyz_to_raster(filename_in,annual_or_seasonal=i)

    put_raster_in_netcdf_basic(cdata[[1]],filename=paste(filename_out,"-","phase",".nc",sep=""),
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,
                                data_description=data_description)
    put_raster_in_netcdf_basic(cdata[[2]],filename=paste(filename_out,"-","conc",".nc",sep=""),
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,
                                data_description=data_description)     
}

convert_NPP <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="outputs/NPP.txt"
    
    # Define output file
	filename_out="outputs/NPP.nc"
    varname="NPP"
    long_name="Net Primary Production"
    data_description="Simulated NPP fromthe C3 component of Wang Hans SDBM"
    data_source="calculated by SimpleModel.f in bitbucket repo:
        Resprouting_workshop/bioclimatic_limits,
        which simulated NPP from climetologies of: ANUCLIM
        monthly temp, Jan 1970 -Dec 1999; EVI monthly
		fapar; and monthly PAR0 and alpha from 
		Respouting_workshop/bioclimatic_limits/Bioclimate.f"
    
    cdata=convert_xyz_to_raster(filename_in,
		annual_or_seasonal='annual')

    put_raster_in_netcdf(cdata,filename=filename_out,
                                varname=varname,
                                data_source=data_source,
                                convertion_descp=convertion_descp,
                                longname=long_name,
                                data_description=data_description)    
}        
    