close all
clear all

%% Common attributes
missing_value=-999;
addpath ../libs/

%% Open and plot GDD0
filename='outputs/GDD0.nc';
varname='GDD0';
plot_names='figs/GDD0';
limits=[0  5000:1000:10000];
colours={'brown'};


open_and_plot_annual_climate_data(filename,plot_names,varname,...
    limits,colours,missing_value)

%% Open and plot MI
filename='outputs/moisture_index.nc';
varname='MI';
plot_names='figs/moisture_index';
limits=[0.05 0.1 0.15 0.2 0.3 0.5 1];
colours={'dark_blue'};


open_and_plot_annual_climate_data(filename,plot_names,varname,...
    limits,colours,missing_value)
