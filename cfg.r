
###############################################################################
## Load Packages                                                              ##
################################################################################
source("libs/install_and_source_library.r")
library("gitProjectExtras")
install_and_source_library("visreg")
install_and_source_library("devtools")
install_and_source_library("rmarkdown")

################################################################################
## Setup project                                                              ##
################################################################################
setupProjectStructure()
sourceAllLibs()
graphics.off()



lon_range	 	= cbind(Aus = c(112.50, 160.00),
                          NT  = c(130.00, 134.80),  # North region
                          NSW = c(148.80, 151.50),  # East region
                          WA  = c(114.50, 119.00))  # West region

lat_range 		= cbind(Aus = c(-44.00, -10.75),
                          NT  = c(-14.50, -11.17),	# North region
                          NSW = c(-38.00, -32.00),	# East region
                          WA  = c(-35.50, -27.00))	# West region

region_names 	= colnames(lon_range)

nregions		= length(region_names)
