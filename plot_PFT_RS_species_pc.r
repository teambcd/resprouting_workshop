source("libs/dev.off.all.r")
dev.off.all()
pdf("figs/fig4.pdf", 12,11)
mar=par("mar")
mar[3]=1
mar[1]=4

oma=c(0,0.2,2,0)

layout(t(matrix(c(1,2,3,3),2,2)),widths=c(0.82,0.18))

par(mar=mar,oma=oma,cex=1.2,cex.axis=1.1)
	
source("plotting/Group_Species_and_RS2PFT.r")

Group_Species_and_RS2PFT()

source("plotting/transform_Species_PFT_query_into_required_table.r")
transform_Species_PFT_query_into_required_table()
mtext("(b)",side=3,adj=0,cex=1.5*par("cex"),line=1)
mtext('                                                                 Proportion of taxa (%)',
	side=2,line=3,,cex=1.5*par("cex"))
dev.off()