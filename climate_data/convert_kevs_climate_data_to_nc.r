
common_attributes <- function()
{
    source("../libs/put_raster_in_netcdf.r")
    source("../libs/convert_xyz_to_raster.r")
    
    convertion_descp <<- "Climatology calculated by Kev Willis. Converted from csv to netcdf for Doug Kelley"
    missing_value <<- -999
}
    

convert_EVI <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="data/Kevs_EVI.csv"
    
    # Define output file
	filename_out="outputs/EVI_fapar_climatology.nc"
    varname='fapar'
    long_name="fraction of absorbed photosythetic radiation"
    data_description="fapar climatology"
    data_source="EVI remote sensed data, all years"
    
    cdata=convert_xyz_to_raster(filename_in)   

    for (m in 1:12) {
        temp=values(cdata[[m]])
        test=(temp<0) + (temp!=missing_value)
        temp[which(test==2)]=0
        cdata[[m]]=temp
    }
    
    put_raster_in_netcdf(cdata,missing_value,
                                 filename_out,varname,
                                 data_source,convertion_descp,
                                 long_name,
                                 data_description)    
}

convert_sun <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="data/Kevs_sun.csv"
    
    # Define output file
	filename_out="outputs/ANUCLIM_sun_climatology.nc"
    varname='sun'
    long_name="fractional sunshine hours"
    data_description="climatology"
    data_source="ANUCLIM data, Jan 1970 - Dec 1999"
    
    cdata=convert_xyz_to_raster(filename_in) 
    for (m in 1:12) {
        temp=values(cdata[[m]])
        temp[which(temp!=missing_value)]=
            temp[which(temp!=missing_value)]/100
        cdata[[m]]=temp
    }
    
    
    put_raster_in_netcdf(cdata,missing_value,
                                 filename_out,varname,
                                 data_source,convertion_descp,
                                 long_name,
                                 data_description)    
}

convert_temp <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="data/Kevs_temp.csv"
    
    # Define output file
	filename_out="outputs/ANUCLIM_temp_climatology.nc"
    varname='temp'
    long_name="mean temperature"
    units="degrees C"
    data_description="climatology"
    data_source="ANUCLIM data, Jan 1970 - Dec 1999"
    
    cdata=convert_xyz_to_raster(filename_in)     
    
    put_raster_in_netcdf(cdata,missing_value,
                                 filename_out,varname,
                                 data_source,convertion_descp,
                                 long_name,units,
                                 data_description)    
}

convert_rain <-function()
{
    common_attributes()
    
    # Define inputs file
    filename_in="data/Kevs_rain.csv"
    
    # Define output file
	filename_out="outputs/ANUCLIM_precip_climatology.nc"
    varname='pr'
    long_name="precipitation"
    units="mm/month"
    data_description="climatology"
    data_source="ANUCLIM data, Jan 1970 - Dec 1999"
    
    cdata=convert_xyz_to_raster(filename_in)     
    
    put_raster_in_netcdf(cdata,missing_value,
                                 filename_out,varname,
                                 data_source,convertion_descp,
                                 long_name,units,
                                 data_description)    
}