close all
clear all
addpath ../libs/

%Common attributes
missing_value=-999;

%Open and plot precip
filename='outputs/ANUCLIM_precip_climatology.nc';
varname='pr';
plot_names='figs/precip';
limits=[0  100 200 300 400 800 1500];
min_limits=[0 1 5 10 20 30 40 50 80];
max_limits=[0 10 20 30 40 80 150 200]  ;
colours={'blue'};

open_and_plot_seasonal_climate_data(filename,plot_names,varname,...
    limits,min_limits,max_limits,...
    colours,false,false,missing_value)

%plot negative precip
plot_names='figs/dryness';
open_and_plot_seasonal_climate_data(filename,plot_names,varname,...
    limits,min_limits,max_limits,...
    colours,false,true,missing_value)


%Open and plot sun
filename='outputs/ANUCLIM_sun_climatology.nc';
varname='sun';
plot_names='figs/sun';
limits=[0 0.5:.1:.8 .85 .9];
min_limits=limits;
max_limits=limits;
colours={'red'};

open_and_plot_seasonal_climate_data(filename,plot_names,varname,...
    limits,min_limits,max_limits,...
    colours,true,false,missing_value)

%Open and plot temp
filename='outputs/ANUCLIM_temp_climatology.nc';
varname='temp';
plot_names='figs/temp';
limits=[10:2.5:27.5];
min_limits=limits-5;
max_limits=limits+5;
colours={'red'};

open_and_plot_seasonal_climate_data(filename,plot_names,varname,...
    limits,min_limits,max_limits,...
    colours,true,false,missing_value)

%Open and plot fapar
filename='outputs/EVI_fapar_climatology.nc';
varname='fapar';
plot_names='figs/fapar';
limits=[0:.2:.8]/2;
min_limits=limits;
max_limits=limits;
colours={'dark_green'};

open_and_plot_seasonal_climate_data(filename,plot_names,varname,...
    limits,min_limits,max_limits,...
    colours,true,false,missing_value)

