################################################################################
## cfg                                                                        ##
################################################################################
source('cfg.r')
fname = 'data/Site_bioclimate-ACEAS.csv'

figname = c(SitesPerCell = 'figs/DatasetPointsTest.pdf',
            NoCells      = 'figs/resPointTest.pdf'     )


loc   = read.csv(fname)[c('lat','lon')]

################################################################################
## find info on number of unique cells for a res & no. of sites in cells      ##                                                                   ##
################################################################################
testRes <- function(res, rN = FALSE) {
    loc = loc / res
    loc = round(loc)

    loc[,2] = (loc[,2] + 180) * 1000
    loc[,1] = (loc[,1] + 90)
    loc = loc[,1] + loc[,2]
    loc = sort(table(loc))
    if (rN) return(length(loc))
    return(loc)
}

################################################################################
## find no. unqiue cells for full range of possible resultions               ##                                                                   ##
################################################################################
res = seq(0.5, 0.0009, -0.0025)
ncs = sapply(res, testRes, TRUE)

pdf(figname['NoCells'])
    plot(res, ncs, type = 'l', xlab = 'resolution (deg)',
         ylab = 'estimated no. cells')
dev.off.gitWatermark()

################################################################################
## find no. unqiue cells for each dataset                                     ##                                                                   ##
################################################################################
res = c(0.25, 0.0025, 0.0025, 0.0045, 0.01, 0.1, 0.0009, 0.05, 0.5, 0.005)
ncs = sapply(res, testRes, TRUE)


################################################################################
## find no. sites within each cells for each dataset                          ##                                                                   ##
################################################################################
plotCounts <- function(i, name) {
    names(i) = 1:length(i)
    barplot(i, col = 'black', log = 'y')
    names = paste('Dataset:', name)
    mtext(name, line = -2)
}


datNames = c("GFED v4","MODIS Date of Burn","MODIS Burnt Area",
             "MCD45A1 Burnt Area","Fire Frequancy","Fire radiative power","NVIS",
             "MOD13C1 EVI","VCF","MODIS VCF")

uqs = sapply(res, testRes, FALSE)

pdf(figname['SitesPerCell'])
    par(mfrow = c(4,3), mar = c(2,2,1,1))
    mapply(plotCounts, uqs, datNames)
dev.off.gitWatermark(srt = 0)
