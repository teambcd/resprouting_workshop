##############################################################################
## script sert up                                                           ##
##############################################################################

## libraries ##
source("plotting/plot_SitesClimateMatrix_FocusTraits.r")

## define inputs ##            
#filename_abundances		<<- c(	'outputs/Site_RS_abundance.csv',
#                      		 	'outputs/Site_RS_cover.csv',
#                      			'outputs/Site_RS_ncover.csv',
#                      			'outputs/Site_RS_no_of_species.csv',
#                    			'outputs/Site_RS_pc_of_species.csv')
filename_abundances			<<- c(	'outputs/Site_RS_abundance_no_grass.csv')
## define plots outputs ##
filename_plots			<<-		"figs/SitesClimMatrix.plot-"


## define what needs plotting ##
## define what needs plotting ##
xClimVars_polar			<<-		matrix(c(rep(FALSE,5),TRUE,TRUE),7,1)
xClimVars				<<-		matrix(c('MAP','MAP','MAP','MAP','MAP','DP','faparP'),7,1)
xClimVars_labs 			<<- c(	expression(paste("MAP (mm/year)",sep="")),
								expression(paste("                                      MAP (mm/year)",sep="")),
								expression(paste("MAP (mm/year)",sep="")),
								expression(paste("MAP (mm/year)",sep="")),
								expression(paste("MAP (mm/year)",sep="")),
								expression(paste("Dry"[Phi]," vs. ","Pr"[Theta],sep="")),
					 			expression(paste("EVI"[Phi]," vs. EVI",sep="")))
					 
yClimVars				<<-		matrix(c('MAT','PC','alpha','NPP','MPD','PC','fapar'),7,1)
yClimVars_labs 			<<- c(	expression(paste("MAT (", degree,"C)",sep="")),
					 		  	expression(paste("Pr"[Theta])),
				  	 		  	expression(alpha),
					 		  	expression(paste("NPP (gC/m"^2,")",sep="")),
					 		  	expression(paste("MPD"[IAV],sep="")),
					 		  	expression(paste("Dry"[Phi]," vs. ","Pr"[Theta],sep="")),
					 			expression(paste("EVI"[Phi]," vs. EVI",sep="")))
					 		  	
					 		  	
xClimVars_polar			<<-		matrix(c(rep(FALSE,5),TRUE,TRUE),7,1)
#xClimVars <- matrix(c('MAP','DP'),2,1)
#yClimVars <- matrix(c('MAT','MAP'),2,1)
#xClimVars_polar <- matrix(c(FALSE,TRUE),2,1)

syndromes				<<-	c(	'Apical','Epicormic',
                				'Basal.or.Collar','Underground')

sydnrome_labels		    <<- c( 'Apical','Epicormic',
                				'Basal/Collar','Underground')
LocVars					<<-	c(	'lat','lon')
nregions				<<-		3
lon_range				<<-	matrix(c(
    							128,133.2,    	# North region
    							148.5,152,     	# East region
    							114.5,119),  	# West region            
    							2,nregions)
    
lat_range 				<<- matrix(c(
    							-14.5,-12, 		# North region
    							-37.5,-32, 		# East region
    							-35.5,-29),  	# West region
     							2,nregions)

## limits of file 1 for each attrbite
## Abundances
limits1					<- list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5)          # Underground

## Cover
limits2					<- list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
            	 				5)          # Underground

## nCover
limits3					<- list(5, # Resprouter
            	 				5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5)          # Underground

## No of species
limits_type				<-	c(1,5,10,15,20,25,30,35)
limits4					<-list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5 )          # Underground

## PC of species
limits5					<- list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5)          # Underground

limits_all				<<-list(limits1,limits2,limits3,limits4,limits5)
cunits					<<- 	c('','','','no. of species','')

## deine plotting area ##
mar						<<-		c(1.5,.5,.5,.5)
oma						<<-		c(3,5,5,3)
pc						<<-		19
cex						<<-		2.5
cex.axis				<<-		2
plot_edge_axis_only		<<-		TRUE
plot_axis_on_row		<<-		5
plot_labl_on_col		<<-		2
plot_random				<<-		TRUE
plot_density 			<<- 	TRUE
png_not_pdf				<<-		TRUE

plot_SitesClimateMatrix_FocusTraits_regions()