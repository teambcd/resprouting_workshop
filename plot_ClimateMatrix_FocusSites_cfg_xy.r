source("plotting/plot_ClimateMatrix_FocusSites.R")

filename_plot	<<- 	'figs/climate_space1'

xy_held=TRUE

## Define what climate variables want plotting and how they should be labelled
xClimVars 		<<- c(	'MAT','MTCO','MTWA','GDD0','PAR0')
xClimVars_labs 	<<- list(	c("MAT\n",expression(paste("(",degree,"C)",sep=""))),
							c("MTCO\n",expression(paste("(",degree,"C)",sep=""))),
					 		c("MAT\n",expression(paste("(",degree,"C)",sep=""))),
					 		c("GDD0\n",expression(paste("(",degree,"C)",sep=""))),
					 		#c(expression("PAR"[0]),expression(paste(" (photon/m"^2,")",sep=""))))
					 		expression(paste("PAR"[0]," (",gamma,"/m"^2,")")))
yClimVars 		<<- c(	'MAP','MPW','MPD','alpha','MI','IAVpr')
yClimVars_labs 	<<- list(	c("MAP\n","(mm/year"),
							c("MPW\n","(mm/month"),
							c("MPD\n","(mm/month"),
					 		expression(alpha),
					 		expression(paste("MI",sep="")),
					 		expression(paste("Pr"[IAV],sep="")))
					 
## None of these are polar
xClimVars_polar <<- 	rep(FALSE,5)
cex_general		<<- 3
oma				<<- c(6, 14, 12, 6)

plot_ClimateMatrix_FocusSites()