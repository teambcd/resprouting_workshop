#!/usr/bin/env r
source('cfg.r')
library(markdown)

render.gitInfo('doc/fireDatasets.Rmd', output_dir = 'outputs')
