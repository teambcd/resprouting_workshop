#############################################################################
# this is for the data arrangement of Jeremy's data                         #
# 1 merge the tree and shrub together (one species appears once in one plot)#
# 2 fill the the attributes for each species in each plot                   #
# 3 merge the aboundance data with the species and plot data                #
# 4 fill the method colum based on the tree and shrub                       #
#############################################################################
library(sqldf) #### load the package of sqldf to match the 'species' colums
####### read the data from excel table
address_plotno<-"e:/research/doug/plot_position.csv"
address_tree<-"e:/research/doug/tree.csv"
address_shrub<-"e:/research/doug/shrub.csv"
address_size<-"e:/research/doug/plot_size.csv"
address_atri<-"e:/research/doug/attributes.csv"
address_inte<-"e:/research/doug/g.csv"
address_species_list<-"e:/research/doug/j.csv"
address_method<-"e:/research/doug/method.csv"
read.csv(address_plotno,head=T)-> plotno
read.csv(address_tree,head=T)->tree
read.csv(address_shrub,head=T)->shrub
read.csv(address_size,head=T)->size
read.csv(address_atri,head=T)->atri
# 1 merge the tree and shrub together (one species appears once in one plot)#
tree<-tree[,c(1,3,4)]
shrub<-shrub[,c(1,2,3)]
colnames(tree)<-c("plot","species","method")
colnames(shrub)<-c("plot","species","method")
species<-rbind(tree,shrub)
species<-species[order(species$plot,species$species),]
species[!duplicated(species[1:2]),]->a
a->species
plotno<-plotno[,c(5,6,7)]
colnames(plotno)<-c("plot","lat","lon")
# 2 fill the the attributes for each species in each plot                   #
as.data.frame(atri)->a
a[is.na(a)] <- 0  #### give the na cell with "0" value
a$Resprouter->b
b<-ifelse(b==2,NaN,b) #### there is question maek, to replace the data
b->a$Resprouter
a$Life.form->b
b<-ifelse(b=="SST","single-stem tree",ifelse(b=="MST", "multi-stem tree",ifelse(b=="Sh", "shrub","not sure")))
b->a$Life.form
a->atri
colnames(atri)<-c("species","Resprouter","Apical.resprouter","Epicormic.resprouter",
                  "Basal.collar.resprouter","Underground.resprouter","Non.resprouter",
                  "Seeder","Soil.seedbank","Aerial.seedbank","Life.form","Phenology",
                  "Climatic.range")
###### merge the lat&lon, the plot, and the species attributes together
a<-species
b<-atri
e<-sqldf('SELECT * FROM a LEFT JOIN b ON a.species=b.species')
a<-e
b<-plotno
e<-sqldf('SELECT * FROM a LEFT JOIN b ON a.plot=b.plot')
e<-e[order(e$plot,e$species),]
e[,c(1,18:19,2,3,5:16)]->f
write.csv(f,address_inte)
# 4 fill the method colum based on the tree and shrub
#### caculation of the aboudance
read.csv(address_method,head=T)-> method
method$method<-ifelse(method$adult>0,
                  ifelse((method$saml+method$medium+method$large)>0,
                          "basal area / individuals per ha","basal area"),
                  "individuals per ha")
write.csv(method,address_species_list)
################# then use the 'sqldf' merge the two files together