find_species_site_BB_score_from_BB <- function(lat,lon)
{
  filename="reformatted_FontaineEnright_08May2013_BBscore_t2.csv"
  
  filename_in=paste("../data/",filename,sep="")
  sdata=read.csv(filename_in, header=TRUE)
  
  a=dim(sdata)
    
  tscore=0
  si=1
    
  for (i in 1:a[1]) {
    print("yay")
    print(i)
    if (is.na(sdata[i,1])) {
      sdata[si:(i-1),1]=sdata[si:(i-1),1]*100/tscore      
      tscore=0
      si=i+1  
      
    } else {
      tscore=tscore+sdata[i,1]
    }
  }
  sdata[si:(i),1]=sdata[si:(i),1]*100/tscore 
 
  
  filename_out=paste("../outputs/",filename,sep="")
  write.csv(sdata,filename_out)
  
  return(sdata)
}