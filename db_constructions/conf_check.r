###### this is for checking whether there is a confliction between the data base and the global bark data
###### the input is formated with the first column as the groupno to compare (only do the comparison within the same group)

##### prepare for the confliction
address_conflict<-"e:/research/doug/conflict/conficts_step1.csv"
address_output<-"e:/research/doug/conflict/conficts_final.csv"
read.csv(address_conflict)->conf ## read the data

#####
# 1 unify all the information about "life.form", "leaf.form", "phenology", "climatic range"
# 2 compare the same species from difference source (from)
# 3 if there is no cofliction there should be no out puts, other wise outputs all the lines of different sources
# 4 delete the added column

####### 1summary the "Life.form", "Leaf.form", "Phenology", "Climatic.Range"
#unique(conf$Life.form)->a1
#unique(conf$Leaf.form)->a2
#unique(conf$Phenology)->a3
#unique(conf$Climatic.Range)->a4
#write.csv(a1,"a1.csv")
#write.csv(a2,"a2.csv")
#write.csv(a3,"a3.csv")
#write.csv(a4,"a4.csv")

#### construct the unify link
conf$Life.form.n<-ifelse(conf$Life.form=="single-stem small tree" |
                        conf$Life.form=="Single-stemmed tree" | 
                        conf$Life.form=="single-stem tree",
                        "single-stem tree",
                        ifelse(conf$Life.form=="small tree"|conf$Life.form=="tree", "tree",
                               ifelse(conf$Life.form=="multi-stem tree", "multi-stem tree","shrub"           
                        )))
conf$Leaf.form.n<-ifelse(conf$Leaf.form=="broad"|conf$Leaf.form=="broadleaf","broadleaf","gymnosperm")
conf$Phenology.n<-ifelse(conf$Phenology=="evergreen"|conf$Phenology=="Evergreen","Evergreen","deciduous")
conf$Climatic.Range.n<-ifelse(conf$Climatic.Range=="tropical"|conf$Climatic.Range=="Tropical","Tropical",
                              ifelse(conf$Climatic.Range=="warm temperate"|conf$Climatic.Range=="warm-temperate","warm-temperate","temperate"))

###### the simple comparasion function
com1<-function (a) {
     ifelse(length(unique(a))>1, TRUE, FALSE)
}

# compare the different rows
com3 <-  function (a){
 a$con <-ifelse(com1(a$Resprouter) | com1(a$Apical.resprouter) | com1(a$Epicormic.resprouter) | com1(a$Basal.collar.resprouter)
         |com1(a$Underground.resprouter) |com1(a$Non.resprouter) |com1(a$Seeder) |com1(a$Life.form.n)
         |com1(a$Leaf.form.n) |com1(a$Phenology.n) |com1(a$Climatic.Range.n),1, 0)
 a$con
}

######### to do them by each
b<-NA
groupno<-length(unique(conf$ID))
for (i in 1:groupno) {
### to know the rows we compare
a<-conf[conf$ID==i,]
d<-com3(a)
b<-c(b,d)
}
conf$con<-b[-1] ######### 1 means confiliction, 2 means no conliction
conf_final<-conf[conf$con==1,]
write.csv(conf_final[,1:14],"address_output")