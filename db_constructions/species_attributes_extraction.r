#############################################################################
# this is for species attributes extraction for Ross's data                 #
# we transfered the original code into the data base format                 #
# with the simple ifelse function, which can be carried out easily in excel #
#############################################################################
####### read the data from excel table
address_ross<-"e:/research/doug/ross.csv"
read.csv(address_ross,head=T)-> ross
ross[!duplicated(ross$species),]->a
a$Resprouter<-ifelse(a$Fire.response=="S",0,ifelse(is.na(a$Fire.response),NaN,1))
a$Seeder<-ifelse(a$Fire.response=="R",0,ifelse(is.na(a$Fire.response),NaN,1))
a$soil_bank<-ifelse(a$Seed.storage=="persistent soil" |
                      a$Seed.storage=="transient / persistent soil" |
                      a$Seed.storage=="soil" | a$Seed.storage=="canopy (>1y) & soil" |
                      a$Seed.storage=="persistent soil?", 1,ifelse(is.na(a$Fire.response),NaN,0))
a$areial_bank<-ifelse(a$Seed.storage=="canopy (>1y) & soil" |
                      a$Seed.storage=="serotinous canopy (retention >8y; 20)" |
                      a$Seed.storage=="serotinous canopy" |
                      a$Seed.storage=="canopy" |
                      a$Seed.storage=="canopy - transient",1, ifelse(is.na(a$Fire.response),NaN,0) )
a$life_form<-ifelse(a$Life.form=="T" |
                      a$Life.form=="Tw hemiparasite" |
                      a$Life.form=="sT" |
                      a$Life.form=="M or T" |
                      a$Life.form=="TF" |
                      a$Life.form=="S or T non-lignotuberous" |
                      a$Life.form=="Tw","Single-stemmed tree",
                      ifelse(a$Life.form=="S" |
                             a$Life.form=="S or sT" |
                             a$Life.form=="S palm-like" |
                             a$Life.form=="S multistemmed" |
                             a$Life.form=="S lignotuberous" |
                             a$Life.form=="S diffuse" |
                             a$Life.form=="S divaricate" |
                             a$Life.form=="sT or S" |
                             a$Life.form=="S slender" |
                             a$Life.form=="S robust" |
                             a$Life.form=="S or subS" |
                             a$Life.form=="S bushy" ,"Shrub",
                             ifelse(a$Life.form=="S spreading" |
                                    a$Life.form=="S prostrate" |
                                    a$Life.form=="subS" |
                                    a$Life.form=="S straggling" |
                                    a$Life.form=="sS" |
                                    a$Life.form=="S scrambling" |
                                    a$Life.form=="S mat-forming" |
                                    a$Life.form=="S procumbent" |
                                    a$Life.form=="S dwarf" |
                                    a$Life.form=="subS multistemmed" |
                                    a$Life.form=="S decumbent", "Dwarf shrub (sub-shrub)",
                                    ifelse(a$Life.form=="F rhizomatous" |
                                           a$Life.form=="H or subS" |
                                           a$Life.form=="H with creeping stems" |
                                           a$Life.form=="H prostrate" |
                                           a$Life.form=="H taproot persistent" |
                                           a$Life.form=="H epiphyte" |
                                           a$Life.form=="H prostrate or climbing" |
                                           a$Life.form=="H tuberous" |
                                           a$Life.form=="H hemicryptophyte" |
                                           a$Life.form=="H straggling" |
                                           a$Life.form=="H twining" |
                                           a$Life.form=="H scrambling or twining" |
                                           a$Life.form=="H creeping" |
                                           a$Life.form=="H sprawling" |
                                           a$Life.form=="H woody rootstock" |
                                           a$Life.form=="H woody", "Perennial forb",
                                           ifelse(a$Life.form=="subS climbing or prostrate" ,"C4 grass",
                      NaN)))))
                      


