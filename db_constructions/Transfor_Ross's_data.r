library(sqldf) #### load the package of sqldf to match the colums

address_species<-"e:/research/doug/ross1/new species.csv"
address_pre<-"e:/research/doug/ross1/final111 - Copy.csv"
read.csv(address_species,head=T)-> species
read.csv(address_pre,head=T)-> pre

############  transfer the data into stander verison
species->a
a$Resprouter<-ifelse(a$Fire.response=="S",0,ifelse(is.na(a$Fire.response),NaN,1))
a$Seeder<-ifelse(a$Fire.response=="R",0,ifelse(is.na(a$Fire.response),NaN,1))
a$soil_bank<-ifelse(a$Seed.storage=="persistent soil" |
                      a$Seed.storage=="transient / persistent soil" |
                      a$Seed.storage=="soil" | a$Seed.storage=="canopy (>1y) & soil" |
                      a$Seed.storage=="persistent soil?", 1,ifelse(is.na(a$Fire.response),NaN,0))
a$areial_bank<-ifelse(a$Seed.storage=="canopy (>1y) & soil" |
                      a$Seed.storage=="serotinous canopy (retention >8y; 20)" |
                      a$Seed.storage=="serotinous canopy" |
                      a$Seed.storage=="canopy" |
                      a$Seed.storage=="canopy - transient",1, ifelse(is.na(a$Fire.response),NaN,0) )
a$life..form<-ifelse(a$Life.form=="T" |
                      a$Life.form=="Tw hemiparasite" |
                      a$Life.form=="sT" |
                      a$Life.form=="M or T" |
                      a$Life.form=="TF" |
                      a$Life.form=="S or T non-lignotuberous" |
                      a$Life.form=="Tw","Single-stemmed tree",
                      ifelse(a$Life.form=="S" |
                             a$Life.form=="S or sT" |
                             a$Life.form=="S palm-like" |
                             a$Life.form=="S multistemmed" |
                             a$Life.form=="S lignotuberous" |
                             a$Life.form=="S diffuse" |
                             a$Life.form=="S divaricate" |
                             a$Life.form=="sT or S" |
                             a$Life.form=="S slender" |
                             a$Life.form=="S robust" |
                             a$Life.form=="S or subS" |
                             a$Life.form=="S bushy" ,"Shrub",
                             ifelse(a$Life.form=="S spreading" |
                                    a$Life.form=="S prostrate" |
                                    a$Life.form=="subS" |
                                    a$Life.form=="S straggling" |
                                    a$Life.form=="sS" |
                                    a$Life.form=="S scrambling" |
                                    a$Life.form=="S mat-forming" |
                                    a$Life.form=="S procumbent" |
                                    a$Life.form=="S dwarf" |
                                    a$Life.form=="subS multistemmed" |
                                    a$Life.form=="S decumbent", "Dwarf shrub (sub-shrub)",
                                    ifelse(a$Life.form=="F rhizomatous" |
                                           a$Life.form=="H or subS" |
                                           a$Life.form=="H with creeping stems" |
                                           a$Life.form=="H prostrate" |
                                           a$Life.form=="H taproot persistent" |
                                           a$Life.form=="H epiphyte" |
                                           a$Life.form=="H prostrate or climbing" |
                                           a$Life.form=="H tuberous" |
                                           a$Life.form=="H hemicryptophyte" |
                                           a$Life.form=="H straggling" |
                                           a$Life.form=="H twining" |
                                           a$Life.form=="H scrambling or twining" |
                                           a$Life.form=="H creeping" |
                                           a$Life.form=="H sprawling" |
                                           a$Life.form=="H woody rootstock" |
                                           a$Life.form=="H woody", "Perennial forb",
                                           ifelse(a$Life.form=="subS climbing or prostrate" ,"C4 grass",
                      NaN)))))

a$r.apical<-ifelse(a$Resprout.location=="epicormic or apical & rhizome" |
                   a$Resprout.location=="apical / basal" |
                   a$Resprout.location=="basal / apical" |
                   a$Resprout.location=="rhizome or apical" |
                   a$Resprout.location=="apical" |
                   a$Resprout.location=="apical buds", 1, NA)
a$r.epicormic<-ifelse(a$Resprout.location=="epicormic or apical & rhizome" |
                      a$Resprout.location=="epicormic & basal & root suckers" |
                      a$Resprout.location=="epicormic & basal / root suckers" |
                      a$Resprout.location=="epicormic, lignotuber and root suckers" |
                      a$Resprout.location=="root suckers & basal & epicormic" |
                      a$Resprout.location=="root suckers / lignotuber & epicormic" |
                      a$Resprout.location=="basal & epicormic" |
                      a$Resprout.location=="basal & some epicormic" |
                      a$Resprout.location=="basal or epicormic" |
                      a$Resprout.location=="coppice / epicormic" |
                      a$Resprout.location=="epicormic & basal" |
                      a$Resprout.location=="epicormic & lignotuber" |
                      a$Resprout.location=="epicormic / lignotuber" |
                      a$Resprout.location=="lignotuber & epicormic" |
                      a$Resprout.location=="lignotuber / epicormic" |
                      a$Resprout.location=="epicormic & rootstock" |
                      a$Resprout.location=="epicormicT" ,1, NA)
a$r.basal<-ifelse(a$Resprout.location=="apical / basal" |
                  a$Resprout.location=="basal / apical" |
                  a$Resprout.location=="epicormic & basal & root suckers" |
                  a$Resprout.location=="epicormic & basal / root suckers" |
                  a$Resprout.location=="epicormic, lignotuber and root suckers" |
                  a$Resprout.location=="root suckers & basal & epicormic" |
                  a$Resprout.location=="root suckers / lignotuber & epicormic" |
                  a$Resprout.location=="basal & epicormic" |
                  a$Resprout.location=="basal & some epicormic" |
                  a$Resprout.location=="basal or epicormic" |
                  a$Resprout.location=="coppice / epicormic" |
                  a$Resprout.location=="epicormic & basal" |
                  a$Resprout.location=="epicormic & lignotuber" |
                  a$Resprout.location=="epicormic / lignotuber" |
                  a$Resprout.location=="lignotuber & epicormic" |
                  a$Resprout.location=="lignotuber / epicormic" |
                  a$Resprout.location=="basal & root suckers" |
                  a$Resprout.location=="basal & stolons" |
                  a$Resprout.location=="basal / rhizome" |
                  a$Resprout.location=="basal / root suckers" |
                  a$Resprout.location=="lignotuber & root suckers" |
                  a$Resprout.location=="lignotuber / root suckers " |
                  a$Resprout.location=="lignotuber / rootuckers" |
                  a$Resprout.location=="lignotuber / sometimes suckers" |
                  a$Resprout.location=="root suckers / basal" |
                  a$Resprout.location=="root suckers / lignotuber" |
                  a$Resprout.location=="rootstock & coppice" |
                  a$Resprout.location=="basal" |
                  a$Resprout.location=="basal: tuber (10; 48)" |
                  a$Resprout.location=="coppice" |
                  a$Resprout.location=="lignotuber" |
                  a$Resprout.location=="lignotuber and coppice" |
                  a$Resprout.location=="stem bases" |
                  a$Resprout.location=="tap root",1,NA)
a$r.underground<-ifelse(a$Resprout.location=="epicormic or apical & rhizome" |
                        a$Resprout.location=="rhizome or apical" |
                        a$Resprout.location=="epicormic & basal & root suckers" |
                        a$Resprout.location=="epicormic & basal / root suckers" |
                        a$Resprout.location=="epicormic, lignotuber and root suckers" |
                        a$Resprout.location=="root suckers & basal & epicormic" |
                        a$Resprout.location=="root suckers / lignotuber & epicormic" |
                        a$Resprout.location=="epicormic & rootstock" |
                        a$Resprout.location=="basal & root suckers" |
                        a$Resprout.location=="basal & stolons" |
                        a$Resprout.location=="basal / rhizome" |
                        a$Resprout.location=="basal / root suckers" |
                        a$Resprout.location=="lignotuber & root suckers" |
                        a$Resprout.location=="lignotuber / root suckers" |
                        a$Resprout.location=="lignotuber / rootuckers" |
                        a$Resprout.location=="lignotuber / sometimes suckers" |
                        a$Resprout.location=="root suckers / basal" |
                        a$Resprout.location=="root suckers / lignotuber" |
                        a$Resprout.location=="rootstock & coppice" |
                        a$Resprout.location=="has rhizome" |
                        a$Resprout.location=="rhizome" |
                        a$Resprout.location=="rhizome?" |
                        a$Resprout.location=="root stock" |
                        a$Resprout.location=="root suckers" |
                        a$Resprout.location=="root suckers & rhizome" |
                        a$Resprout.location=="rootstock" |
                        a$Resprout.location=="rootstock & root suckers" |
                        a$Resprout.location=="rootstock / root suckers" |
                        a$Resprout.location=="rootsuckers &/or rhizome" |
                        a$Resprout.location=="stolons", 1, NA)

a<-a
b<-pre
e<-sqldf('SELECT * FROM a LEFT JOIN b ON a.Species=b.species')

dif <- function (a,b) {
  ifelse(is.na(a), b, a)
}

dif(e$Resprouter, e$Resprouter_a)->e$Resprouter

dif(e$r_basal, e$basalResprout)->e$r_basal
dif(e$Resprouter, e$Resprouter_a)->e$Resprouter
dif(e$Seeder, e$Seedera)->e$Seeder
dif(e$soil_bank, e$soilbank)->e$soil_bank
dif(e$areial_bank, e$areialbank)->e$areial_bank
dif(e$life__form, e$lifeform)->e$life__form
e[,c(1:17)]->a     #### this should be a draft for species attributes


