configure_converting_raw_cover_and_abundance <- function() {
	source("../libs/BB_cover_conversions.r")
	source("../libs/return_multiple_from_functions.r")
	
	filename_in		<<- 'data/raw_abundance_data.csv'
	filename_out	<<- 'outputs/converted_abundance_data.csv'

}

converting_raw_cover_and_abundance <- function() {
	configure_converting_raw_cover_and_abundance()
	
	rdata=read.csv(filename_in,stringsAsFactors=FALSE)

	## remove  empties
	test=(is.na(rdata$BBscore)+is.na(rdata$Cover))<2
	rdata=rdata[test,]
	
	## Find and convert BB scores only	
	rdata=convert_BB_scores_to_cover(rdata)
	
	## convert all % to BB score
	print('Progress: about to convert % to BB')
	c(rdata$BBscore,nn):=BB_cover_conversions(cover=rdata$Cover)
	
	## normalise both
	print('Progress: about to normalise BB')
	rdata$BBscore_normalised=normalise_scores_or_cover(rdata$site,rdata$BBscore)*100
	print('Progress: about to normalise cover')
	rdata$cover_normalised  =normalise_scores_or_cover(rdata$site,rdata$Cover)*100
	
	## And we're done!
	write.csv(rdata,filename_out,row.names=FALSE)
}

convert_BB_scores_to_cover <- function(rdata) {
	testi=((is.na(rdata$BBscore)==FALSE)+
		  (is.na(rdata$Cover)  ==TRUE))==2
	bdata=rdata[testi,]
	
	
	
	## convert for score 1-6 to 0.1-5 and 1-7 to 0.1-6
	test=bdata$Method=='BB'
	for (i in c("Cover 1 to 6","Cover 1 to 6 (a)","Cover 1 to 7")) {
		c(bdata,test0):=change_1_6_to_0.1_5(bdata,i)
		test=test+test0
	}
	
	## Make sure we have converted all the different Methods of recording BB
	if (sum(test==0)>0) {
		stop(paste("some BB scores not converting. Missing conversions for:",
					unique(bdata$Method[test==0])))
	}

	## convert 0.1-6 BB to %
	print('Progress: about to convert BB7 to %')
	test=bdata$Method=="Cover 1 to 7"
	c(nn,bdata$Cover[test]):=BB_cover_conversions(bdata$BBscore[test],BBtype='BB7')
	
	## convert 0.1-5 BB to %
	print('Progress: about to convert BB6 to %')
	test=test==FALSE

	c(nn,bdata$Cover[test]):=BB_cover_conversions(bdata$BBscore[test])
	
	rdata[testi,]=bdata
	
	return(rdata)
	
}

change_1_6_to_0.1_5 <- function(bdata,method="Cover 1 to 6") {
	test=bdata$Method==method
	
	cscores=bdata$BBscore[test]
	
	cscores[cscores==1]=0.1
	cscores[cscores!=0.1]=cscores[cscores!=0.1]-1
	
	
	bdata$BBscore[test]=cscores
	
	return(list(bdata,test))
}

normalise_scores_or_cover <- function(ID,nums) {
	for (i in unique(ID)) {
		test=ID==i
		tot=sum(nums[test])
		nums[test]=nums[test]/tot
	}
	
	return(nums)
}
	