configure <- function() {
    ##############################################################
    ## for defining file paths etc                              ##
    ##############################################################
    

    ## Database sites location                                  ##
    ##############################################################
	filename_locs       <<- "../data/Site_locations.csv"
    ##############################################################
    
    
    ## Climate file and path info                               ##
    ##############################################################
    filename_temp       <<-
             "../climate_data/outputs/ANUCLIM_temp_climatology.nc"
    varname_temp        <<- "temp"
    #------------------------------------------------------------#
    filename_fapar      <<- 
                "../climate_data/outputs/EVI_fapar_climatology.nc"
    varname_fapar       <<- "fapar"
    #------------------------------------------------------------#
    filename_precip     <<-
           "../climate_data/outputs/ANUCLIM_precip_climatology.nc"
    varname_precip      <<- "pr"
    #------------------------------------------------------------#
    filename_IAVpr      <<-
           "../climate_data/data/precip_IAV_ANU_aus.nc"
    varname_IAVpr       <<- "IAV"
    ##############################################################
    
	
    ## Bio-climate file and path info                           ##
    ##############################################################
    filename_alpha      <<- 
                   "../bioclimatic_limits/outputs/alpha_annual.nc"
    varname_alpha       <<- "alpha"
    #------------------------------------------------------------#
    filename_alpha_min      <<- 
                   "../bioclimatic_limits/outputs/alpha-min.nc"
    varname_alpha_min       <<- "alpha"
    #------------------------------------------------------------#
    filename_alpha_max      <<- 
                   "../bioclimatic_limits/outputs/alpha-max.nc"
    varname_alpha_max       <<- "alpha"
    #------------------------------------------------------------#
    filename_alpha_phase      <<- 
                   "../bioclimatic_limits/outputs/alpha-phase.nc"
    varname_alpha_phase       <<- "alpha"
    #------------------------------------------------------------#
    filename_alpha_conc      <<- 
                   "../bioclimatic_limits/outputs/alpha-conc.nc"
    varname_alpha_conc       <<- "alpha"
    #------------------------------------------------------------#
    filename_GDD0       <<- 
                           '../bioclimatic_limits/outputs/GDD0.nc'
    varname_GDD0        <<- 'GDD0'
    #------------------------------------------------------------#
    filename_moisture_index  <<- 
                 '../bioclimatic_limits/outputs/moisture_index.nc'
    varname_moisture_index   <<- 'MI'
    #------------------------------------------------------------#
    filename_PAR0       <<-
                           '../bioclimatic_limits/outputs/PAR0.nc'
    varname_PAR0        <<- 'PAR0'
    #------------------------------------------------------------#
    filename_NPP        <<-'../bioclimatic_limits/outputs/NPP.nc'
    varname_NPP         <<- 'NPP'
    ##############################################################
    	
	## outputs file                                             ##
    ##############################################################
	output_file         <<- "../outputs/Site_bioclimate.csv"
    ############################################################## 


}

find_climate_variables <- function(lat,lon)
{  
    configure()
	
    ## Set up: open lat and lon  for sites                      ##
    ##############################################################
	ll=read.csv(filename_locs,header=TRUE)
	SiteID=ll[,1]
	lat=ll[,2]
	lon=ll[,3]
	
	## find climate points with seasonal information at sites   ##
    ##############################################################
    print("finding temp")
    temp=load_seasonal_var_and_find_points(filename_temp,
        varname_temp,lat,lon,TRUE,TRUE,FALSE,TRUE)
    #------------------------------------------------------------#
    print("finding precip")
    precip=load_seasonal_var_and_find_points(filename_precip,
        varname_precip,lat,lon,FALSE,TRUE,TRUE,TRUE)
    #------------------------------------------------------------# 
    print("finding 'dryness'")
    dryness=load_seasonal_var_and_find_points(filename_precip,
        varname_precip,lat,lon,FALSE,TRUE,TRUE,TRUE,negative=TRUE)
        
    #------------------------------------------------------------#
    print("finding fapar")
    fapar=load_seasonal_var_and_find_points(filename_fapar,
        varname_fapar,lat,lon,TRUE,TRUE,TRUE,TRUE)
        
    ##############################################################
    ## An odd one: temp during middle of dry season 			##
    ##############################################################
    print("finding temp in wettest month")
    dry.month=round(12*dryness$phase/360)
    dry.month[dry.month==0]=12
    
    Tdry=find_x_during_month(filename_temp,varname_temp,
    							 lat,lon,dry.month)
    ##############################################################
    ## The lonely one-and-only non-seasonal climate variable    ##
    ##############################################################
    IAVpr=load_annual_and_find_points(filename_IAVpr,
        varname_IAVpr,lat,lon)

    ## find bioclimate annual information at sites              ##
    ##############################################################
    valpha=load_annual_and_find_points(filename_alpha,
        varname_alpha,lat,lon)
        
    valpha_min=load_annual_and_find_points(filename_alpha_min,
        varname_alpha_min,lat,lon)
        
    valpha_max=load_annual_and_find_points(filename_alpha_max,
        varname_alpha_max,lat,lon)
        
    valpha_phase=load_annual_and_find_points(filename_alpha_phase,
        varname_alpha_phase,lat,lon)
        
    valpha_conc=load_annual_and_find_points(filename_alpha_conc,
        varname_alpha_conc,lat,lon)
    #------------------------------------------------------------#
    vGDD0=load_annual_and_find_points(filename_GDD0,varname_GDD0,
        lat,lon)
    #------------------------------------------------------------#
	vmoisture_index=load_annual_and_find_points(
        filename_moisture_index,varname_moisture_index,lat,lon)
    #------------------------------------------------------------#
	vPAR0=load_annual_and_find_points(filename_PAR0,
        varname_PAR0,lat,lon)
    #------------------------------------------------------------#     
	vNPP=load_annual_and_find_points(filename_NPP,varname_NPP,
        lat,lon) 
    ##############################################################     
    
    ## Output to csv ready for insersion into database          ##
    ## Order of variables must match database order             ##
    ############################################################## 
	output=data.frame(SiteID=SiteID,lat=lat,lon=lon,
		MAT=temp$annual_average,MTCO=temp$minv,MTWA=temp$maxv,
            GDD0=vGDD0,Tdry=Tdry,
		MAP=precip$annual_average,MPW=precip$maxv,MPD=precip$minv,
			PC=precip$conc,PP=precip$phase,IAVpr=IAVpr,
            DP=dryness$phase,
            alpha=valpha,alpha_min=valpha_min,alpha_max=valpha_max,
            alpha_phase=valpha_phase,alpha_conc=valpha_conc,
            MI=vmoisture_index,
		fapar=fapar$annual_average,fapar_max=fapar$maxv,
            fapar_min=fapar$minv,faparC=fapar$conc,
            faparP=fapar$phase,PAR0=vPAR0,NPP=vNPP)
    #------------------------------------------------------------# 	
	write.table(output,output_file,sep=",",row.names=FALSE)
    ############################################################## 

}


load_annual_and_find_points <- function(filename,varname,lat,lon) {

    #opens netcdf
    cdata=raster(filename,varname)

    #extent and lat lon conversions    
    ij=convert_lat_lon_to_ijs(cdata,lat,lon)
    
    # cite the bioclimate data with grid index
    cdata=find_closest_land_cell(cdata,ij)
	
    return(cdata)
}

find_closest_land_cell <- function(cdata,ij) {
    cdata[trunc(cdata)==-999]=NA
	vdata=cdata[ij]
	sea=which(is.na(vdata))
	a=length(sea)
	if (a==0) return(vdata)
	
	for (cell in 1:a[1]) {
		print(cell)
		imin=ij[sea[cell],1]-1
		imax=ij[sea[cell],1]+1
		jmin=ij[sea[cell],2]-1
		jmax=ij[sea[cell],2]+1
		
		vdata[sea[cell]]=mean(cdata[imin:imax,jmin:jmax],na.rm=TRUE)
	}
	
	return(vdata)


}

find_closest_land_cell_and_month<- function(cdata,ij) {
    cdata[trunc(cdata)==-999]=NA
    
	vdata=cdata[ij[,1:2]]
	
	find_month <- function(i) vdata[i,ij[i,3]]
	vdata=sapply(1:dim(vdata)[1],find_month)
	
	
	
	sea=which(is.na(vdata))
	a=length(sea)
	if (a==0) return(vdata)
	
	for (cell in 1:a[1]) {
		print(cell)
		imin=ij[sea[cell],1]-1
		imax=ij[sea[cell],1]+1
		jmin=ij[sea[cell],2]-1
		jmax=ij[sea[cell],2]+1
		
		surr=cdata[imin:imax,jmin:jmax]
		vdata[sea[cell]]=apply(surr,2,mean,na.rm=TRUE)[ij[sea[cell],3]]
	}
	
	return(vdata)
}


find_x_during_month <- function(filename,varname,lat,lon,month) {
    library("raster")
	print("opening netcdf")
    cdata=brick(filename,varname)
    
    ij=convert_lat_lon_to_ijs(cdata,lat,lon)
    
    vdata=find_closest_land_cell_and_month(cdata,cbind(ij,month))
    browser()
    return(vdata)
    
}


load_seasonal_var_and_find_points <-function(filename,varname,lat,lon,average=FALSE,
  annual=TRUE,seasonal=TRUE,max_min=TRUE,negative=FALSE)
{
    
	source("../libs/find_annual_average.r")
    source("../libs/find_seasonal_concentration_and_phase.r")
    source("../libs/find_max_and_min_of_highest_lowest_month.r")
    library("raster")
    
    print("opening netcdf")
    cdata=brick(filename,varname)
    
    if (negative) {
        cdata=max(cdata)-cdata
    }
    
    #extent and lat lon conversions
    print("converting lats and lons to matrix index")
    ij=convert_lat_lon_to_ijs(cdata,lat,lon)
    
    annual_average=NaN
    phase=NaN
    conc=NaN 
    minv=NaN
    maxv=NaN   
    
    if (annual) {
      print("calcuating annual average")
      adata=find_annual_average(cdata,average)
	  annual_average=find_closest_land_cell(adata,ij)
    }
    
    if (seasonal) {
      print("calculating seasonal values")
      sdata=find_seasonal_concentration_and_phase(cdata)
	  
	  phase=find_closest_land_cell(sdata[[1]],ij)
	  conc=find_closest_land_cell(sdata[[2]],ij)
    }
    
    if(max_min)   {
      print("calculating mins and maxs")
      mdata=find_max_and_min_of_highest_lowest_month(cdata)   
	  
	  minv=find_closest_land_cell(mdata[[1]],ij)
	  maxv=find_closest_land_cell(mdata[[2]],ij)
    }
    
    list(annual_average=annual_average,
		phase=phase,conc=conc,minv=minv,maxv=maxv)
    
}


convert_lat_lon_to_ijs <- function(cdata,lat,lon) {

    lon_range=c(xmin(cdata),xmax(cdata))
    lat_range=c(ymin(cdata),ymax(cdata))
    
    dlat=res(cdata)[1]
    dlon=res(cdata)[2]
	
    jj=round(1+(lon-lon_range[1])/dlon)
    ii=round(1+(lat_range[2]-lat)/dlat)
   
    ij=cbind(ii,jj)
    return(ij) 
  }