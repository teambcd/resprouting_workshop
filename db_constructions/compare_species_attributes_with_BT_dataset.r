configure <-function() {

	source("../libs/find_and_return_first_n_words.r")

	filename_BT <<- "../data/BT_species_list.csv"	
	filename_RS <<- "../data/RS_species_list.csv"
	
	BT_atts <<- read.table(filename_BT,
        sep=",",header=TRUE,stringsAsFactors = FALSE)
		
	RS_atts <<- read.table(filename_RS,
        sep=",",header=TRUE,stringsAsFactors = FALSE)
}

convert_species_list_to_same_format <-function(BT_atts) {
	configure()
	
	a=dim(BT_atts)
	species_names=paste(BT_atts$Genus,BT_atts$Species,sep=" ")
	
	resprouting=rep(NA,a[1])
	resprouting[BT_atts$reprouter.=='yes']=1
	resprouting[BT_atts$reprouter.=='Yes']=1
	resprouting[BT_atts$reprouter.=='no']=0
	resprouting[BT_atts$reprouter.=='No']=0
	
	BT_atts=data.frame(
		Species.name=species_names,	
		Resprouter=resprouting,
		Apical.resprouter=BT_atts$Apical,
		Epicormic.resprouter=BT_atts$Epicormic,
		Basal.collar.resprouter=BT_atts$Basal.Collar,
		Underground.resprouter=BT_atts$Underground,
		Non.resprouter=1-resprouting,
		Seeder=rep(NA,a[1]),
		Life.form=BT_atts$life.form,
		Leaf.form=BT_atts$leaf.type,
		Phenology=BT_atts$phenology,
		Climatic.Range=BT_atts$Climate.range,
		stringsAsFactors=FALSE)
	
	return(BT_atts)
	
	
}

use_bark_thickness_species_list_to_fill_blanks <- function() {

	configure()
	
	BT_atts=convert_species_list_to_same_format(BT_atts)
	BT_atts[BT_atts==0]=NA
	RS_atts$Life.form[RS_atts$Life.form==0]=NA
	
	conflict=RS_atts[1,]
	conflict_file='no'
	conflictID=c(0)
	nconflicts=0

	RS_species_names=find_and_return_first_n_words(RS_atts$Species.name)
	BT_species_names=find_and_return_first_n_words(BT_atts$Species.name)
		
	a=length(BT_species_names)
	p=length(RS_species_names)
	updated=rep('no',p)
	
	ID=0
	
	for ( i in 1:a[1]) {
		indexs=which(BT_species_names[i]==RS_species_names)
		if (length(indexs)>0) {
			for (j in indexs) {
				blank=(RS_atts[j,]=="")
				blank[is.na(blank)]=FALSE
				
				empty_cells=(is.na(RS_atts[j,])+blank)>0
				RS_atts[j,empty_cells]=BT_atts[i,empty_cells]
				updated[j]='yes'
				
				BT_unknown=is.na(BT_atts[i,])
				BT_atts[i,BT_unknown]=RS_atts[j,BT_unknown]
				unempty_cells=empty_cells==FALSE
				
				if (sum(RS_atts[j,unempty_cells]!=BT_atts[i,unempty_cells],na.rm=TRUE)>0) {

					if (nconflicts==0 || conflict[nconflicts,1]!=RS_atts[j,1]) {
						print(conflict[nconflicts,1])
						print(RS_atts[j,1])
						print("-----")
						test=1
						ID=ID+1
						
						nconflicts=nconflicts+1
						conflict[nconflicts,]=BT_atts[i,]
						conflict_file[nconflicts]='BT'
						conflictID[nconflicts]=ID
					}
					
					nconflicts=nconflicts+1
					conflict[nconflicts,]=RS_atts[j,]
					conflict_file[nconflicts]='RS'
					conflictID[nconflicts]=ID			
					
					
				}					
			}
		}
		
	}
		RS_atts$updated=updated
		conflict$from=conflict_file
		
		conflict$ID=conflictID
		
	write.csv(RS_atts,"tst.csv")
	write.csv(conflict,"tst2.csv")
		
}


