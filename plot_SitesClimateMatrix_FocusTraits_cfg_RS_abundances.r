##############################################################################
## script sert up                                                           ##
##############################################################################

## libraries ##
source("plotting/plot_SitesClimateMatrix_FocusTraits_regions.r")

## define inputs ##            
filename_abundances		<<- c(	'outputs/Site_RS_no_of_species_no_grass.csv',
                    			'outputs/Site_RS_pc_of_species_no_grass.csv',
                      		 	'outputs/Site_RS_cover_no_grass.csv',
                      			'outputs/Site_RS_ncover_no_grass.csv',
                      			'outputs/Site_RS_abundance_no_grass.csv')          
#filename_abundances		<<- c(	'outputs/Site_RS_cover.csv')
coltitle				<<-	c(	'number','% taxa','% cover',
								'normalized cover','abundance score')
file_combined			<<-		TRUE
## define plots outputs ##
filename_plots			<<-		"figs/SitesClimMatrix.plot-"


## define what needs plotting ##
xClimVars_polar			<<-		matrix(c(rep(FALSE,5),TRUE,TRUE),7,1)
xClimVars				<<-		matrix(c('MAP','MAP','MAP','MAP','MAP','PP','faparP'),7,1)
xClimVars_labs 			<<- c(	expression(paste(" ",sep="")),
								expression(paste(" ",sep="")),
								expression(paste("MAP (mm/year)",sep="")),
								expression(paste(" ",sep="")),
								expression(paste(" ",sep="")),
								expression(paste("Pr"[Phi]," vs. ","Pr"[Theta],sep="")),
					 			expression(paste("EVI"[Phi]," vs. EVI",sep="")))
					 
yClimVars				<<-		matrix(c('MAT','PC','alpha','NPP','MPD','PC','fapar'),7,1)
yClimVars_labs 			<<- c(	expression(paste("MAT (", degree,"C)",sep="")),
					 		  	expression(paste("Pr"[Theta])),
				  	 		  	expression(alpha),
					 		  	expression(paste("NPP (gC/m"^2,")",sep="")),
					 		  	expression(paste("MPD"[IAV],sep="")),
					 		  	expression(paste("Pr"[Phi]," vs. ","Pr"[Theta],sep="")),
					 			expression(paste("EVI"[Phi]," vs. EVI",sep="")))
					 		  	
					 		  	
xClimVars_polar			<<-		matrix(c(rep(FALSE,5),TRUE,TRUE),7,1)
#xClimVars <- matrix(c('MAP','MAP'),2,1)
#yClimVars <- matrix(c('MAT','DP'),2,1)
#xClimVars_polar <- matrix(c(FALSE,TRUE),2,1)

syndromes				<<-	c(	'Resprouter')


## limits of file 1 for each attrbite
## Abundances
limits1					<- list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5)          # Underground

## Cover
limits2					<- list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
            	 				5)          # Underground

## nCover
limits3					<- list(5, # Resprouter
            	 				5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5)          # Underground

## No of species
limits_type				<-	c(1,5,10,15,20,25,30,35)
limits4					<-list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5)          # Underground

## PC of species
limits5					<- list(5, # Resprouter
             					5,          # Aprical
             					5,          # Epicormic
             					5,          # Basal.or.Collar
             					5)          # Underground

limits_all				<<-list(limits1,limits2,limits3,limits4,limits5)
cunits					<<- 	c('','','',' ','')

## deine plotting area ##
mar						<<-		c(1.5,.5,.5,.5)
oma						<<-		c(3,5,5,3)
pc						<<-		19
cex						<<-		2.5
cex.axis				<<-		2
plot_edge_axis_only		<<-		TRUE
plot_axis_on_row		<<-		5
plot_labl_on_col		<<-		2
plot_random				<<-		TRUE
plot_density 			<<- 	TRUE
png_not_pdf				<<-		TRUE

plot_SitesClimateMatrix_FocusTraits_regions()