function [pdata,cdata]=convert_to_seasonal_conc_and_phase...
    (data,missing_value)
    
    [a b c]=size(data);
    
    xdata=zeros(a,b,'single');
    ydata=zeros(a,b,'single');
    for k=1:c
        angle=2*pi*(13-k)/12;
        xdata=xdata+data(:,:,k)*cos(angle);        
        ydata=ydata+data(:,:,k)*sin(angle);
    end
    
    pdata=atans(-ydata,-xdata);
    cdata=sqrt(xdata.^2+ydata.^2)./sum(data,3);
    
    for i=1:c
        pdata(find(data(:,:,i)==missing_value))=0;
        pdata(isnan(data(:,:,i)))=0;
        
        cdata(find(data(:,:,i)==missing_value))=0;
        cdata(isnan(data(:,:,i)))=0;        
    end
end
%   