find_annual_average <- function(cdata,average=FALSE)
{
    a=dim(cdata)
    
    adata=cdata[[1]]
    
    adata[,]=0
    
    for (i in 1:a[3]) {
        adata=adata+cdata[[i]]
    }
    
    if (average) {
        adata=adata/a[3]
    }
    
    return(adata)
}