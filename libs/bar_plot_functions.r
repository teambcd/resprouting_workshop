###################################################################
## Scripts for plotting                                          ##
###################################################################
plot_barchart <- function(mymat,figname,beside=TRUE,fwidth=10,
						  fheight=10,ymax=NULL,axes=TRUE,bty='n',
						  mar=par("mar"),cex_all=par("cex.axis"),legendX=38.1,
						  cex_legend=cex_all,plot_new=FALSE,
						  col=c('#000000',"#777777","#DBDBDB","#FDFDFD"),...) {
		
	if (is.null(ymax)) {
		ymax=max(mymat)*1.1
	}
	if (plot_new) {
		pdf(figname, fwidth,fheight)
		
	}

	par(mar=mar)
    barplot(mymat,beside=beside,
        legend.text=rownames(mymat),las=1,
        args.legend=list(x=legendX,y=100,bty=bty,xpd=TRUE,
        pt.cex=1.85*par("cex"),cex=par("cex"),y.intersp=2),
        ylim=c(0,ymax),axes = axes,col=col,...)
    #mtext('%',side=2,line=2)
    #dev.off()
}

calculate_width_or_plots <- function(cdata) {   
    plot_widths=c()
    a=length(cdata)
    for (i in 1:a) {    
        plot_widths=c(plot_widths,dim(cdata[[i]])[2])
    }
    
    plot_widths[2]=plot_widths[2]*0.6
    nbars=sum(plot_widths)
    plot_widths=plot_widths/nbars
    plot_widths[a+1]=0.1
    
    return(plot_widths)
    
}

setup_plot_layout <-function(a,plot_widths) {

    plot_areas=matrix(c(1:a,0),1,a+1,byrow=TRUE)
    layout(plot_areas,widths=plot_widths,heights=c(1,1,1,1))
    par(mar=c(3,2,1,0))
    
}


reorder_pfts_into_correct_order_for_plotting <- function(cdata,threshold=0) {
  
  rowname_trees=c('Apical', 'Epicorminc','Basal/Collar',
                  'Underground','non-resprouter','total')
  
  # tree data   
  cdata_trees=data.frame(
    TBE=cdata$TBE,
    TBD=cdata$TBD,
    tNE=cdata$tNE,
    tBE=cdata$tBE,
    tBD=cdata$tBD,
    "tuft tree"=cdata$tuft.trees, 
    "scaled tree"=cdata$scaled.tree,
    shrubs=cdata$shrub,
    "dwarf shrub"=cdata$dwarf.shrub)

  	rownames(cdata_trees)=rowname_trees
  
  	# grass data
  	cdata=data.frame(
    	"C3 grass"=cdata$C3.grass,
    	"C4 grass"=cdata$C4.grass,
    	"perennial forb"=cdata$perennial.forb,
    	"other herb"=cdata$other.herb)
  	rownames(cdata)=rowname_trees
  
  	NR=cdata[5,]
  	RS=100-cdata[5,]
  	rownames(RS)='resprouter'
  	tot=cdata[6,]
  
  	cdata_grasses=data.frame(
    	t(data.frame(Resprouter=t(RS),"Non-resprouter"=t(NR),
                 total=t(tot))))
  
  	cdata_trees=remove_pfts_with_less_than_n_species(cdata_trees,threshold)
  	cdata_grasses=remove_pfts_with_less_than_n_species(cdata_grasses,threshold)
  
  	list(cdata_trees,cdata_grasses)
  
}

output_table_name <- function(string) {
filename= paste(substr(filename_out,1,nchar(filename_out)-4), string,
    substr(filename_out,nchar(filename_out)-3,nchar(filename_out)),sep="")
    
    return(filename)
    
 }
