plot_visReg <- function(fdata,fit,heads,polar=rep(FALSE,length(fit[[1]])),xlabs,fig_out) {
	source("libs/install_and_source_library.r")
	source("libs/make.transparent.r")
	source("libs/climate_space_functions.r")
	install_and_source_library("visreg")
	
	print(fig_out)
	
	
	polar=logical2count(polar)
	plot_all_vars_visReg(fdata,fit,heads,polar,xlabs,
		paste(fig_out,"-all.png",sep=""))
	index=summary(fit)$coefficients[,4][-1]<0.01
	plot_all_vars_visReg(fdata,fit,heads[index],polar[index],xlabs[index],
		paste(fig_out,"-sig.png",sep=""))
}

plot_all_vars_visReg <- function(fdata,fit,heads,polar,xlabs,fig_out) {
	
	nplots=length(heads)
	dims=most_square_grid(nplots)
	png(fig_out,width=2*dims[2],height=2*dims[1],units='in',res=600)
	par(mfrow=dims,mar=c(3,3,.2,.2),oma=c(1,1,.5,.5))
	
	
	for (i in 1:nplots) plot_VR(fdata,fit,heads[i],polar[i],xlabs[[i]])
	
	dev.off()
}

logical2count <- function(x) {
	l2c <- function(i,x,y) {
		if(!x[i]) return(0)
		return(max(y)+1)
	}
	y=rep(0,length(x))
	for (i in 1:length(x)) y[i]=l2c(i,x,y)
	return(y)
}

plot_VR <- function(fdata,fit,varn,polar,xlab) {
	print(varn)
	if (polar==0) {
		plot_normal(fit,varn,xlab)
	} else {
		x=fdata[[varn]]
		varn=paste("x",polar,":y",polar,sep="")
		y=residuals(fit,'partial')[,varn]
		standard_polar_plot(x,y,z="#333333",cex_guides=2*par("cex"))
		mtext(xlab,side=1,line=2,cex=par("cex"))
		
	}
}

plot_normal <- function(fit,varn,xlab) {
	vreg=get.vreg(fit,varn)
	x=vreg[[1]]$x$D[[varn]]
	y=vreg[[varn]]$y$r
	plot(x=x,y=y,xlab="",ylab="",type='n',xaxs='i',yaxs='i')
	
	points(x=vreg[[1]]$x$D[[varn]],y=vreg[[1]]$y$r,xlab="",ylab="",pch=19,
		col="#333333",cex=0.33*par("cex"))
	
	
	mtext(xlab,side=1,line=2,cex=par("cex"))
	
	
	lapply(seq(0.05,0.95,.05),vreg.alpha.polygon,fit,varn,
		make.transparent("#999999",0.90))
		
	lines(x=vreg[[1]]$x$DD[[varn]],y=vreg[[varn]]$y$fit,ylim=c(0,140))
		
	points(x=vreg[[1]]$x$D[[varn]],y=vreg[[1]]$y$r,xlab="",ylab="",pch=19,
		col="#333333",cex=0.165*par("cex"))
	
	if (summary(fit)$coefficients[varn,1]<0) {
		side=1
	} else {
		side=3
	}
	R2Val=round((cor(x,y))^2,2)
	mtext(bquote(R^2 == .(R2Val)),line=-1.1,adj=0.05,cex=par("cex"),side=side)
}

get.vreg <- function(fit,varn,...) {
	pdf("figs/temp.pdf")
	vreg=visreg(fit,xvar=varn,...)
	dev.off()
	return(vreg)
}

vreg.alpha.polygon <- function(alpha,fit,varn,col) {
	vreg=get.vreg(fit,varn,alpha=alpha)
	x=c(vreg[[1]]$x$DD[[varn]],rev(vreg[[1]]$x$DD[[varn]]))
	y=c(vreg[[varn]]$y$lwr,rev(vreg[[varn]]$y$upr))
	polygon(x,y,col=col,border='transparent')
	
}
	
most_square_grid <-function(j) {

	for (i in 2:(j/2)) {
		k=ceiling(j/i)
		if (k<=i) return(c(i,k))
	}
	return(c(j,1))
}	

plot_VR_polar <- function() {

	

}
	
#for points:
#plot(x=yay[[1]]$x$D[,1],y=yay[[1]]$y$r)
#
#low polygon:
#plot(x=yay[[1]]$x$DD[,1],y=yay[[1]]$y$lwr,ylim=c(0,140))
#
#upper polygon:
#plot(x=yay[[1]]$x$DD[,1],y=yay[[1]]$y$upr,ylim=c(0,140))
#
#best fit:
#plot(x=yay[[1]]$x$DD[,1],y=yay[[1]]$y$fit,ylim=c(0,140))
#
#Other variable values:
#yay[[1]]$x$D[1,2:3]

