function open_and_plot_seasonal_climate_data(filename,plot_names,varname,...
    limits,mmmlimits1,mmmlimits2,...
    colours,...
    average,negative,missing_value)
    
    addpath ../libs/export_fig
    load mask2
    load Aus_land_shape
    
    
    data=open_netcdf_variable(0,filename,varname,0,0);
    
    if negative
        data=negativitisation(data);
    end
    
    data = flipdim(data,2);
    
    adata=find_annual_average(data,missing_value,average);
    mmmdata=find_max_and_min_of_highest_lowest_month...
        (data,missing_value);    
    
    
    lat=open_netcdf_variable(0,filename,'latitude',0,0);
    lon=open_netcdf_variable(0,filename,'longitude',0,0);

    rlat=[min(lat) max(lat) abs(lat(2)-lat(1))];
    rlat(1)=rlat(1)-2.5*rlat(3);

    rlon=[min(lon) max(lon) abs(lon(2)-lon(1))];
    rlon(2)=rlon(2)+5*rlon(3);

    name=[plot_names '-annual_average.tif'];
    plot_with_lims_dres_cline11(rlat,rlon,...
        adata,'same window',...
        'colour',colours,...
        'limits',limits,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[0],'ytick',[0],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[100 160 -45 -10]) 
    
    export_fig(name,'-transparent')
    
    name=[plot_names '-min_lowest_month.tif'];
    plot_with_lims_dres_cline11(rlat,rlon,...
        mmmdata(:,:,1),'same window',...
        'colour',colours,...
        'limits',mmmlimits1,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[0],'ytick',[0],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[100 160 -45 -10]) 
    
    export_fig(name,'-transparent')
    
    name=[plot_names '-max_height_month.tif'];
    plot_with_lims_dres_cline11(rlat,rlon,...
        mmmdata(:,:,2),'same window',...
        'colour',colours,...
        'limits',mmmlimits2,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[0],'ytick',[0],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[100 160 -45 -10])  
    
    export_fig(name,'-transparent')
       
    
    [pdata cdata]=convert_to_seasonal_conc_and_phase(data,missing_value);
    
    name=[plot_names '-seasonal_conc.tif'];
    plot_with_lims_dres_cline11(rlat,rlon,...
        cdata,'same window',...
        'colour',colours,...
        'limits',[0 0.01 0.1 0.2 0.5 0.8],...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[0],'ytick',[0],...
        'boundry line width',1,'axis',[100 160 -45 -10])   
    
    export_fig(name,'-transparent')
    
    name=[plot_names '-seasonal_phase.tif'];
    plot_with_lims_dres_cline11(rlat,rlon,...
        pdata,'same window',...
        'colour',{'brown','dark_red','dark_purple','blue','cyan','dark_green','brown'},...
        'limits',[0 1.5:1:11.5],'not_white_at_zero',...
        'labels',{'no data','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep',...
        'Oct','Nov','Dec'},'label_between_boundry',...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[0],'ytick',[0],...,...
        'boundry line width',1,...
        'turn off find missing values','axis',[100 160 -45 -10])   
    
    export_fig(name,'-transparent')
        
end

function data=negativitisation(data)

    mdata=max(data,[],3);
    for i=1:12
        data(:,:,i)=mdata-data(:,:,i);
    end
end