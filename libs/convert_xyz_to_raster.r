convert_xyz_to_raster <-function(filename,
    projection_info='+proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0',
	annual_or_seasonal='seasonal') {
	source("../libs/return_multiple_from_functions.r")
	source("../libs/find_seasonal_concentration_and_phase.r")
    # local parameters
    nmonths=12
   
    #librarys
    library("raster")
    source("../libs/substrRight.r")
    #open climate csv file
    if (substrRight(filename,3)=='csv') {
        cdata=read.csv(filename,header=FALSE)
    } else if (substrRight(filename,3)=='txt') {    
        cdata=read.table(filename,header=FALSE)
    }  
    
    cdata[cdata==-99.9]=NaN
    cdata[cdata==-99.8]=NaN
    cdata[cdata==-999]=NaN      
    
	a=dim(cdata)
	
	if (a[2]==15 || a[2]==14) {
		if (annual_or_seasonal=='seasonal') {
			cdata=cdata[,1:14]
		} else if(annual_or_seasonal=='min') {
			cdata=cbind(cdata[,1:2],apply(cdata[,3:14],1,min))
		} else if(annual_or_seasonal=='max') {
			cdata=cbind(cdata[,1:2],apply(cdata[,3:14],1,max))
		} else if(annual_or_seasonal=='conc') {
			cdatai=find_seasonal_concentration_and_phase_vector(cdata[,3:14])
			cdatai[[1]]=cbind(cdata[,1:2],cdatai[[1]])
			cdatai[[2]]=cbind(cdata[,1:2],cdatai[[2]])
			cdata=cdatai
		} else {
			if (a[2]==15) {
				cdata=cdata[,c(1,2,15)]
			} else {
				cdata=cbind(cdata[,1:2],apply(cdata[,3:14],1,sum))
			}
		}
	}
	
	convert2raster <- function(cdata) {
    #convert to raster
    	if (class(try(rasterFromXYZ(cdata),silent = TRUE))
        	=="try-error") {
        	cdata=convert_xyz_points_to_raster(cdata,projection_info)
    	} else {
        	cdata=rasterFromXYZ_with_lat_lon_check(cdata)
    	}     
    
    	projection(cdata)=projection_info
    	return(cdata)
    }
    
    if (class(cdata)=="list")  {
    	cdata=lapply(cdata,convert2raster)
    } else {
    	cdata=convert2raster(cdata)
    }
    return(cdata)
}

 convert_xyz_points_to_raster <-function(cdata,projection_info) {
    
    if (max(cdata[,2])>90 || min(cdata[,2])<(-90)) {
        latID=1
        lonID=2
    } else {
        latID=2
        lonID=1
    }       
    
    a=dim(cdata)
    dlat=find_min_distance(cdata[,latID])
    dlon=find_min_distance(cdata[,lonID])
    
    lat_range=c(min(cdata[,latID]),max(cdata[,latID]))-dlat
    lon_range=c(min(cdata[,lonID]),max(cdata[,lonID]))-dlon
    
    nrows=(lat_range[2]-lat_range[1])/dlat
    ncols=(lon_range[2]-lon_range[1])/dlon
    
    if (a[2]>3) {
        cdata=convert_xyz_points_to_raster_brick(cdata,lat_range,lon_range,
            nrows,ncols,projection_info,latID,lonID)
    } else {
        cdata=convert_xyz_points_to_raster_raster(cdata,lat_range,lon_range,
            nrows,ncols,projection_info,latID,lonID)
    }
    
    return(cdata)
    
}

convert_xyz_points_to_raster_raster <-  function
    (cdata,lat_range,lon_range,nrows,ncols,projection_info,
    latID,lonID) {
    
    cgrid=raster(ncol=ncols,nrow=nrows,
        xmn=lon_range[1],xmx=lon_range[2],
        ymn=lat_range[1],ymx=lat_range[2])        
    
    projection(cgrid)=projection_info

    cdata=rasterize(cdata[,lonID:latID],cgrid,cdata[,3])  
      
    return(cdata)
}
 

 convert_xyz_points_to_raster_brick <-  function
    (cdata,lat_range,lon_range,nrows,ncols,projection_info,
    latID,lonID) {
    
    a=dim(cdata)
    cgrid0=raster(ncol=ncols,nrow=nrows,
        xmn=lon_range[1],xmx=lon_range[2],
        ymn=lat_range[1],ymx=lat_range[2])        
        
    cgrid=brick(ncol=ncols,nrow=nrows,nl=a[2],
        xmn=lon_range[1],xmx=lon_range[2],
        ymn=lat_range[1],ymx=lat_range[2])        
     
    write.csv(cdata[,3:a[2]],"temp.csv")
    cdata=cdata[,1:2]
    gc()
    
    projection(cgrid)=projection_info

    for (i in 1:(a[2]-2)) {
        print(i)
        gc()
        cols=rep("NULL",13)
        cols[i+1]=NA
        mdata=data.matrix(
            read.csv("temp.csv",header=TRUE,colClasses=cols))
        
        cgrid0=rasterize(cdata[,lonID:latID],cgrid0,mdata)
        cgrid[[i]]=cgrid0
    }    
      
    return(cgrid)
}
       

find_min_distance <-function(clist) {
    
    a=length(clist)
    clist=sort(clist)
    clist=clist[2:a]-clist[1:a-1]
    clist=clist[which(clist>0)]
    dlist=min(clist,na.rm=TRUE)
    
    return(dlist)
}

rasterFromXYZ_with_lat_lon_check <- function(cdata) {
    if (max(cdata[,2])>90 || min(cdata[,2])<(-90)) {
        a=dim(cdata)
        cdata=rasterFromXYZ(cdata[,c(2,1,3:a[2])])
    } else {
        cdata=rasterFromXYZ(cdata)
    }  
    
    return(cdata)
    
}
 