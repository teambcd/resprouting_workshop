put_raster_in_netcdf_basic <- function(cdata,missing_value='NaN',
                                 filename,varname,
                                 data_source,convertion_descp,
                                 longname='NaN',units='NaN',
                                 data_description='NaN')
{   
    library("raster")
    library("ncdf4")
    
    #define attributes describing this script
    date_descp=paste("date:", Sys.Date(), sep =" ") 
    sript_name="put_raster_in_netcdf" #Doug 03/13: need to change so this is automated
    contact_descp="See bitbucket.org/teambcd/resprouting_workshop for scripts and contact information"
    
    #write to netcdf	
	writeRaster(cdata,filename,format="CDF",overwrite=TRUE,varname=varname)
}
	
	
	
put_raster_in_netcdf <- function(cdata,missing_value='NaN',
                                 filename,varname,
                                 data_source,convertion_descp,
                                 longname='NaN',units='NaN',
                                 data_description='NaN')
{   
    library("raster")
    library("ncdf")
    library("RNetCDF")
    
    #define attributes describing this script
    date_descp=paste("date:", Sys.Date(), sep =" ") 
    sript_name="put_raster_in_netcdf" #Doug 03/13: need to change so this is automated
    contact_descp="See bitbucket.org/teambcd/resprouting_workshop for scripts and contact information"
    
    #write to netcdf	
	writeRaster(cdata,filename,format="CDF",overwrite=TRUE)
    nc <- open.nc(filename, write=TRUE)
    
    #rename variables
    var.rename.nc(nc, 2, varname)    
    try(var.rename.nc(nc, 'value', 'time'),silent=TRUE )
    
    #add attributes
    if (longname!='NaN') {
        att.put.nc(nc, varname, "long_name", "NC_CHAR", longname) 
    }
    if (units!='NaN') {
        att.put.nc(nc, varname, "units", "NC_CHAR", units)
    }
    if (data_description!='NaN') {
        att.put.nc(nc, varname, "description", "NC_CHAR", data_description)
    }
    
    att.put.nc(nc, varname, "data source", "NC_CHAR", data_source)
    att.put.nc(nc, varname, "conversion information", "NC_CHAR", convertion_descp)
    att.put.nc(nc, varname, "script name", "NC_CHAR", sript_name)
    att.put.nc(nc, varname, "more information", "NC_CHAR", contact_descp)
    att.put.nc(nc, varname, "Date created", "NC_CHAR", date_descp)  
	if (missing_value!='NaN') {
        att.put.nc(nc, varname, "missing_value", 
            "NC_DOUBLE", missing_value)
    }
    try(att.put.nc(nc, "time", "long_name", "NC_CHAR", "month"),silent=TRUE )
    close.nc(nc)
	
}
    