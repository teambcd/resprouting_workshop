render.gitInfo <- function(filename, temp_file = 'temp.Rmd',
                           output_dir = 'outputs',
                           output_file = input_file, ...) {
input_file = strsplit(filename, '/')[[1]]
input_dir  = paste(head(input_file, -1), collapse = "")
input_file = tail(input_file, 1)
input_file = strsplit(input_file,'.Rmd')[[1]]
input_file = paste(input_file, '.html', sep = '')
temp_file  = paste(input_dir, temp_file, sep = '/')


file.copy(filename, temp_file, overwrite = TRUE)

library(gitProjectExtras)
cat("


git info
========

**git version number:** ", gitVersionNumber(), "


**Project URL:** ", gitRemoteURL(),
file = temp_file, sep = "", append = TRUE)

render(temp_file, output_dir = output_dir, output_file = output_file, ...)

file.remove(temp_file)

temp_file = strsplit(temp_file,'.Rmd')[[1]]
temp_file = paste(temp_file, "cache/", sep = "_")

unlink(temp_file, recursive=TRUE)
}
