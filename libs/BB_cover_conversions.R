configure_BB_cover_conversion <- function() {
	path=getwd()
	
	if (substr(path,nchar(path)-15,nchar(path))=="db_constructions") {
		path='../libs/'
	} else if (substr(path,nchar(path)-4,nchar(path))=='libs') {
		path=''
	} else {
		path='libs/'
	}
	
	source(paste( path, "return_multiple_from_functions.r",sep=""))
}
		

BB_cover_conversions <- function(BB=NULL,cover=NULL,BBtype='standard') {
	
	configure_BB_cover_conversion() 
    
    if (is.null(cover) && is.null(BB)==0) {
        c(BBlu,midlu):=BB_lookup(start='BB',BBtype)
        cover=find_cover(BBlu,midlu,BB)
        return(list(BB,cover))
    }
    
    if (is.null(BB) && is.null(cover)==0) {
        c(BBlu,ranges):=BB_lookup(start='cover',BBtype)
        BB=find_BB(BBlu,ranges,cover)
        return(list(BB,cover))
    }
}

find_cover <- function(BBlu,midlu,BB) {
    
    cover=rep(0,length(BB))
    nconverted=rep(TRUE,length(BB))
    for (i in 1:length(BBlu)) {
        test=BB==BBlu[i]
        cover[test]=midlu[i]
        nconverted[test]=FALSE
    }
	cover[nconverted]=NaN
    return(cover)
}

find_BB <- function(BBlu,ranges,cover) {
    BB=rep(0,length(cover))
    nconverted=rep(TRUE,length(BB))
    for (i in 1:dim(ranges)[1]) {
        test=((cover>ranges[i,1])+(cover<=ranges[i,2]))==2
        BB[test]=BBlu[i]
        nconverted[test]=FALSE
    }
	BB[nconverted]=NaN
    return(BB)
}
    
    
BB_lookup <- function(start='BB',BBtype='standard') {

		
        if (BBtype=='standard') {
            # adapted from Wikum & Shanholtzer  (1978)
            BB=c(0.1,1:5)
            mid=c(0.1,2.6,15.0,37.5,62.5,87.5) 
            range=t(matrix(c(
                0,         0.2,
                0.2,       5,
                5,         25,
                25,        50,
                50,        75,
                75,        100  ),2,6))
            
        } else if (BBtype=='BB7') {
            BB=c(0.1,1:6) 
            
            mid=c(0.1,0.6,3,12.5,35,62.5,87.5) 
            range=t(matrix(c(
                0,         0.2,
                0.2,       1,
                1,         5,
                5,        20,
                20,       50,
                50,        75,
                75,        100  ),2,7))
        }
        
        if (start=='BB') {
            return(list(BB,mid))
        } else {
            return(list(BB,range))
        }
        
}
    
    
    
    
# References    
#Wikum, D. a. and Shanholtzer, G. F.: Application of the Braun-Blanquet
#       cover-abundance scale for vegetation analysis in land development
#       studies, Environmental Management, 2(4), 323-329,
#       doi:10.1007/BF01866672, 1978.