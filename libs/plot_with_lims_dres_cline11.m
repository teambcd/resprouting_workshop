function figuren=plot_with_lims_dres_cline11(varargin)
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%Auther: Doug Kelley, University of Bristol
%Date: May 2009
%Contact: kd7052@bris.jeje.ac.uk
%-----------------------------------------------------------------------
%Useage:
%	plot_with_lims(z,y,x,L,C,mask1,mask2,T0,TF)
%Description:
%	plots image of z.
%-----------------------------------------------------------------------
%Inputs (needs updating)
%	Descrip:                                |Example:
%	z: matrix atht forms image.             |z=[global varaible, at .5 degree of lat vs lon]
%		If size(z)=[a b c], the z is		|A: z=global burnt fraction,
%		plotted as z(i,j)=sum(z(i,j,:))     |    with values between 0 and 1.
%                                           |B: z=change in carbon flux
%	y: y-axis. Size(y)==size(z(:,1))		|y=[-89.75:.5:89.75] for latitude.
%	x: x-axis. Size(x)==size(z(1,:))		|x=[-179.75:.5:179.75] for longditude.
%	L: limits or cut off points for each	|A: L=[0 0.001 0.01 0.02 0.05 0.1 0.2 0.5] if z is a fraction
%		colour for image z                  |B: L=[-500 -200 -100 -10 -1 0 0 1 10 100 200 500] if z is posative or negative
%	C: text description of 2 or more        |A: C={'red','red'}
%		colours, transitioning from one to 	|B: C={'red,'blue'}
%		the other. If the same hue is kept	|Also C={'red 'green' 'blue'}
%		for entire image, 2 colours should  |
%		be enetered as the same.            |
%	mask1: matriz, size(mask1)=size(z)		|mask1=landmask
%		values of 999 or -999 shades        |
%		cell dark-gray                      |
%	mask2: same as mask1, apart             |mask2=icemask
%		from shades light-grey              |
%	T0: if true, image becomes more luminuos|T0='T' or T0='F'
%		closer to a value in z of 0. If     |
%		false, normal transition is         |
%		conducted from C(1) to C(2)         |
%
%Colour choices: 'red','orange','blue','purple','green','yellow', 'red_grey', 'blue_grey',
%			'brown','grey','turquoise','blue_turquoise','white','black','dark_red'
%-----------------------------------------------------------------------
%For 'Modeling the role of fire in the global carbon cycle':
%for figure 1,2,2a as are
%for figure 3 post-processed
%Inputs:
%	z=outputs
%		for fig3; z=data(:,:,modeln)/no. models
%	y=[-89.75:.5:89,75]
%	x=[-179.75:.5:179.75]
%	L=[0 0.001 0.01 0.02 0.05 0.1 0.2 0.5]
%	C={'red','red'}
%	mask1=landmask_0k
%	mask2=icemask_0k
%	T0='T'
%
%for figure 4: post-processed
%	[nn z(i,j)]]=max(afire_frac(i,j,:)
%	y=[-89.75:.5:89,75]
%	x=[-179.75:.5:179.75]
%	L=[0.5:1:11.5]
%	C={'blue','green','red','brown'}
%	mask1=landmask_0k
%	mask2=icemask_0k
%	T0='F'	
%+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++



error(nargchk(1,inf,nargin,'struct'))

[v6,arg] = usev6plotapi(varargin{:},'-mfilename',mfilename)
[cax,arg,nargs] = axescheck(arg{:})


PROGRESS='arrange correct inputs' %#ok<NOPRT,NASGU>
%set defaults:
plat=[-90 90 .5]; %change .5
plon=[-180 180 .5];
missing_value=-999;
fill_value=1E20+2.0041e+012-12271616;
colour={'blue','red'};
tzero=1;
frac=1;
clabels=0;
mtick=0;
testo=0;
plot_from_file_test=0; %#ok<NASGU>
same_window_test=0;
line_thickness=5;
titlet={'nan'};
title_pos=1;
maintitle='nan'; %#ok<NASGU>
ytick=[-90 -60 -30 0 30 60 90];
xtick=[-180 -120 -60 0 60 120 180];
stdv=0;
slim=0;
height=0.8;
colour_bar_test=1;
figuren=0;
label_size=1;
ice_greys=0;
test_boundry=0;
boundry_size=1;
zero_colour={'white'};
colour_finder=1;
tcommon=1;
inf_colour={'nan'};
test_fmv=1;
date_line=[-180 180];
resize_text=1;
resize_texts=1;

if nargs==1
    data=arg{1};
end

targ=zeros(nargs,1,'single');

if nargs>=3
    point=1;

    if max(size(arg{1}))==3
        plat=arg{1};
        point=point+1;
        
        if max(size(arg{2}))==3
            plon=arg{2};
            point=point+1;
        end
    end
    if ischar(arg{point})==1
        count=0; start=0;
        testp=0;
        plot_from_file_test=1; %#ok<NASGU>
        for i=point+2:nargs
            if ischar(arg{i})==0
                continue
            end
            if isequal('nc_start',char(arg{i}))==1
                start=arg{i+1};
                testp=testp+1;
                continue
            end
            if isequal('nc_count',char(arg{i}))==1
                count=arg{i+1};
                testp=testp+1;
                continue
            end
            if testp==2
                break
            end
        end        
        [data]=open_netcdf_variable(0,arg{point},arg{point+1},start,count);
        point=point+2;
    else
           data=arg{point};
     
    end


    for i=point:nargs
        if ischar(arg{i})==0 || targ(i)==1
            continue
        end
        if isequal('missing value',char(arg{i}))==1
            missing_value=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('colour',char(arg{i}))==1
            colour=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('colour vector',char(arg{i}))==1
            colour=arg{i+1};
            colour_finder=0;
            targ(i:i+1)=1;
            continue
        end
        if isequal('not_white_at_zero',char(arg{i}))==1
            tzero=0;
            targ(i)=1;
            continue
        end
        if isequal('data_is_fraction',char(arg{i}))==1
            frac=1;
            targ(i)=1;
            continue
        end
        if isequal('labels',char(arg{i}))==1
            clabels=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('label_between_boundry',char(arg{i}))==1
            mtick=0.5;
            targ(i)=1;
            continue
        end
        if isequal('set_ocean_to_fill',char(arg{i}))==1
            testo=1;
            targ(i)=1;
            continue
        end
        if isequal('close previous',char(arg{i}))==1
            close all
            targ(i)=1;
            continue
        end
        if isequal('same window',char(arg{i}))==1
            same_window_test=1;
            targ(i)=1;
            continue
        end
        if isequal('line thickness',char(arg{i}))==1
            line_thickness=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('title',char(arg{i}))            
            titlet=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('title position',char(arg{i}))
            for cyc=1
                if isequal('bottom left',char(arg{i+1}))
                    title_pos=2;
                    continue
                end
                if isequal('middle left',char(arg{i+1}))
                    title_pos=3;
                    continue
                end
                if isequal('top left',char(arg{i+1}))
                    title_pos=4;
                    continue
                end
                if isequal('top middle',char(arg{i+1}))
                    title_pos=5;
                    continue
                end
                if isequal('top right',char(arg{i+1}))
                    title_pos=6;
                    continue
                end
                if isequal('middle right',char(arg{i+1}))
                    title_pos=7;
                    continue
                end
                if isequal('bottom right',char(arg{i+1}))
                    title_pos=8;
                    continue
                end
            end
            targ(i:i+1)=1;
            continue
        end
        if isequal('main title',char(arg{i}))==1
            maintitle=char(arg{i+1}); %#ok<NASGU>
            targ(i:i+1)=1;
            continue
        end
        if isequal('xtick',char(arg{i}))==1
            ax=max(size(arg{i+1}));
            xtick=zeros(ax,1,'single');
            xtick(:,1)=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('ytick',char(arg{i}))==1
            ax=max(size(arg{i+1}));
            ytick=zeros(ax,1,'single');
            ytick(:,1)=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('figure number',char(arg{i}))==1 || ...
                isequal('figure no',char(arg{i})) || ...
                isequal('fign',char(arg{i}))
            figuren=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('label size',char(arg{i}))==1
            label_size=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('icemask greys',char(arg{i}))
            ice_greys=1;
            targ(i)=1;
            continue
        end
        if isequal('boundry lines',char(arg{i}))
            test_boundry=test_boundry+1;
            line_thickness=0;
            if test_boundry==3
                boundry3=arg{i+1};
            else
                if test_boundry==2
                    boundry2=arg{i+1};
                else
                    if test_boundry==1
                        boundry1=arg{i+1};
                    end
                end
            end
            targ(i:i+1)=1;
            continue
        end 
        if isequal('boundry line width',char(arg{i}))
            
            boundry_size=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('colour bar off',char(arg{i}))
            colour_bar_test=0;
            targ(i)=1;
            continue
        end
        if isequal('colour at zero',char(arg{i}))
            zero_colour=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('infinate colour',char(arg{i}))
            inf_colour=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('turn off find missing values',char(arg{i})) || ...
            isequal('turn_off find missing values',char(arg{i})) || ...
            isequal('skip find missing values',char(arg{i}))
            test_fmv=0;
            targ(i)=1;
            continue
        end
        if isequal('set date line',char(arg{i}))
            date_line=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
        if isequal('resize text',char(arg{i}))
            resize_text=arg{i+1};
            resize_texts=arg{i+1};
            targ(i:i+1)=1;
            continue
        end
    end

end

[a b c]=size(data);


data=single(data);
if test_fmv==1
    PROGRESS='Finding missing values' %#ok<NASGU,NOPRT>
    threshold0=5;
    threshold=threshold0;
    for i=1:a
        if threshold<100*i/a
            percent=threshold %#ok<NASGU,NOPRT>
            threshold=threshold+threshold0;
        end
        for j=1:b
            for k=1:c
                if data(i,j,k)==missing_value || data(i,j,k)==fill_value...
                        || data(i,j,k)>9E9
                    data(i,j,k)=-999;
                end
            end
        end
    end
end



if max(size(plon))>3
    axisv(1:2)=[plon(1) plon(max(size(plon)))];
else
    axisv(1:2)=[plon(1) plon(2)];
end
if max(size(plat))>3
    axisv(3:4)=[plat(1) plat(max(size(plat)))];
else
    axisv(3:4)=[plat(1) plat(2)];
end
avisvi=axisv;
%set final inputs
for i=1:nargs
    if targ(i)==1 || ischar(arg{i})==0
        continue
    end
    
    if isequal('minus',char(arg{i}))
        PROGRESS='calculation anomolies' %#ok<NASGU,NOPRT>        
        if isequal('when either has value',char(arg{i+2}))
            tcommon=0;
            targ(i+2)=1;
        end
        data=anomolie(data,arg{i+1},tcommon,missing_value);
        targ(i:i+1)=1;
        continue
    end
    if isequal('plus',char(arg{i}))
        PROGRESS='calculating sum' %#ok<NOPRT,NASGU>
        data=plus_d(data,arg{i+1},missing_value);
        targ(i:i+1)=1;
        continue
    end
	if isequal('correlate',char(arg{i}))
		PROGRESS='performing correlation' %#ok<NOPRT,NASGU>
		if isequal('two dimensional',arg{i+1}) || isequal('two dimensional',arg{i+2})
			if isequal('order sequance',arg{i+1}) || isequal('order sequance',arg{i+2})
				data=correlate(data,arg{i+3},missing_value,1,1);
				targ{i:i+3}=1;
			else			
				data=correlate(data,arg{i+2},missing_value,1,0);
				targ{i:i+2}=1;
			end
		else
			if isequal('order sequance',arg{i+1})
				data=correlate(data,arg{i+2},missing_value,0,1);
				targ{i:i+2}=1;
			else
				data=correlate(data,arg{i+1},missing_value,0,0);
				targ(i:i+1)=1;
			end
		end
		continue
	end
    if isequal('std',char(arg{i}))==1 || ...
            isequal('standard deviation',char(arg{i}))==1
        PROGRESS='calculating standard deviation' %#ok<NOPRT,NASGU>
        if isequal('supplied',char(arg{i+1}))
            stdv=arg{i+2};
            slim=arg{i+3};
            targ(i:i+3)=0;
        else
            [data,stdv]=standard_deviaton_cal(data,-999);
            slim=arg{i+1};
            targ(i:i+1)=0;
        end
        height=max([1-(0.2+0.05*max(size(slim))) 0.5]);
        colour_bar_test=0;
        continue
    end
    if isequal('sum dimension',char(arg{i}))==1
        PROGRESS='summing dimension' %#ok<NASGU,NOPRT>
        data=sum_data(data,0);            
        targ(i)=1;
        continue
    end
    if isequal('average dimension',char(arg{i}))==1
        data=sum_data(data,1);               
        targ(i)=1;
        continue
    end
    if isequal('sum dimension',char(arg{i}))==1
        PROGRESS='summing dimensions' %#ok<NASGU,NOPRT>
        
        data=sum_data_all(data,0);
        
        targ(i)=1;
        continue
    end
    if isequal('average dimensions',char(arg{i}))==1
        data=sum_data_all(data,1);
        targ(i)=1;
        continue
    end
    if isequal('annual cycle',char(arg{i}))==1
        data=annual_cycle(data,nan);
        targ(i)=1;
        continue
    end
    if isequal('swop longitude centre',char(arg{i}))==1
        [a b c]=size(data); %#ok<NASGU>
        datai=data;
        data(1:ceil(a/2),:,:)=datai(floor(1+a/2):a,:,:);
        data(floor(1+a/2):a,:,:)=datai(1:ceil(a/2),:,:);
        targ(i)=1;
        continue
    end
    if isequal('axis',char(arg{i}))
        axisv=arg{i+1};
        targ(i:i+1)=1;
        if max(size(arg))>=i+2 && isequal('cross date line',char(arg{i+2}))
            if max(size(arg))>=i+3 && isequal('left',char(arg{i+3}))
                axisvi(1)=date_line(1)+(axisv(1)-date_line(2));
                targ(i+2:i+3)=1;
            else
                axisvi(2)=date_line(2)+(axisv(2)-date_line(1));
                targ(i+2)=1;
            end
        end
        continue
    end
    if isequal('peak month',char(arg{i}))
        PROGRESS='Finding peak month' %#ok<NOPRT,NASGU>
        size(data)
        data=find_peak_month(data);
        targ(i)=1;
        continue
    end
    if isequal('divide plot',char(arg{i}))
        PROGRESS='dividing plot windows' %#ok<NOPRT,NASGU>

        di=arg{i+1};
        [a b c]=size(data);
        ci=ceil(c/di);        
        
        datai=zeros(a,b,ci,di,'single');
        
        p=0;
        for k=1:ci
            for l=1:di
                p=p+1;
                if p>c
                    break
                end
                datai(:,:,k,l)=data(:,:,p);
            end
        end
        
        clear data
        data=datai;
        clear datai
        [a b c d]=size(data); %#ok<NASGU,ASGLU>
    end
end

mask_test=0;
if nargs>3
for i=1:nargs
    if targ(i)==1 || ischar(arg{i})==0
        continue
    end
    if isequal('landmask',char(arg{i}))==1
        landmask=arg{i+1};
        mask_test=1;
        targ(i:i+1)=1;
        break
    end
end
end
if mask_test==0
    landmask=zeros(size(data(:,:,1)));
end

mask_test=0;
if nargs>3
for i=1:nargs
    if targ(i)==1 || ischar(arg{i})==0
        continue
    end
    if isequal('icemask',char(arg{i}))==1
        icemask=arg{i+1};
        mask_test=1;
        targ(i:i+1)=1;
        break
    end
end
end
if mask_test==0
    icemask=zeros(size(data(:,:,1)));
end

lim_test=0;
if nargs>3
for i=1:nargs
    if targ(i)==1 || ischar(arg{i})==0
        continue
    end
    if isequal('limits',char(arg{i}))==1
        lim=arg{i+1};
        lim_test=1;
        targ(i:i+1)=1; %#ok<NASGU>
        break
    end
end
end

if lim_test==0    
    [a b c]=size(data); %#ok<NASGU>
    maxmin=[0 0];
    for i=1:a
        for j=1:b
            if data(i,j,1)~=-999 && data(i,j,1)<9E9
                if maxmin(1)<max(data(i,j,:))
                    maxmin(1)=max(data(i,j,:));
                    continue
                end
                if maxmin(2)>min(data(i,j,:))
                    maxmin(2)=min(data(i,j,:));
                end
            end
        end
    end
    lim=maxmin(2):(maxmin(1)-maxmin(2))/9:maxmin(1);
end

% figure (101); imagesc(landmask(:,:,1,1));
[a b c d]=size(data);

if same_window_test~=1 && (c>1 || d>1)
    Check='number of plots:' %#ok<NOPRT,NASGU>
    c*d %#ok<NOPRT>
    Check='Push any key if this is okay' %#ok<NOPRT,NASGU>
    pause
end

if same_window_test==1
    cc=(0.033/c);dd=(0.033/d);
    if c==1
        cc=0;
    end
    if d==1
        dd=0;
    end
    swindow=[-cc+0.8/max(c) -dd+height/max(d)];
else
    swindow=[0.8 height];
end


[at bt]=size(titlet);

if at==1 && bt==1 && isequal(char(titlet),'nan')
    at=0;
else
    if at==1 || bt==1
    at=max([at bt]);
    bt=1;
    end
end
p=0;

% if max(size(plon))==3
%     plon_mid=(plon(2)+plon(1))/2;
%     plat_bot=0.975*plat(1)+0.025*plat(2);
% else
%     plon_mid=max(size(plon)); plon_mid=plon(floor(plon_mid/2));
%     plat_bot=max(size(plat)); plat_bot=plat(plat_bot-floor(plat_bot/10));
% end
plon_left=axisv(2)*0.96+axisv(1)*0.04;
plon_mid=(axisv(2)+axisv(1))/2;
plon_right=axisv(2)*0.04+axisv(1)*0.96;
plat_bot=0.93*axisv(3)+0.07*axisv(4);
plat_mid=(axisv(4)+axisv(3))/2;
plat_top=0.04*axisv(3)+0.96*axisv(4);

for i=c:-1:1
    for j=1:d
        if sum(sum(data(:,:,i,j)))==0
            continue
        end
        if same_window_test==1
            if c>1 || d>1
                tick_test=0;
            else
                tick_test=1;
            end
        else
            tick_test=1;
            figuren=0;
        end
        p=p+1;
        if same_window_test==1
            pwindow=[0.05+(0.8)*(i-1)/c  0.1+(height)*(d-j)/d swindow];
        else
            
            pwindow=[0.05 0.1 swindow];
        end
        if sum(slim)>0
            pwindow(2)=pwindow(2)+0.8-height;
        end
        
        [tla tlb tlc]=size(lim);
        if tla>1 && tlb>1 && tlc==1
            limin=lim(p,:);
        else
            if tla>1 && tlb>1 && tlc>1
                limin=lim(i,j,:);
            else
                limin=lim;
            end
        end
        
        [tla tlb tlc]=size(colour);
        if tla>1 && tlb>1 && tlc==1 && colour_finder==1
            colourin=colour{p,:};
        else
            if tla>1 && tlb>1 && tlc>1
                colourin=colour{i,j,:};
            else
                colourin=colour;
            end
        end
        
        [resize_text, figuren,inf_colour,colourin]=...
            gubbins(data(:,:,i,j),stdv,plat,plon,limin,slim,...
            colourin,zero_colour,colour_finder,landmask',icemask,...
            tzero,testo,frac,clabels,mtick,pwindow,colour_bar_test,...
            figuren,tick_test,...
            [c d],[i j],line_thickness,resize_text,resize_texts,xtick,ytick,height,...
            label_size,ice_greys,inf_colour);
        
        if test_boundry>=1            
            plot_boundries(boundry1,axisv,1.5*boundry_size);
            if test_boundry>=2
                plot_boundries(boundry2,axisv,boundry_size);
                if test_boundry==3
                    plot_boundries(boundry3,axisv,0.5*boundry_size);
                end
            end
        end
        axis(axisv)
        
        if at~=0
            
            
            if at>=c || bt>=d
                testtitle=title(' ');
                ptitle=get(testtitle,'Position');
                ptitle(2)=ptitle(2)*(-.9);
%                 title(titlet{i,j},'Position',ptitle);%+[.4 .3]);%[.4*568*c .3*502*d]);
                for cyc=1
                    if title_pos==1
                        text(plon_mid,plat_bot,titlet{i,j},...
                            'HorizontalAlignment','center',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                    if title_pos==8
                        text(plon_left,plat_bot,titlet{i,j},...
                            'HorizontalAlignment','right',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                    if title_pos==2
                        text(plon_right,plat_bot,titlet{i,j},...
                            'HorizontalAlignment','left',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                    if title_pos==3
                        text(plon_right,plat_mid,titlet{i,j},...
                            'HorizontalAlignment','left',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                    if title_pos==4
                        text(plon_right,plat_top,titlet{i,j},...
                            'HorizontalAlignment','left',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                    if title_pos==5
                        text(plon_mid,plat_top,titlet{i,j},...
                            'HorizontalAlignment','center',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                    if title_pos==6
                        text(plon_left,plat_top,titlet{i,j},...
                            'HorizontalAlignment','right',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                    if title_pos==7
                        text(plon_left,plat_mid,titlet{i,j},...
                            'HorizontalAlignment','right',...
                            'FontSize',resize_text*10,...
                            'BackgroundColor',[1 1 1])
                        continue
                    end
                end
            else
                if bt==1 && at>c*d
%                     title(titlet{p})
                    text(plon_mid,plat_bot,titlet{p},...
                        'HorizontalAlignment','center',...
                        'FontSize',resize_text*10)
                else
                    title(titlet(1,1))
                end
            end
        end
            
        
    end
end

if  isequal(maintitle,'nan')==0
    if max(size(slim))>1
        text(axisv(1),(d-1)*(axisv(4)-axisv(3))+axisv(4)+0.05*(axisv(4)-axisv(3)),maintitle,...
        'FontSize',resize_text*12);
    else
        text(axisv(1),axisv(3)+0.15*(axisv(3)-axisv(4)),maintitle,...
            'FontSize',resize_text*12)
%         text(plon(1),plat(1)+0.15*(plat(1)-plat(2)),'hellooooooooooo!',...
%             'FontSize',resize_text*12)
    end
end

if max(size(slim))>1
    a=max(size(lim));
    ai=a;
    for i=2:a
        if i>ai
            break
        end
%         if lim(i)==lim(i-1)
%             lim(1:i-1)
%             lim(i:a)
%             
%             lim(i-1:a-1)=lim(i:a);
%             a=a-1;
%             ai=a;
%         end
    end
    limi=lim;
    lim=zeros(1,1,'single');
    lim(1:a)=limi(1:a);
    b=max(size(slim));
    smat=zeros(a+1,b+1,2,'single');
    
    smat(1,:,1)=0.5*(lim(1)+(2*lim(1)-lim(2)));
    smat(a+1,:,1)=0.5*(lim(a)+(2*lim(a)-lim(a-1)));
    for i=2:a
        smat(i,:,1)=0.5*(lim(i)+lim(i-1));
    end
    
    smat(:,1,2)=0.5*(slim(1)+(2*lim(1)-slim(2)));
    smat(:,b+1,2)=0.5*(slim(b)+(2*slim(b)-slim(b-1)));
    for j=2:b
        smat(:,j,2)=0.5*(slim(j)+slim(j-1));
    end
    pwindow=[0.1 0.05 0.7 1-0.20-height];
    if b>a
        gubbins(smat(:,:,1)',smat(:,:,2)',smat(:,1,1),smat(1,:,2),lim,slim,...
                colour,1,zeros(size(smat(:,:,1)),'int8'),zeros(size(smat(:,:,1)),'int8')',...
                tzero,testo,frac,clabels,mtick,pwindow,0,figuren,tick_test,...
                [1 1],[1 1],line_thickness,resize_text*.75,resize_texts,slim,lim,height,...
                label_size,ice_greys,{'nan'});
    else
        
        gubbins(smat(:,:,1),smat(:,:,2),smat(1,:,2),smat(:,1,1),lim,slim,...
                colour,zero_colour,1,zeros(size(smat(:,:,1)),'int8')',zeros(size(smat(:,:,1)),'int8'),...
                tzero,testo,frac,clabels,mtick,pwindow,0,figuren,tick_test,...
                [1 1],[1 1],line_thickness,...
                resize_text*min([1 .75*(1+0.25*d)]),resize_texts,lim,slim,height,...
                label_size,ice_greys,{'nan'});
            
    end
end
axis off;

end

function [resize_text,figuren,inf_colour_out,colour_name_out]=...
    gubbins(data,stdv,plat,plon,lim,slim,...
    colour,zero_colour,colour_finder,landmask,icemask,...
    tzero,testo,frac,clabels,mtick,pwindow,colour_bar_test,figuren,...
    tick_test,np,ip,line_thickness,resize_text,resize_texts,xtick,ytick,height,...
    label_size,ice_greys,inf_colour)

%data(1:360,:,:)=datai(361:720,:,:);data(361:720,:,:)=datai(1:360,:,:);

if isequal(tzero,'T')==1 || isequal(tzero,'true')==1 || ...
        isequal(tzero,'TRUE') || tzero==1
	tzero=1;
else
	tzero=0;
end

% data=anomolie(data(:,:,:,1),data(:,:,:,2),1);
%  data=sum_data_all(data,1);
%data=anomolie_percentage(data(:,:,:,1),data(:,:,:,2),1);
% data=wood_grass(data,'grass');
%data=sum_data(data,1);

% data=data';
PROGRESS='Projecting data onto landmask' %#ok<NOPRT,NASGU>
data=refit_to_mask(data,landmask',frac);
stdv=refit_to_mask(stdv,landmask',frac);
PROGRESS='Projecting icemask onto landmask' %#ok<NOPRT,NASGU>
icemask=refit_to_mask(icemask,landmask,frac);


[a b c]=size(data); %#ok<NASGU>

PROGRESS='Finding data-range for plot' %#ok<NOPRT,NASGU>
pi=plon(3)+a*([plon(1) plon(2)]+180)/360;
pj=plat(3)+b*([plat(1) plat(2)]+90)/180;

pi=round(pi); pj=round(pj);

if pi(1)<1
    pi(1)=1;
end

if pi(2)>a
    pi(2)=a;
end

if pj(1)<1
    pj(1)=1;
end

if pj(2)>b
    pj(2)=b;
end
pi=[1 a];
pj=[1 b];

datai=zeros(1,1,'int8'); %#ok<NASGU>

datai=data(pi(1):pi(2),pj(1):pj(2));

data=datai;
datai=zeros(1,1,'int8'); %#ok<NASGU>

data=data';
stdv=stdv';

landmaski=landmask';icemaski=icemask';%masklinei=maskline';

landmask=landmaski(pi(1):pi(2),pj(1):pj(2))'; landmaski=zeros(1,1,'int8'); ...
    %#ok<NASGU>
icemask=icemaski(pi(1):pi(2),pj(1):pj(2))';icemaski=zeros(1,1,'int8');...
    %#ok<NASGU>

PROGRESS='Drawing coast line' %#ok<NOPRT,NASGU>

if line_thickness>1
    maskline=make_mask_a_line(landmask,line_thickness);
else
    maskline=zeros(size(landmask));
end

[a b c]=size(data); %#ok<NASGU>

PROGRESS='Assigning data to limits' %#ok<NOPRT,NASGU>

pdata=convert_data_to_numbers(data,landmask,icemask,maskline,lim,testo,...
    ice_greys);

if max(size(slim))>1
    
    stdv=convert_data_to_numbers(stdv,landmask,icemask,maskline,slim,...
        testo,ice_greys);
end





PROGRESS='Assigning colours' %#ok<NOPRT,NASGU>

[pdata2,colour,zero_colour,colour_name_out]=find_colour(pdata,colour,zero_colour,lim,tzero,colour_finder);

inf_colour_out=inf_colour;

if isequal(inf_colour{1},'nan')==0 && isnan(inf_colour)==0
    [cname,cval]=colour_vectors;
    ac=max(size(cname));
    inf_test=1;
    while inf_test==1
        inf_test=0;
        for i=1:ac
            if isequal(inf_colour,cname{i})
                clear inf_colour
                inf_colour=cval(i,:);
                break
            end
            if i==ac
                error='Infinate colour name is not defined' %#ok<NOPRT,NASGU>
                old_colour=inf_colour %#ok<NOPRT,NASGU>
                clear old_colour
                new_inf_colour=input('try another one:');
                inf_colour={new_inf_colour};
                inf_colour_out=inf_colour;
                inf_test=1;
            end
        end
    end
    
    clear cname cval
    for i=1:a
        for j=1:b
            if icemask(i,j)~=-999 && landmask(i,j)~=-999 ...
                    && isinf(data(i,j))==1
                pdata2(i,j,:)=inf_colour;
            end
        end
    end
end

clear data
                

if max(size(slim))>1
    PROGRESS='Assigning deviation shade' %#ok<NASGU,NOPRT>
    pdata2=alter_colour(pdata2,stdv);
end

if ice_greys==1
    PROGRESS='graying icemask' %#ok<NOPRT,NASGU>
    for i=1:a
        for j=1:b
            if icemask(i,j)==-999
                if pdata(i,j)==0
                    pdata2(i,j,:)=0.9;
                else
                    if ceil((i+j+round(line_thickness))/round(2*line_thickness))/2==...
                        round(ceil((i+j)/round(2*line_thickness))/2)...
                        && ceil((i+j)/round(2*line_thickness))/2==...
                        round(ceil((i+j-1)/round(2*line_thickness))/2)                        
                        
                        pdata2(i,j,:)=0.9;
                    end
%                     [rgb,hsl]=rgb_hsl_and_back(pdata2(i,j,:),0); %#ok<ASGLU>
% 
%                     hsl(2)=hsl(2)*0.3;
% 
%                     pdata2(i,j,:)=rgb_hsl_and_back(0,hsl);
                end

            end
        end
    end
end

PROGRESS='Plotting' %#ok<NOPRT,NASGU>
%  xdata=-180+180/b:360/b:180-180/b;
%  ydata= -90+90/a: 180/a: 90-90/a;
% [a b]=size(landmask);
% ydata=plat(1):180/a:plat(2);
% xdata=plon(1):360/b:plon(2);
if max(size(plat))==3 && max(size(plon))==3
    ydata=plat(1):plat(3):plat(2);
    xdata=plon(1):plon(3):plon(2);
    gap=[norm(plon(2)-plon(1))+plon(3) norm(plat(2)-plat(1))+plat(3)]/180;
else
    ydata=plat;
    xdata=plon;
    a=max(size(plon)); b=max(size(plat));
    gap=[norm(plon(a)-plon(1))+90*norm(plon(a)-plon(1))/a norm(plat(b)-plat(1))+100*norm(plat(b)-plat(1))/b]/180;

end



a=max(size(xdata)); b=max(size(ydata));
[resize_text,figuren]=ploty(xdata, ydata, pdata2,lim,...
    colour,zero_colour,inf_colour,tzero,...
    clabels,mtick,pwindow,figuren,colour_bar_test,tick_test,np,ip,...
    resize_text,resize_texts,xtick,ytick,height,label_size);

    axis([xdata(1)-gap(1) xdata(a)+gap(1) ydata(1)-gap(2) ydata(b)+gap(2)]);

    % axis([-20 50 -inf inf])


end

function out=sum_data(in,tav)  %#ok<DEFNU>

[a b c d e]=size(in);

if e>1
    out=zeros(a,b,c,d,'single');
else
    if d>1
        out=zeros(a,b,c,'single');
    else
        if c>1
            out=zeros(a,b,'single');
        else
            out=in;
        end
    end
end


if c>1 || d>1 || e>1
    threshold0=2;
    threshold=threshold0;
    for i=1:a
        if 100*i/a>threshold
            percent=threshold %#ok<NASGU,NOPRT>
            threshold=threshold+threshold0;
        end
        for j=1:b
            if sum(sum(sum(in(i,j,:,:,:)==-999)))==0
                if e>1
                    for k=1:c                
                        for l=1:d
                            out(i,j,k,l)=sum(in(i,j,k,l,:));
                        end
                    end
                else
                    if d>1
                        for k=1:c
                            out(i,j,k)=sum(in(i,j,k,:));
                        end
                    else
                        out(i,j)=sum(in(i,j,:)); 
                    end
                end
            end
        end
    end
else
    out=in;
end

if tav==1
    if e>1
        out=out/e;
    else
        if d>1
            out=out/d;
        else
            out=out/c;
        end
    end
end



% [a b c d]=size(in);
% 
% if d>1
%     ini=zeros(a,b,d,'single');
%     for i=1:a
%         for j=1:b
%             for k=1:d
%                 ini(i,j,k)=sum(in(i,j,:,k))/d;
%             end
%         end
%     end
%     in=ini;
%     [a b c]=size(in);
% end
% 
% if c>1
%     out=zeros(a,b,'single');
%     for i=1:a
%         for j=1:b
%             for k=1:c
%                 if isnan(in(i,j,k))~=1
%                     out(i,j)=out(i,j)+in(i,j,k);
%                 end
%             end
%         end
%     end
% else
%     out=in;
% end
% 
% if tav==1
%     out=out/c;
% end

end

function out=sum_data_all(in,tav) 

[a b c d e]=size(in);

out=zeros(a,b);

for i=1:a
    for j=1:b
        if norm(in(i,j,1,1,1))<9E9 && norm(in(i,j,1,1,1))~=999
            out(i,j)=sum(sum(sum(in(i,j,:,:,:))));
        else
            out(i,j)=in(i,j,1,1,1);
        end
    end
end

if tav==1
    out=out/(c*d*e);
else
    if tav==2
        out=100*out/(c*d*e);
    end
end

end

function out=annual_cycle(in,tlength) %#ok<INUSD>
%tlength will help find which dimension is month at some piont, and not
%automatically assume it's the 3rd dimension
[a b c d e]=size(in);

if e>1
    error='Too many dimension for seasonal cycleing' %#ok<NASGU,NOPRT>
    stop
end

out=zeros(a,b,c);

for i=1:a
    for j=1:b
        if in(i,j,1,1)~=-999
            for k=1:c
                out(i,j,k)=sum(in(i,j,k))/d;
            end
        else
            out(i,j,:)=in(i,j,:,1);
        end
    end
end

end

function out=anomolie_percentage(in1,in2,tsum) %#ok<DEFNU>

in(:,:,:,1)=single(in1(:,:,:));
in(:,:,:,2)=single(in2(:,:,:));
in1=zeros(1,1);in2=zeros(1,1); %#ok<NASGU>
[a b c d]=size(in); %#ok<NASGU>
inter=zeros(2,1);
if tsum==1
    out=single(zeros(a,b));
else
    out=single(zeros(a,b,c));
end

for i=1:a
    for j=1:b
        if norm(in(i,j,1,1))<9E9
            if tsum==1
                inter(1)=sum(in(i,j,:,1));
                inter(2)=sum(in(i,j,:,2));
                if inter(1)~=0 || inter(2)~=0
                    out(i,j)=(inter(1)-inter(2))/max(inter(:));
                    
                end
                
            else
                for k=1:c
                    
                    if sum(in(i,j,k,:))>0
                        out(i,j,k)=(in(i,j,k,1)-in(i,j,k,2))/max(in(i,j,k,:));
                    
                    end
                end
            end
        else
            out(i,j,:)=in(i,j,1,1);
        end

    end   
end
out=out*100;
end

function out=anomolie(in1,in2,tcommon,mv)

[a b c d e]=size(in1);

[nn nn c2 d2 e2]=size(in2);

out=zeros(a,b,c,d,e,'single');

threshold0=10;
threshold=threshold0;
for i=1:a
    if 100*i/a>threshold
        percent=threshold %#ok<NASGU,NOPRT>
        threshold=threshold+threshold0;
    end
    for j=1:b
        if tcommon==1
            test=(norm(in1(i,j,1,1))==mv || norm(in1(i,j,1,1))==999 || in1(i,j,1,1)>9E9...
                || norm(in2(i,j,1,1))==mv || norm(in2(i,j,1,1))==999 || in2(i,j,1,1)>9E9);
        else
            test=((norm(in1(i,j,1,1))==mv || norm(in1(i,j,1,1))==999 || in1(i,j,1,1)>9E9)...
                && (norm(in2(i,j,1,1))==mv || norm(in2(i,j,1,1))==999 || in2(i,j,1,1)>9E9));
            
        end
        if test==1
            out(i,j,:,:)=in1(i,j,1,1);
        else
            if norm(in1(i,j,1,1))==mv || norm(in1(i,j,1,1))==999 || in1(i,j,1,1)>9E9
                in1(i,j,:,:)=0;
            end
            if norm(in2(i,j,1,1))==mv || norm(in2(i,j,1,1))==999 || in2(i,j,1,1)>9E9
                in2(i,j,:,:)=0;
            end
            for k=1:c
                for m=1:d
                    for n=1:e
                        if c2==1 && d2==1 && e2==1
                            out(i,j,k,m,n)=in1(i,j,k,m,n)-in2(i,j);
                        else
                            if d2==1 && e2==1
                                out(i,j,k,m,n)=in1(i,j,k,m,n)-in2(i,j,k);
                            else
                                if e2==1
                                    out(i,j,k,m,n)=in1(i,j,k,m,n)-...
                                        in2(i,j,k,m);
                                else
                                    out(i,j,k,m,n)=in1(i,j,k,m,n)-...
                                        in2(i,j,k,m,n);
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
end

function out=plus_d(in1,in2,mv)

[a b c d e]=size(in1);

out=zeros(a,b,c,d,e,'single');

threshold0=10;
threshold=threshold0;
for i=1:a
    if 100*i/a>threshold
        percent=threshold %#ok<NASGU,NOPRT>
        threshold=threshold+threshold0;
    end
    for j=1:b
        if sum(sum(sum(in1(i,j,:,:,:)==mv)))==0 && sum(sum(sum(in1(i,j,:,:,:)==-999)))==0
            test(i,j)=1;
            for k=1:c
                for l=1:d
                    for m=1:e
                        out(i,j,k,l,m)=in1(i,j,k,l,m)+in2(i,j,k,l,m);
                    end
                end
            end
        else
            out(i,j,:,:,:)=-999;
        end
    end
end
end

function out=correlate(in1,in2,mv,t2d,tod)

[a b c d e]=size(in1);

if c==1 && d==1 && e==1
    error='not enough dimensions is array' %#ok<NOPRT,NASGU>
end

[aa bb cc dd ee]=size(in2);

if (a~=aa || b~=bb || c~=cc || d~=dd || e~=ee) && (cc~=e || dd~=1 || ee~=1)
	error='for correlation function, size of arrays must match' %#ok<NASGU,NOPRT>
	size_of_array_1=size(in1) %#ok<NASGU,NOPRT>
	size_of_array_2=size(in2) %#ok<NASGU,NOPRT>
	stop
end

for i=1:aa
    for j=1:bb
        for k=1:cc
            if in2(i,j,k)==0
                in1(i,j,:,:,k)=0;
            end
        end
    end
end

% if cc==e && dd==1 && ee==1
%     temp=in2;
%     in2=zeros(a,b,c,d,e,'single');
%     for i=1:c
%         for j=1:d
%             in2(:,:,i,j,:)=temp(:,:,:);
%         end
%     end
% end


% clear aa bb cc dd ee



if t2d==1
	temp=in1;
	clear in1
	in1=zeros(a,b,c*d*e,'single');
	p=0;
	for i=1:c
		for j=1:d
			for k=1:e
				p=p+1;
				in1(:,:,p)=temp(:,:,i,j,k);
			end
		end
	end
	temp=in2;
	in2=zeros(a,b,c*d*e,'single');
	p=0;
	for i=1:c
		for j=1:d
			for k=1:e
				p=p+1;
				in2(:,:,i,j,k)=temp(:,:,i,j,k);
			end
		end
	end
	clear temp
end

[a b c d e]=size(in1);
temp1=in1;
temp2=in2;
out=zeros(a,b,c,d,'single');
if tod==1
	for i=1:a
		for j=1:b
            if in1(i,j,1)~=-999 || in1(i,j,1)<9E9 || in1(i,j)~=mv
                for k=1:c
                    if d>1 && e>1
                        for l=1:d
                            if e>1
                                for m=1:e
                                    p=[0 0];
                                    for n=1:e
                                        if n==m
                                            continue
                                        end
                                        if temp1(i,j,k,l,m)>temp1(i,j,k,l,n)
                                            p(1)=p(1)+1;
                                        end
                                        if temp2(i,j,k,l,m)>temp2(i,j,k,l,n)
                                            p(2)=p(2)+1;
                                        end
                                    end
                                    in1(i,j,k,l,m)=p(1);
                                    in2(i,j,k,l,m)=p(2);
                                end
                            else
                                p=[0 0];
                                for m=1:d
                                    if l==m
                                        continue
                                    end
                                    if temp1(i,j,k,l)>temp1(i,j,k,m)
                                        p(1)=p(1)+1;
                                    end
                                    if temp2(i,j,k,l)>temp2(i,j,k,m)
                                        p(2)=p(2)+1;
                                    end
                                end

                                in1(i,j,k,l)=p(1);
                                in2(i,j,k,l)=p(2);
                            end
                        end
                    else
                        p=[0 0];
                        for l=1:c
                            if k==1
                                continue
                            end
                            if temp1(i,j,k)>temp1(i,j,l)
                                p(1)=p(1)+1;
                            end
                            if temp2(i,j,k)>temp2(i,j,l)
                                p(2)=p(2)+1;
                            end
                        end

                        in1(i,j,k)=p(1);
                        in2(i,j,k)=p(2);
                    end
                end
			end
		end
	end
end
									
								

for i=1:a
	for j=1:b
		if in1(i,j,1)~=mv && in2(i,j,1)~=mv && in1(i,j,1)~=-999 && in1(i,j,1)<9E9

			if c>1 && (d>1 || e>1)
				for k=1:c
					if d>1 && e>1
						for l=1:d
                            
                            if dd==1 && ee==1 && cc==e
                                R12=corrcoef(in1(i,j,k,l,:),in2(i,j,:));
                            else
                                R12=corrcoef(in1(i,j,k,l,:),in2(i,j,k,l,:));
                            end
                            
							out(i,j,k,l)=(R12(1,2))^2;
						end
					else
						R12=corrcoef(in1(i,j,k,:),in2(i,j,k,:));
						out(i,j,k)=R12(1,1);
					end
				end
			else
				R12=corrcoef(in1(i,j,:),in2(i,j,:));
			end
		else
			out(i,jk)=-999;
		end
	end
end

end           

function out=wood_grass(in,type) %#ok<DEFNU>

[a b c]=size(in); %#ok<NASGU>


if isequal(type,'WOOD') || isequal(type, 'Wood') || isequal(type,'wood')...
        || isequal(type,'W') || isequal(type,'w')
    sumc=1:7;
else
    if isequal(type,'GRASS') || isequal(type, 'Grass') ||...
            isequal(type,'grass') || isequal(type,'G') || isequal(type,'g')
        sumc=8:9;
    else
        error=char('grass/wood type not recignised') %#ok<NOPRT,NASGU>
    end
end

out=zeros(a,b,'single');

for i=1:a
    for j=1:b
        out(i,j)=sum(in(i,j,sumc));
    end
end

end

function out=find_peak_month(in)

[a b c d e]=size(in);

if d==1 && e==1
    out=zeros(a,b,'single');
else
    if e==1
        out=zeros(a,b,c,'single');
    else
        out=zeros(a,b,c,d,'single');
    end
end

threshold0=10;
threshold=threshold0;
for i=1:a
    if threshold<100*i/a
        percent=threshold %#ok<NOPRT,NASGU>
        threshold=threshold+threshold0;
    end
    for j=1:b
        if in(i,j,1,1)==-999
            out(i,j,:,:)=-999;
        else
            if d==1 && e==1
                if sum(in(i,j,:))==0
                    out(i,j)=0;
                else
                    [nn out(i,j)]=max(in(i,j,:));
                end
            else
                for k=1:c
                    if e==1
                        if sum(in(i,j,k,:))==0
                            out(i,j,k)=0;
                        else
                            [nn out(i,j,k)]=max(in(i,j,k,:));
                        end
                    else
                        for m=1:d
                            if sum(in(i,j,k,m,:))==0
                                out(i,j,k,m)=0;
                            else
                                [nn out(i,j,k,m)]=max(in(i,j,k,m,:));
                            end
                        end
                    end
                end
            end
        end
    end
end

for k=1:c
    for l=1:d
        for m=1:e
            if sum(sum(sum(in(:,:,k,l,:))))==0
                out(:,:,k,l)=0;
            end
        end
    end
end

end

function [mean,stdv]=standard_deviaton_cal(data,missing_value)

[a b c d e]=size(data);
size(data)
if d==1 && e==1
    mean=zeros(a,b,'single');
    stdv=zeros(a,b,'single');
else
    if e==1
        mean=zeros(a,b,c,'single');
        stdv=zeros(a,b,c,'single');
    else
        mean=zeros(a,b,c,d,'single');
        stdv=zeros(a,b,c,d,'single');
    end
end

threshold0=10;
threshold=threshold0;
for i=1:a
    
    if 100*i/a>threshold
        percent=threshold %#ok<NASGU,NOPRT>
        threshold=threshold+threshold0;
    end
    for j=1:b
        if d==1 && e==1
            if sum(data(i,j,:)==missing_value)>0
                mean(i,j)=missing_value;
                stdv(i,j)=0;
            else
%                 mean(i,j)=sum(data(i,j,:))/c;
                mean(i,j)=nanmean(data(i,j,:));
                stdv(i,j)=100*nanstd(data(i,j,:))/norm(mean(i,j));
            end
        else
            if e==1
                for k=1:c
                    if sum(data(i,j,k,:)==missing_value)>0
                        mean(i,j,k)=missing_value;
                        stdv(i,j,k)=0;
                    else
                        mean(i,j,k)=sum(data(i,j,k,:))/d;
                        stdv(i,j,k)=100*std(data(i,j,k,:))/norm(mean(i,j,k));
                    end
                end
            else
                for k=1:c
                    for m=1:d
                        if sum(data(i,j,k,m,:)==missing_value)>0
                            mean(i,j,k,m)=missing_value;
                            stdv(i,j,k,m)=0;
                        else
                            mean(i,j,k,m)=sum(data(i,j,k,m,:))/e;
                            stdv(i,j,k,m)=100*std(data(i,j,k,m,:))/norm(mean(i,j,k,m));
                           
                        end
                    end
                end
            end
        end                
    end
end

end

function plot_boundries(country_boundry,axisv,bsize)

[a b]=size(country_boundry); %#ok<NASGU>

p0=[1 1];
p=p0;
n=1;

percent=0;
threshold0=1;
threshold=threshold0;
PROGRESS='Placing and plotting boundries' %#ok<NOPRT,NASGU>
while n<=a
    testr=0;
    for i=n:a
        if threshold<100*i/a
            pause(0.01)
            percent=threshold %#ok<NOPRT,NASGU,NOPTS>
            threshold=threshold+threshold0;
        end
        if country_boundry(i,1)>p(1) || country_boundry(i,2)>p(2)
            break        
        end
        if country_boundry(i,4)>axisv(1) && country_boundry(i,4)<axisv(2)...
                && country_boundry(i,5)>axisv(3) && ...
                country_boundry(i,5)<axisv(4)
            testr=1;
        end
    end
    
    if testr==1
        plot(country_boundry(n:i-1,4),country_boundry(n:i-1,5),...
            'k','LineWidth',bsize);
    end
    
    if country_boundry(i,1)>p(1)
        p(1)=p(1)+1;
        p(2)=1;        
    else
        p(2)=p(2)+1;
    end        
    n=i;
    if n<a
        if percent>100-threshold0
            pause(0.01)
        end
    else
        break
    end
end
end

function out=refit_to_mask(data,landmask,frac)

[al bl]=size(landmask);

[ad bd cd]=size(data); %#ok<NASGU>



if round(al/ad)~=al/ad || ad/al~=bd/bl || ad>al
    al/ad, bl/bd %#ok<NOPRT>
	error=char('data and landmask sizes dont work nicely together') %#ok<NOPRT,NASGU>
end

out=zeros(al,bl,'single');

dn=al/ad; dl=180/al; dd=180/ad;

for i=1:al
	id=ceil((2*i-1)/(2*dn));
    if id>ad
        continue
    end
	if frac~=1
		latl=90-(i-0.5)*dl;
		latd=90-(id-0.5)*dd;
		ll=lat_size(latl,dl);
		ld=lat_size(latd,dd);
	end

	for j=1:bl
		jd=ceil((2*j-1)/(2*dn));
        if jd>bd
            continue
        end
		if frac==1
			out(i,j)=data(id,jd);
		else
			out(i,j)=data(id,jd)*ll/ld;
		end
	end
end

end

function out=make_mask_a_line(mask,lt)

[a b]=size(mask);

out=zeros(a,b,'single');

threshold0=10;
threshold=threshold0;
percent=0 %#ok<NOPRT,NASGU>
for i=1:a
    if threshold<100*i/a
        percent=threshold %#ok<NOPRT,NASGU>
        threshold=threshold+threshold0;
    end
    for j=1:b
        if mask(i,j)==999 || mask(i,j)==-999
            test=0;
            for i1=max(1,i-1):min(i+1,a)	
                for j1=max(1,j-1):min(j+1,b)
                    if mask(i1,j1)~=999 && mask(i1,j1)~=-999
%                         out(max(1,i-1):min(i+1,a),max(1,j-1):min(j+1,b))=-999;
%                         test=1;
%                         break
                        
                        test=test+1;
                    end
                end

%                 if test==1
%                     break
%                 end
            end
            if test<8 && test>=1
                out(max(1,i-floor((lt-1)/2)):min(i+ceil(((lt-1)/2)),a),max(1,j-floor((lt-1)/2)):min(j+ceil(((lt-1)/2)),b))=-999;
            end
        end
        
        if mask(i,j)~=999 && mask(i,j)~=-999
            out(i,j)=0;
        end
    end
end



end

function pdata=convert_data_to_numbers(data,landmask,icemask,maskline,lim,testo,ice_greys)
[a b c]=size(data); %#ok<NASGU>
c=max(size(lim));

pdata=zeros(a,b,'single');


threshold0=10;
threshold=threshold0;
percent=0 %#ok<NOPRT,NASGU>
size(data)
size(icemask)

for i=1:a
    if threshold<100*i/a
        percent=threshold %#ok<NOPRT,NASGU>
        threshold=threshold+threshold0;
    end
    for j=1:b
        if norm(double(data(i,j)))>1E10 || ...
                (testo==1 && norm(double(landmask(i,j)))==999)
            data(i,j)=-999;
        end
        
        if ice_greys==0 && icemask(i,j)==-999 || icemask(i,j)==999 || ...
                (isnan(data(i,j))==1 && landmask(i,j)~=-999)
            pdata(i,j)=-1;
        else
        if maskline(i,j)==-999
        	pdata(i,j)=-2;
        else
            if  data(i,j)==-999 %|| landmask(i,j)==-999 || landmask(i,j)==999
                pdata(i,j)=0;
            else

                    if data(i,j)>lim(c)
                        pdata(i,j)=c+1;
                    
                    else
                    
                        for k=1:c
                            if lim(k)<0
                                if data(i,j)<lim(k)
                                    pdata(i,j)=k;
                                    break
                                end
                            else
                                if lim(k)==0
                                    if k>1 && k<c
                                        if lim(k+1)~=0
                                            if data(i,j)<=lim(k)
                                                pdata(i,j)=k;
                                                break
                                            end
                                        else
                                            if data(i,j)<lim(k)
                                                pdata(i,j)=k;
                                                break
                                            end
                                        end
                                    else
                                        if k==1
                                            if data(i,j)<=lim(k)
                                                pdata(i,j)=k;
                                                
                                                
                                                break
                                            end
                                        else
                                            if data(i,j)<lim(k)
                                                pdata(i,j)=k;
                                                break
                                            end
                                        end
                                        
                                    end
                                else
                                
                                    if data(i,j)<=lim(k)
                                        pdata(i,j)=k;
                                        break
                                    end
                                end
                            end
 
                        end
                    end
            end
            
        end
        end
    end
end

end

function [out,colour,colour0,cname]=find_colour(in,cname,colour0,lim,tzero,colour_finder)

[a b]=size(cname); a=max([a b])-1;
if colour_finder==1
    [name,colour_choice]=colour_vectors;
     for i=1:max(size(colour_choice))
         TF = [strcmp(colour0(1), name(i)) strcmp(colour0(1),'white')];
         if TF(1)==1
             clear colour0
             colour0(1,:)=colour_choice(i,:);
             if TF(2)==0
                 tzero=1;
             end
         end
     end

    if a<=1
        [colour,cname(i:i+1)]=pick_colour(cname,colour0,lim,tzero);
    else
        [c d]=size(lim); c=max(c,d);


        colour(1:4+round(c/a),:)=pick_colour2(cname(1:2),colour0,lim(1:round(c/a)),tzero);
        for i=2:a
            colouri=pick_colour2(cname(i:i+1),colour0,...
                lim((i-1)*round(c/a)+1:min(max(size(lim)),i*round(c/a))),tzero);
            [d e]=size(colouri); %#ok<NASGU>
            %colour(5+(i-1)*round(c/a):4+i*round(c/a),:)=colouri(4+1:d,:);
            colour(5+(i-1)*round(c/a):5+(i-1)*round(c/a)+d-4-1,:)=colouri(4+1:d,:);
        end

    end
else
    colour=cname;
end

colouri=colour;
[a b]=size(colour);
colour=zeros(a+1,b,'single');
colour(2:a+1,:)=colouri;

[a b]=size(in);

out=zeros(a,b,3,'single');
threshold0=25;
threshold=threshold0;
for i=1:a
    if i/a>threshold
        percent=threshold %#ok<NASGU,NOPRT>
        threshold=threshold+threshold0;
    end
    for j=1:b
        
        out(i,j,:)=colour(in(i,j)+3,:);
    end
end

end

function [colour,name_colour]=pick_colour(name_colour,zcolour,lim,tzero)
[name,colour_choice]=colour_vectors;

[a b]=size(lim); a=max([a b])+1;
[c d]=size(name); c=max([c d]);

[d e]=size(name_colour); d=max([d e]);

if d==1    
    name_colour=[name_colour name_colour];
end


colour=zeros(a+2,3,'single');
colour(1:2,:)=[0.9 0.9 0.9 ;0.5 0.5 0.5];

find_test=0;
while find_test==0;
     for i=1:c
          TF = strcmp(name_colour(1), name(i));
         if TF==1
             scolour(1,:)=colour_choice(i,:);
             find_test=1;
             break
         end
     end

     if find_test==0
         error='colour not found' %#ok<NOPRT,NASGU>
         old_colour=name_colour(1) %#ok<NOPRT,NASGU>
         clear old_colour
         name_colour(1)={input('try another one:')};
     end
end

find_test=0;
while find_test==0;
     for i=1:c
         TF = strcmp(name_colour(2), name(i));
         if TF==1
             ecolour(1,:)=colour_choice(i,:);
             dcolour(1,:)=colour_choice(i,:)-scolour(1,:);
             find_test=1;
             break
         end
     end
     if find_test==0
         error='colour not found' %#ok<NOPRT,NASGU>
         old_colour=name_colour(2) %#ok<NOPRT,NASGU>
         clear old_colour
         name_colour(2)=input('try another one:');
     end
end
 
%  for i=1:c
%      TF = strcmp(colour0(1), name(i));
%      if TF==1
%          zcolour(1,:)=colour_choice(i,:);
%          tzero=1;
%      end
%  end

if tzero==1
    zp=0;
    for i=1:a
        if lim(i)==0
            if i==1
                zp=i;
            else
                zp=i+1;
            end
            break
        else
            if lim(i)>0
                zp=i;
                break
            end
        end
    end
end

for i=1:a
    if tzero==1

        if zp~=0
            if i<zp
                colour(i+2,:)=scolour+((i-1)/(zp-1))*(zcolour-scolour);
                if sum(zcolour)==2
                    [rgb,hsl]=rgb_hsl_and_back(colour(i+2,:),0);
    %                 hsl(3)=(zp-i)/zp;
                    hsl(3)=0.9*(i)/(zp-1);
                    colour(i+2,:)=rgb_hsl_and_back(0,hsl);
                end
%                 if scolour(1)==scolour(3) && scolour(1)~=1
%                     colour(i+2,1)=colour(i+2,3);
%                 end
%                         swop(1:2)=colour(i+2,1:2);
%                         colour(i+2,1:2)=swop(2:-1:1);
            else
                if i==zp
                    colour(i+2,:)=zcolour;
                else
                    colour(i+2,:)=zcolour+((i-zp)/(a-zp))*(ecolour-zcolour);
                    if sum(zcolour)==3
                        [rgb,hsl]=rgb_hsl_and_back(colour(i+2,:),0);
    %                     hsl(3)=0.25+0.75*((a-i)-zp)/(a-zp);
                        hsl(3)=0.9*(1+(zp+1-i)/(a-zp));
                        colour(i+2,:)=rgb_hsl_and_back(0,hsl);
                    end

                    if ecolour(1)==ecolour(3) %&& ecolour(1)~=1

                        colour(i+2,3)=colour(i+2,1);
                    end
                end
            end
        end
        
    else
        colour(i+2,:)=scolour+((i-1)/(a-1))*dcolour;
    end
    
   
end


[a b]=size(colour);

for i=1:a
    for j=1:b
        if colour(i,j)>1
            colour(i,j)=1;
        else
            if colour(i,j)<0
                colour(i,j)=0;
            end
        end
    end
end

% colour
% if ecolour(1)==ecolour(3)
%      colour(:,3)=colour(:,1);
% end
% colour
% stop
end

function colour=pick_colour2(name_colour,zcolour,lim,tzero)

[name,colour_choice]=colour_vectors;

[a b]=size(lim); a=max([a b])+1;
[c d]=size(name); c=max([c d]);

[d e]=size(name_colour); d=max([d e]);

if d==1    
    name_colour=[name_colour name_colour];
end

colour=zeros(a+3,3,'single');
colour(1:3,:)=[0.9 0.9 0.9 ;0.5 0.5 0.5;1 1 1];


 for i=1:c
%      name_colour(1)
     TF = strcmp(name_colour(1), name(i));
     if TF==1
         scolour(1,:)=colour_choice(i,:);
         break
     end
 end

 for i=1:c     
     TF = strcmp(name_colour(2), name(i));
     if TF==1
         %ecolour(1,:)=colour_choice(i,:);
     	 dcolour(1,:)=colour_choice(i,:)-scolour(1,:);         
         break
     end
 end
 
%  for i=1:c     
%      TF = strcmp(colour0(2), name(i));
%      if TF==1
%      	 zcolour(1,:)=colour_choice(i,:);  
%          tzero=1;
%          break
%      end
%  end


if tzero==1
    zp=0;
    for i=1:a
        if lim(i)==0
            zp=i;
            break
        else
            if lim(i)>0
                zp=i-0.5;
                break
            end
        end
    end
end

for i=1:a
    if tzero==1

        if zp~=0
            if i<zp
                colour(i+3,:)=scolour+((i-1)/(zp-1))*(zcolour-scolour);
            else
                if i==zp
                    colour(i+3,:)=zcolour;
                else
                    colour(i+3,:)=zcolour+((i-zp)/(a-zp))*(scolour-zcolour);
                end
            end
        end
    else
        colour(i+3,:)=scolour+((i-1)/(a-1))*dcolour;
    end
end

end

function [name,colour_choice]=colour_vectors
name={'red','orange','blue','purple','green','yellow', 'red_grey',...
    'blue_grey', 'brown','grey','turquoise','blue_turquoise','white',...
    'black','dark_red','lilac','dark_blue','dark_green','dark_brown',...
    'brown2','pink','pale_green','dark_blue/green','olive_green',...
    'pale_blue','lime_green','light purple','very light purple',...
    'magenta','dark_purple','black','PMIP ocean','PMIP ice',...
    'Uncertain biome','Tundra','Shrub-tundra','Desert',...
    'Dry grassland/shrubland','Boreal parkland','Temperate parkland',...
    'Sclerophyll woodland','Savanna','Boreal forest','Temperate forest',...
    'Warm-temperate forest','Tropical forest','cyan','light_blue',...
    'light_red','light_green'};

colour_choice(1,:)=[1 0.05 0.05];               %red
colour_choice(2,:)=[1 0.5 0.05];                %orange
colour_choice(3,:)=[0.05 0.05 1];               %blue
colour_choice(4,:)=[1 0.05 1];                  %purple
colour_choice(5,:)=[0.05 1 0.05];               %green
colour_choice(6,:)=[1 1 0.05];                  %yellow
colour_choice(7,:)=[1 .7 .7];                   %red_grey
colour_choice(8,:)=[0.7 .7 1];                  %blue_grey
colour_choice(9,:)=[1 0.75 0.05];               %brown
colour_choice(10,:)=[1 1 1];                    %grey
colour_choice(11,:)=[64/255 224/225 208/255];   %turquiose
colour_choice(12,:)=[64/255 224/225 1];         %blue_turquoise
colour_choice(13,:)=[1 1 1];                    %white
colour_choice(14,:)=[0 0 0];                    %black
colour_choice(15,:)=[0.7 0 0];                  %dark red
colour_choice(16,:)=[200 162 200]/255;          %lilac
colour_choice(17,:)=[0 0 0.5];                  %dark_blue
colour_choice(18,:)=[0 0.5 0];                  %dark_green
colour_choice(19,:)=[150 75 0]/255;             %dark_brown
colour_choice(20,:)=[200 125 50]/255;           %brown2
colour_choice(21,:)=[255, 192, 203]/255;     	%pink
colour_choice(22,:)=[0.8 1 0.8];                %pale_green
colour_choice(23,:)=[0 0.25 0.25];              %dark_blue/green
colour_choice(24,:)=[128 128 0]/255;            %olive green
colour_choice(25,:)=[0.8 0.8 1];                %pale blue
colour_choice(26,:)=[191 255 0]/255;            %lime_green
colour_choice(27,:)=[1 0.5 1];                  %light purple
colour_choice(28,:)=[1 0.75 1];                 %light purple
colour_choice(29,:)=[1 0 1];                    %magenta
colour_choice(30,:)=[0.5 0 0.5];                % dark purple
colour_choice(31,:)=[0 0 0];                    %black
colour_choice(32,:)=[211 211 211]/255;          %PMIP ocean
colour_choice(33,:)=[244 255 255]/255;          %PMIP ice
colour_choice(34,:)=[255 255 255]/255;          %Uncertain biome
colour_choice(35,:)=[255 204 204]/255;          %Tundra
colour_choice(36,:)=[204 204 255]/255;          %Shrub-tundra
colour_choice(37,:)=[255 255 51]/255;           %Desert
colour_choice(38,:)=[255 153 0]/255;            %Dry grassland/shrubland
colour_choice(39,:)=[102 255 255]/255;          %Boreal parkland
colour_choice(40,:)=[153 255 153]/255;          %Temperate parkland
colour_choice(41,:)=[153 153 51]/255;           %Sclerophyll woodland
colour_choice(42,:)=[204 102 0]/255;            %Savanna
colour_choice(43,:)=[0 0 153]/255;              %Boreal forest
colour_choice(44,:)=[0 204 0]/255;              %Temperate forest
colour_choice(45,:)=[51 102 102]/255;           %Warm-temperate forest
colour_choice(46,:)=[0 51 0]/255;               %Tropical forest
colour_choice(47,:)=[0 255 255]/255;            %Cyan
colour_choice(48,:)=[0.5 0.5 1];                %light blue
colour_choice(49,:)=[1 0.5 0.5];                %light red
colour_choice(50,:)=[0.5 1 0.5];                %light green

end

function data=alter_colour(data,stdv)

[a b]=size(stdv);
stdv=stdv-1;
maxs=max(max(stdv));


for i=1:a
    for j=1:b
        if stdv(i,j)>0 && sum(data(i,j,:))~=3
            [rgb,hsl]=rgb_hsl_and_back(data(i,j,:),0);
            
            hsl(2)=hsl(2)*(1-0.8*stdv(i,j)/maxs);
            
            data(i,j,:)=rgb_hsl_and_back(0,hsl);
            
        end
    end
end

end

function [rgb,hsl]=rgb_hsl_and_back(rgb,hsl)

if sum(rgb)>0
    hsl=zeros(3,1,'single');
    mrgb=zeros(2,1,'single');
    lrgb=zeros(2,1,'single');

    [mrgb(1) lrgb(1)]=max(rgb);
    [mrgb(2) lrgb(2)]=min(rgb);

    drgb=mrgb(1)-mrgb(2);
    srgb=mrgb(1)+mrgb(2);
    if drgb==0
        hsl(1)=0;
    else
        if lrgb(1)==1
            hsl(1)=360+60*(rgb(2)-rgb(3))/drgb;
            if hsl(1)>=360
                hsl(1)=hsl(1)-360;
            end

        else
            if lrgb==2
                hsl(1)=120+60*(rgb(3)-rgb(1))/drgb;
            else
                hsl(1)=240+60*(rgb(1)-rgb(2))/drgb;
            end
        end
    end

    hsl(3)=0.5*srgb;

    if srgb<=1
        hsl(2)=drgb/srgb;
    else
        hsl(2)=drgb/(2-srgb);
    end

    
else
    if sum(hsl)>0
        if hsl(3)<0.5
            q=hsl(3)*(1+hsl(2));
        else
            q=hsl(3)+hsl(2)-(hsl(3)*hsl(2));
        end

        p=2*hsl(3)-q;
        k=hsl(1)/360;

        t=zeros(3,1,'single');

        t(1)=k+1/3;
        t(2)=k;
        t(3)=k-1/3;

        for i=1:3
            if t(i)<0
                t(i)=t(i)+1;
            else
                if t(i)>1
                    t(i)=t(i)+1;
                end
            end

            if t(i)<1/6
                rgb(i)=p+((q-p)*6*t(i));
            else
                if t(i)<0.5
                    rgb(i)=q;
                else
                    if t(i)<2/3
                        rgb(i)=p+((q-p)*6*(2/3-t(i)));
                    else
                        rgb(i)=p;
                    end
                end
            end

        end
    else
        rgb=zeros(3,1,'single');
        hsl=zeros(3,1,'single');
    end
    
end


end

function [resize_text,figuren]=ploty(xdata1, ydata1, zdata1,lim,...
    colour,zero_colour,inf_colour,...
    tzero,labels,mtick,pwindow,figuren,colour_bar_test,tick_test,np,ip,...
    resize_text,resize_texts,xlabel,ylabel,height,label_size)
%CREATEFIGURE(XDATA1,YDATA1,ZDATA1)
%  XDATA1:  surface xdata
%  YDATA1:  surface ydata
%  ZDATA1:  surface zdata


xtick=[-90  90]; ytick=[-180 180];
xlabelc={'-90','90'}; ylabelc={'-180','180'};
[a b]=size(colour);

if a~=3 && b~=3
    error=char('wrong dimensions on colour array') %#ok<NOPRT,NASGU>
    stop
end

colouri=colour;
colour=[0];

colour=colouri(1:max(size(lim))+4,:);
[c nn]=size(colour); %#ok<NASGU>
cn=c;
p=4;
pl=1;
colourn=colour;
limn=lim;

testr=0;
[nn clm]=size(lim);

test_test=0;
for i=2:clm
    if isequal(lim(i-1),lim(i))==1
        test_test=1;
        break
    end
end

for i=5:cn
    
%     isequal(lim(i),lim(i-1))
    if i<cn && isequal(lim(i-3),lim(i-4))==1
        c=c-1;
        testr=1;
    else
        p=p+1;
        pl=pl+1;
        colourn(p,:)=colour(i,:);
        if i<cn
        limn(pl)=lim(i-3);
        end
    end
end

if testr==1
lim=zeros(1,1);
lim=limn(1:pl-1);

end

colour=zeros(p,3,'single');
colour(1:p,:)=colourn(1:p,:);
c=c-3; p=1;

colouring=zeros(64,3);
tickpt=zeros(c+1,1,'single');
tickpt(1)=0;
tickpt(c+1)=c;
slim=max(size(lim));

if iscell(inf_colour)
    si=1;
else
    if isnan(inf_colour)
        si=1;
    else
        si=1+0.5*64/slim;
        si=1+0.9*64/slim;
        si=round(si);
        for i=1:si-1
            colouring(i,:)=inf_colour;
        end
    end
end
test0=0;

for i=si:64
    colouring(i,:)=colour(p+3,:);
    if i/64>p/c
        tickpt(p+1)=c*i/64;
        p=p+1;
    end
    if test0==1
        colouring(i,:)=zero_colour;
        test0=2;
    end
    if p>2 && lim(p-1)==0 && tzero==1 && test0==0      
        colouring(i,:)=zero_colour;
        test0=1;
    end    


end


if tick_test==0
    if ip(1)~=1%np(1)
    ylabel=[];
    end
    if ip(2)~=np(2)
        xlabel=[];
    end
    if sum(size(xlabel))>1
        atck=max(size(xlabel));
        adt=max(size(xdata1));
        if xlabel(1)<xdata1(1)
            atck=atck-1;
            xlabel(1:atck)=xlabel(2:atck+1);
        end

        if xlabel(atck)>xdata1(adt)
            atck=atck-1;
        end
        xlabeli=xlabel;
        xlabel=zeros(1,1);
        xlabel(1:atck)=xlabeli(1:atck);
    end
    
    if sum(size(ylabel))>1
        atck=max(size(ylabel));
        adt=max(size(ydata1));

        if ylabel(1)<ydata1(1)
            atck=atck-1;
            ylabel(1:atck)=ylabel(2:atck+1);
        end

        if ylabel(atck)>ydata1(adt)
            atck=atck-1;
        end
        ylabeli=ylabel;
        ylabel=zeros(1,1);
        ylabel(1:atck)=ylabeli(1:atck);
    end
end

alb=max(size(xlabel));

if alb>0
    xtick=zeros(alb+2,1,'single');
    atck=max(size(xdata1));
    p=1;
    i=0;
    test_lin=1;
    xdata=xdata1(1);
    linear_value=xdata1(1);
    while p<=alb && i<=atck+10
        i=i+1;
        xdatao=xdata; %#ok<NASGU>
        linear_valueo=linear_value;
        if i>atck
            xdata=xdata1(atck)+(i-atck)*(xdata1(atck-1)-xdata1(atck-2));
        else
            xdata=xdata1(i);
        end
        if xdata>=xlabel(p)
            linear_value=xdata1(1)+(i-1)*(xdata1(atck)-xdata1(1))/(atck-1);
            if linear_value==xdata && test_lin==1
                xtick(p+1)=xlabel(p);

            else
                %proportion between found value and wanted value
                %value_mod=(xlabel(p)-xdatao)/(xdata-xdatao);
                value_mod=0.5;
                xtick(p+1)=linear_value-(linear_value-linear_valueo)*value_mod;
                test_lin=0;

            end
            p=p+1;
        end
    end
    gap=norm(xdata1(atck)-xdata1(1))+90*norm(xdata1(atck)-xdata1(1))/atck;
    xtick(1)=xdata1(1)-gap/180;%xtick(2)-(xtick(3)-xtick(2));
    xtick(alb+2)=xdata1(atck)+gap/180;%xtick(alb+1)+xtick(alb+1)-xtick(alb);

    xlabeli=xlabel;
    xticki=xtick;
    for i=2:alb
        if xlabel(i)==xlabel(i-1) && xlabel(i)==0
            xlabeli(i:alb-1)=xlabel(i+1:alb);
            xticki(i)=0.5*(sum(xtick(i:i+1)));
            xticki(i+1:alb+1)=xtick(i+2:alb+2);
            alb=alb-1;
            break
        end
    end
    xlabel=zeros(1,1,'single');
    xtick=zeros(1,1,'single');
    xlabel(1:alb,1)=xlabeli(1:alb);
    xtick(1:alb+2,1)=xticki(1:alb+2);


    xlabelc=cell(alb+2,1);
    if xlabel(1)==0
        xlabelc{1}=' ';
    else
        xlabelc{1}='<';
    end
    for i=1:alb
        xlabelc{i+1}=xlabel(i);
    end
    if xlabel(alb)==0
        xlabelc{alb+2}=' ';
    else
        xlabelc{alb+2}='>';
    end
end


alb=max(size(ylabel));
ytick=zeros(alb+2,1,'single');
atck=max(size(ydata1));
p=1;
i=0;
test_lin=1;

ydata=ydata1(1);
linear_value=ydata1(1);
linear_valueo=ydata1(1)+(-1)*(ydata1(atck)-ydata1(1))/(atck-1);

while p<=alb && i<=atck+10
    i=i+1;
    ydatao=ydata; %#ok<NASGU>
    linear_valueo=linear_value;
    if i>atck
        ydata=ydata1(atck)+(i-atck)*(ydata1(atck-1)-ydata1(atck-2));
    else
        ydata=ydata1(i);
    end
    if ydata>=ylabel(p)
        linear_value=ydata1(1)+(i-1)*(ydata1(atck)-ydata1(1))/(atck-1);
        if linear_value==ydata && test_lin==1
            ytick(p+1)=ylabel(p);
            
        else
            %proportion between found value and wanted value
            %value_mod=(ylabel(p)-ydatao)/(ydata-ydatao);
            value_mod=0.5;
            ytick(p+1)=linear_value-(linear_value-linear_valueo)*value_mod;
            test_lin=0;
            
        end
        p=p+1;
    end
end
gap=norm(ydata1(atck)-ydata1(1))+90*norm(ydata1(atck)-ydata1(1))/atck;
ytick(1)=ydata1(1)-gap/180;%xtick(2)-(xtick(3)-xtick(2));
ytick(alb+2)=ydata1(atck)+gap/180;%xtick(alb+1)+xtick(alb+1)-xtick(alb);

if isequal(ylabel,[])~=1
    ylabelc=cell(alb+2,1);
    if ylabel(1)==0
        ylabelc{1}=' ';
    else
        ylabelc{1}='\/';
    end
    for i=1:alb
        ylabelc{i+1}=ylabel(i);
    end
    if ylabel(alb)==0
        ylabelc{alb+2}=' ';
    else
        ylabelc{alb+2}='/\';
    end
end
% else
%     ylabel=[];
%     xlabel=[];
% end

if figuren==0
    positiond=[568*np(1) (0.8/height)*3*502*np(2)/4];
    resize_text=resize_texts;
    if positiond(2)>800
        resize_text=resize_text*800/positiond(2);
        positiond=positiond*800/positiond(2);
    end
    if positiond(1)>1200
        resize_text=resize_text*1200/positiond(1);
        positiond=positiond*1200/positiond(1);
    end
    figuren = figure('PaperSize',[20.98 29.68],...
        'Colormap',colouring,'OuterPosition',[50 0 positiond]);
end


if test_lin==1
    %test monotonically increasing
    ay=max(size(ylabel));
    for i=2:ay
        if ylabel(i)<ylabel(i-1)
            ylabel %#ok<NOPRT>
        	error('Error using ==> axes: Values must be monotonically increasing')
        end
    end
    ay=max(size(xlabel));
    for i=2:ay
        if xlabel(i)<xlabel(i-1)
            xlabel %#ok<NOPRT>
        	error('Error using ==> axes: Values must be monotonically increasing')
        end
    end
    axes1 = axes('Parent',figuren,...
        'Position',pwindow,...[0.05 0.2 0.9 0.6],...
        'YTick',ylabel,'XTick',xlabel,'CLim',[0 c],'FontSize',10*resize_text);
else
%     if xtick(1)>xtick(2)
%         xtick(1)=xtick(2)-(xtick(3)-xtick(2));
%     end
%     if ytick(1)>ytick(2)
%         ytick(1)=ytick(2)-(ytick(3)-ytick(2));
%     end
    xtick,ytick
    
    axtick=max(size(xtick)); aytick=max(size(ytick));
    for i=2:axtick
        if xtick(i)<xtick(i-1);
            xtick(i)=xtick(i-1);
        end
    end
    
    for i=1:aytick-1
        if ytick(i)>ytick(i+1);
            ytick(i)=(ytick(i+1)+ytick(max([1 i-1])))/2;
        end
    end
    %xtick(3:6)=[1000 2000 3000 3500];
%      ytick=[-1.4303 -1.0000  -0.4939 0.5092 1.5123 2.5154 3.5185 4.2320];
    axes1 = axes('Parent',figuren,...
        'Position',pwindow,'YTickLabel',ylabelc,'XTickLabel',xlabelc,...[0.05 0.2 0.9 0.6],...
        'YTick',xtick,'XTick',ytick,'CLim',[0 c],'FontSize',10*resize_text);
end



% Create axes

% axes1 = axes('Parent',figure1,...
%     'Position',[0.05 0.25 0.9 0.815],...
%     'Layer','top','DataAspectRatio',[1 1 1]);

hold('all');
grid('off');



image(xdata1,ydata1,zdata1,'Parent',axes1,'CDataMapping','scaled');


if isnumeric(labels)==1 && sum(sum(labels))==0
    labels={ '\/' ,lim,'/\'};
end
% labels={ '-' ,'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec',' '};
tickpt=tickpt+mtick;

% tickpt(1)=0.5; tickpt(2)=(tickpt(2)+1)/2; %delete
% tickpt(1)=0.5*(tickpt(2)+tickpt(1);
if colour_bar_test==1
    colorbar('Position',[0.87 0.1-0.4*(-1+1/max([1 .5*np(2)])) 0.05/np(1) 0.8/max([1 .5*np(2)])],'YTick',tickpt,...
        'YTickLabel',labels,'FontSize',10*resize_text*label_size);
end

end

function lat_sizea=lat_size(lat,dlat)

lat1=(lat+dlat/2)*pi/180;
lat2=(lat-dlat/2)*pi/180;

lat_sizea=norm(sin(lat1)-sin(lat2));

lat_sizea=(dlat/360)*2*lat_sizea*pi*6378.1370^2;

lat_sizea=norm(lat_sizea);%/1E6;
end

function [data,ncid]=open_netcdf_variable(ncid,filename,varname,start,count)

if ncid<3
    ncid = netcdf.open( filename, 'NC_NOWRITE' );
end

varid = netcdf.inqVarID(ncid,varname);
if sum(start)==0 && sum(count)==0
    data = netcdf.getVar(ncid,varid);
else
    if count==0
        data = netcdf.getVar(ncid,varid,start);
    else
        data = netcdf.getVar(ncid,varid,start,count);
    end
end
end