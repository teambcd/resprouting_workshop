quantile_colours <- function (choice=1,ncols=5) {
    if (choice==1) {
        cols=c('#CCCCCC',split_colours(startc="#FFB5B5",endc="#600000",ncols,hold='R'))
    }
    if (choice==2) {
        cols=split_colours(startc="#FFFFFF",endc="#FF0000",ncols+1)
        cols=c('#FFFFFF',"#FFD0BF","#FF9A86","#E04422","#C02200","#550000")
    }
    
    return(cols)
}

split_colours <- function(startc='#FFFFFF',endc='#000000',ncols=5,hold=NULL) {
    source("libs/install_and_source_library.r")
    install_and_source_library("colorspace")
    order=c('R','G','B')
    
    endc=col2rgb(endc)/255
    startc=col2rgb(startc)/255
  
    col <-matrix(0,ncols,3)
    
    for (i in 1:3) {
        if (is.null(hold)) {
            col[,i]=(seq(startc[i], endc[i], length = ncols))
        } else if(hold==order[i]) {
            col[,i]=c(rep(startc[i], ceiling(ncols/2)),
                      seq(startc[i], endc[i], length = (1+floor(ncols/2)))[-1])
        } else {
            col[,i]=c(seq(startc[i], endc[i], length = (1+ceiling(ncols/2)))[-1],
                      rep(endc[i], floor(ncols/2)))
        }
    }
    
    col=sRGB(col)
    return(hex(col))
}

pick_colours_based_on_limits <- function(TraitAbundance,limits,plot_density) {
    
    if (length(limits)==1) {
		ncols=limits
        plot_order <- cutN(TraitAbundance,N=ncols-1)
    } else {
        ncols=length(limits)+1
        plot_order=cutN(TraitAbundance,limits)        
    }
            
    if (plot_density) {
         colname <- quantile_colours(2,ncols)
    } else {
        colname <- quantile_colours(1,ncols)
    }
            
    return(list(colname,plot_order))
}

randomize_order_of_3 <- function(x,y,z) {
    i=1:length(x)
    
    i=sample(i)
    
    x=x[i]
    y=y[i]
    z=z[i]
    
    return(list(x,y,z))
}

colscale <- function(Abundance_quantile,colname){
      colname <-make.transparent(colname,.5)
      colors <- Abundance_quantile
      colors[colors==0] <- colname[1]
      colors[colors==1] <- colname[2]
      colors[colors==2] <- colname[3]
      colors[colors==3] <- colname[4]
      colors[colors==4] <- colname[5]
      colors[colors==5] <- colname[6]
      return(colors)
}
# function to classify the objects based on 5 quatiles
cut4<- function(x, ccut=NULL) {
       y <- x[x!=0]
       r <- sort(y)
       l <- length(y)
       z=rep(0,length(x))
       q0=0
       for (i in 1:4) {
          if (is.null(ccut)) {
              li<-round(l/5*i)
              r[li]->q1
          } else {
              q1 <- ccut[i]
          }
          test<- ((x>q0) + (x<=q1))==2
          z[test]=i
          q0=q1
        }
       test=x>q1
      z[test]=i+1
    
     return(z)
}

cutN<- function(x, ccut=NULL,N=NULL) {
    y <- x[x!=0]
    r <- sort(y)
    l <- length(y)
    z=rep(0,length(x))
    q0=0
    
    if (is.null(ccut)==FALSE && is.null(N)) {
        N=length(ccut)
    }
    if (is.null(N)) {
        N=4
    }
    
    for (i in 1:N) {
        if (is.null(ccut)) {
            li<-round(l/(N+1)*i)
            r[li]->q1
        } else {
            q1 <- ccut[i]
        }
        test<- ((x>q0) + (x<=q1))==2
        z[test]=i
        q0=q1
    }
    test=x>q1
    z[test]=i+1
  
    return(z)
}


make.transparent <- function(col, transparency) {
     ## Semitransparent colours
     tmp <- col2rgb(col)/255
     rgb(tmp[1,], tmp[2,], tmp[3,], alpha=1-transparency)
}

remove_3var_nans <-function(x,y,z) {

	test_nans=is.na(x)+is.na(y)+is.na(z)     
    x=x[(test_nans==0)]
    y=y[(test_nans==0)]
    z=z[(test_nans==0)]
	
	list(x=x,y=y,z=z)
}

remove_4var_nans <-function(x,y,z,a) {

	test_nans=is.na(x)+is.na(y)+is.na(z)+is.na(a)    
    x=x[(test_nans==0)]
    y=y[(test_nans==0)]
    z=z[(test_nans==0)]
    a=a[(test_nans==0)]
	
	list(x=x,y=y,z=z,a=a)
}

standard_polar_plot <-function(x,y,z,add=FALSE,
                               radial.lim=c(0,0.5,1),main="",bgfill=FALSE,
                               text_cex=1.5,cex_guides=0.2,season_label=TRUE,
                                ...) {
    source("libs/install_and_source_library.r")
    install_and_source_library("plotrix")
  	
    
    if (bgfill==FALSE) {
    	grid.bg='transparent'
    } else {
		grid.bg='grey'
	}
	cex.lab=par("cex.lab")
	par(cex.lab=text_cex*0.9)
    polar.plot( y,x,
              label.pos=c(0,90,180,270),
              labels=c('','','',''),
              radial.lim=radial.lim,boxed.radial=FALSE,
              radial.labels=radial.lim,
              start=90,clockwise=TRUE,rp.type='s',point.col=z,add=add,
              srt=0,grid.bg=grid.bg,
              grid.col=make.transparent('grey',1),
              point.symbols=19,...)
              
    polar.plot( rep(max(radial.lim),4),c(45,135,225,315),
              label.pos=0,labels='',
              radial.lim=radial.lim,boxed.radial=FALSE,
              radial.labels='',
              line.col=make.transparent('black',0.67),lty=3,lwd=cex_guides,
              start=90,clockwise=TRUE,rp.type='r',point.col=z,add=TRUE,
              srt=0,grid.bg=make.transparent('grey',1),
              grid.col=make.transparent('grey',1),...)
    par(cex.lab=cex.lab)
    for (i in seq(1,0,length=length(radial.lim))) {   
    	if (i==1) {
    		lty=1
    	} else {
    		lty=2
    	}      
		polar.plot( rep(max(radial.lim),1000)*i,seq(1,360,length=1000),
              label.pos=0,labels='',
              radial.lim=radial.lim,boxed.radial=FALSE,
              radial.labels='',
              line.col=make.transparent('black',0.67),lty=lty,lwd=cex_guides,
              start=90,clockwise=TRUE,rp.type='p',point.col=z,add=TRUE,
              srt=0,grid.bg=make.transparent('grey',1),
              grid.col=make.transparent('grey',1),...)
    }
              
              
              
    if (add==FALSE && season_label) {
        
        opos=max(radial.lim,na.rm=TRUE)*1.3
        mtext('Winter',cex=text_cex*par("cex"),srt=0,side=1,  xpd=TRUE)
        text(x=-opos,y=0,'Spring',cex=text_cex,srt=90, xpd=TRUE)
        text(x=0,y=opos, 'Summer',cex=text_cex,srt=0,  xpd=TRUE)
        text(x=opos,y=-0,'Autumn',cex=text_cex,srt=270,xpd=TRUE)
    }
    mtext(main,side = 1, line=2,cex=1.2)
}

standard_polar_legend <-function(cols,labelss=NULL,radial.lim=c(0,1),main="",bgfill=FALSE, 
	cex=1,main_text='',text_cex=1,tline=1,mar=c(0,0,0,0),...) {
	source("libs/install_and_source_library.r")
    install_and_source_library("plotrix")
    
	x=seq(0,360,length=length(cols)+1)[-(length(cols)+1)]
    y=rep(0.5,length(cols))
	cex=cex*3
    polar.legend <- function(x,y,cols,labelss='',add,cex=1,mar=c(0,0,0,0),...) {
    
    	polar.plot(y,x,
              label.pos=x,
              labels='',
              radial.lim=c(0,1),boxed.radial=FALSE,
              radial.labels='',
              start=90,clockwise=TRUE,rp.type='s',point.col=cols,add=add,
              srt=0,grid.bg='white',grid.col=make.transparent('white',1),
              point.symbols=19,cex=cex,mar=mar,...)
              
    	if (is.null(labelss)==FALSE) {
    		for (i in 1:length(labelss)) {
				
    			ang=2*pi*(i-1)/(length(labelss))
    			x=sin(ang)
    			y=cos(ang)
    			ang=-360*ang/(2*pi)
    			text(x,y,labelss[i],srt=ang,cex=text_cex)
    		}
    	}
    }
    
    polar.legend(x,y,'white',labelss,FALSE,cex=cex,mar=mar,...)
    polar.legend(x,y,'grey','',TRUE,cex=cex,mar=mar,...)
    polar.legend(0,0,'grey','',TRUE,cex=cex*2,mar=mar,...)
              
	for (tp in c(seq(1,2.5,length=5),rep(2.5,15))/seq(1,20,length=20)) {
    	polar.legend(x,y,cols,'',TRUE,cex=cex*tp,mar,...)
    }
    
    mtext(main_text,side=1,line=tline,cex=text_cex)
}

density_legend <- function( colname,limits,scex=1.5,
							units='',text_cex=1,mar=c(0,0,0,0),tline=1,circled=FALSE) {

	cols= make.transparent(colname,0.8)
            
    if (length(limits)==1) {
        labelss=c('0',paste('quantile',1:5))
    } else {
		labelss=c('0',paste(0,'-',limits[1]))
		for (li in 2:length(limits)) {
            labelss[li+1]=paste(limits[li-1],'-',limits[li])
        }
    	labelss[li+2]=paste(limits[li], '+')
    }
    labelss=paste(" ",labelss)
    
    cexsc=2*scex
       	    
    if (circled) {
    	standard_polar_legend(cols,cex=cexsc,labelss=labelss,bgfill=plot_density,
        	main_text=units,text_cex=text_cex,tline=tline,mar=mar)
    } else {
    	mar0=par("mar")
    	par(mar=mar)
    	plot(c(0,1),c(0,1),type='n',xlab='',ylab='',xaxt='n',yaxt='n')
    	legs=c("Zero\nAbundance",expression("1"^"st"),
    		expression("2"^"nd"),expression("3"^"rd"),expression("4"^"th"),
    		expression("5"^"th"))
    	legend(x="bottomleft",legend=legs,col='grey',pch=19,ncol=2,
    			pt.cex=cex*2.5,cex=text_cex*1.15,bty='n')
    	for (i in (1:20))
    		legend(x="bottomleft",legend=legs,col=cols,pch=19,bty='n',
    			ncol=2,pt.cex=cex*1.5/i,cex=text_cex*1.15,text.col="transparent")
    			
    	mtext("Quantiles",side=1,cex=text_cex,line=2)
    	par(mar=mar0)
    }
}     
    

order_based_on_order <-function(x,y,z,ord,i) {
    ord=as.vector(ord)
    ord=which(ord==i)
    x=x[ord]
    y=y[ord]
    z=z[ord]
    return(list(x,y,z))
}

maxround <-function(x) {
    x=max(x,na.rm=TRUE)
    x=ceiling(x*10)
    x=x/10
    return(x)
}

dev.off.all <- function() {
    test=0
    while (class(test)!="try-error") {
      test=try(dev.off(),silent=TRUE)
    }
}

draw.box <- function(lon_range,lat_range,lwd=5,...) {
	lines(	c(lon_range[1],lon_range[2],lon_range[2],lon_range[1],lon_range[1]),
    	  	c(lat_range[1],lat_range[1],lat_range[2],lat_range[2],lat_range[1]),
    		lwd=lwd,...)   				  	
}

range.positive <- function(rarray) {
	rarray[rarray<0]=NaN
	return(range(rarray,na.rm=TRUE))
}

arrow_legend <- function(scex=1,
						 a1=c(0.9,4),
						 a2=c(0.675,3.5),
						 a3=c(0.45,3),
						 a4=c(0.225,2.5),
						 a5=c(0.01,2),arrow_length=NULL,text_cex=1.5) {
	plot(c(0,4),c(-.5,4.5),type='n',xlab=' ',ylab=' ',axes=FALSE,pty="s",xpd=TRUE)
	
	arrow_fun <- function(y,alength,hlength,ltitle) {
		if (is.null(arrow_length)) {
			arrow_length=0.33+0.74*alength
		}
		arrows(x0=0.95-alength,x1=0.95,y0=y,y1=y,
			length=arrow_length,lwd=10*hlength,xpd=TRUE,code=2)
		text(1,y,ltitle,pos=4,xpd=TRUE,cex=text_cex)
	}
	
	arrow_fun(0,a1[1],a1[2],"P<0.00001") 
	arrow_fun(1,a2[1],a2[2],"P<0.001")
	arrow_fun(2,a3[1],a3[2],"P<0.05")
	arrow_fun(3,a4[1],a4[2],"P<0.1")
	if (a5[2]>0) {
		arrow_fun(4,a5[1],a5[2],"not significant")
	}
}