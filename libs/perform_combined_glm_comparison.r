perform_combined_glm_comparison <- function(abundance,climate,xname,yname,polar=FALSE,type='standard') {
	source("libs/return_multiple_from_functions.r")

	z=abundance
	x=climate[[xname]]
	y=climate[[yname]]
	if (polar || type=='polar') {
		if (xname==yname) {
			y[]=1
			xonly=TRUE
		} else {
			xonly=FALSE
		}
		c(p,g):=perform_combined_glm_polar_comparison(x,y,z,xonly)
	} else if (type=='standard') {
		glm_sum=summary(glm(z~x+y+x:y))
		glm_sum1=summary(glm(z~x))
		glm_sum2=summary(glm(z~y))
		glm_sum3=summary(glm(z~x:y))
		p	=glm_sum1$coefficients[2,4]
		p[2]=glm_sum2$coefficients[2,4]
		p[3]=glm_sum3$coefficients[2,4]
		#p=glm_sum$coefficients[2:4,4]
		
		g	=glm_sum1$coefficients[2,1]
		g[2]=glm_sum2$coefficients[2,1]
		g[3]=glm_sum3$coefficients[2,1]
	} else if (type=='rotate') {
		c(p,g):=perform_combined_glm_rotation_comparison(x,y,z)
	}

	return(list(p,g))
}

perform_combined_glm_polar_comparison <- function(x,y,z,xonly=FALSE) {
	
	xi=y*cos(x*2*pi/360)
	y=y*sin(x*2*pi/360)
	x=xi
	if (xonly) {
		glm_sum=summary(glm(z~x))
	} else {
		glm_sum=summary(glm(z~x+y+0))
	}
	p=glm_sum$coefficients[,4]
	g=glm_sum$coefficients[,1]
	
	return(list(p,g))
}

perform_combined_glm_rotation_comparison <- function(x,y,z) {
	source("libs/atans.r")
	r=sqrt(x^2+y^2)
	theta0=atans(y,x)

	
	c(p0,g):=glm_from_r_and_theta(0,theta0,r,z,yonly=FALSE)

	theta=atans(g[1],g[2])
	
	c(p,nn):=glm_from_r_and_theta(theta,theta0,r,z,yonly=TRUE)
	c(nn,g):=glm_from_x_and_y(y,x,z,seperate=TRUE)
	return(list(p,g))
	
}

glm_from_x_and_y <- function(x,y,z,seperate=FALSE) {
	if (seperate) {

		glm_sum1=summary(glm(z~x))
		glm_sum2=summary(glm(z~y))
		g=glm_sum1$coefficients[2,1]
		g[2]=glm_sum2$coefficients[2,1]
		p=glm_sum1$coefficients[2,4]
		p[2]=glm_sum2$coefficients[2,4]
		
		return(list(p,g))
		
	} else {
		glm_sum=summary(glm(z~x+y))
		g=glm_sum$coefficients[,1]
		p=glm_sum$coefficients[,4]
		return(list(p[-1],g[-1]))
	}

}


glm_from_r_and_theta <- function(theta,theta0,r,z,yonly=FALSE) {
	theta=theta0+theta
	x=r*cos(theta)
	y=r*sin(theta)

	if (yonly) {
		glm_sum=summary(glm(z~y))
	} else {
		glm_sum=summary(glm(z~x+y))
	}
	g=glm_sum$coefficients[,1]
	p=glm_sum$coefficients[,4]

	return(list(p[-1],g[-1]))

}