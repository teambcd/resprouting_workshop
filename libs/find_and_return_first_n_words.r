find_and_return_first_n_words <- function(string,n=2) {
	library("stringr")
	regular_expersion='^\\w+'
	if (n>1) {
		for (i in 2:n) {
			regular_expersion=paste(regular_expersion,' \\w+',sep="")
		}
	}
	
	string=str_extract(string,regular_expersion)
	
	return(string)
}

find_and_return_first_n_words_scaler <-function(string) {

	word1=first.word(string)
	string=gsub("^.*? ","",string)
	word2=first.word(string)
	
	string=paste(word1,word2,sep=" ")	
	return(string)
}
	
	