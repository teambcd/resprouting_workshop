function phase=atans(x,y)

phase=atan(x./y);

test=(x>0)+(y<0);
phase(find(test==2))=pi+phase(find(test==2));

test=(x==0)+(y<0);
phase(find(test==2))=pi;

test=(x<0)+(y<0);
phase(find(test==2))=-pi+phase(find(test==2));

test=(x<0)+(y==0);
phase(find(test==2))=-pi/2;

phase=6*(phase/pi)+6;

end