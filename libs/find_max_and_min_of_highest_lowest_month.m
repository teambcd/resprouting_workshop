function mdata=find_max_and_min_of_highest_lowest_month...
    (data,missing_value)

    [~,~,c]=size(data);
    
    mdata1=min(data,[],3);
	mdata2=max(data,[],3);
    
    for i=1:c
        mdata1(find(data(:,:,i)==missing_value))=0;
        mdata1(isnan(data(:,:,i)))=0;
        
        mdata2(find(data(:,:,i)==missing_value))=0;
        mdata2(isnan(data(:,:,i)))=0;        
    end
    
    mdata(:,:,1)=mdata1;
    clear mdata1
    mdata(:,:,2)=mdata2;
    
end