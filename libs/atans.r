atans <- function(x,y) {
	phase=atan(x/y);
	
	test=((x>0)+(y<0)==2);
	phase[test]=pi+phase[test];

	test=((x==0)+(y<0))==2;
	phase[test]=pi;

	test=((x<0)+(y<0))==2;
	phase[test]=-pi+phase[test];

	test=((x<0)+(y==0))==2;
	phase[test]=-pi/2;
	
	return(phase)
}