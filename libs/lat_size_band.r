lat_size_band <- function(lat,dlon) {
	size_lat=abs(cos(lat*2*pi/360))

	size_lat=size_lat*40075000*dlon/360
  
  	size_lat=abs(size_lat)
  
  	return(size_lat)
}