load_seasonal_netcdf_vars <-function(filename,varname,average=FALSE,
  annual=TRUE,seasonal=TRUE,max_min=TRUE,negative=FALSE)
{
    
	source("libs/find_annual_average.r")
    source("libs/find_seasonal_concentration_and_phase.r")
    source("libs/find_max_and_min_of_highest_lowest_month.r")
    library("raster")
    
    print("opening netcdf")
    cdata=brick(filename,varname)
    
	if (negative) {
		cdata=max(cdata)-cdata
	}
    
    annual_average=NaN
    phase=NaN
    conc=NaN 
    minv=NaN
    maxv=NaN   
    
    if (annual) {
      print("calcuating annual average")
      annual_average=find_annual_average(cdata,average)
	  
    }
    
    if (seasonal) {
      print("calculating seasonal values")
      sdata=find_seasonal_concentration_and_phase(cdata)
	  phase=sdata[[1]]
	  conc=sdata[[2]]
    }
    
    if(max_min)   {
      print("calculating mins and maxs")
      mdata=find_max_and_min_of_highest_lowest_month(cdata)
	  minv=mdata[[1]]
	  maxv=mdata[[2]]
    }
    
    list(annual_average=annual_average,
		phase=phase,conc=conc,
		minv=minv,maxv=maxv)
		
    
}
