find_seasonal_concentration_and_phase <- function(cdata,phase_units="degrees")
{
    library("raster")
    a=dim(cdata)
    
    xdata=cdata[[1]]
    xdata[,]=0
    ydata=xdata
    adata=xdata
    
    for (k in 1:a[3]) {
        angle=2*pi*(12-k+1)/12
        xdata=xdata+cdata[[k]]*cos(angle)
        ydata=ydata+cdata[[k]]*sin(angle)
        adata=adata+cdata[[k]]
    }
    
    pdata=atans(0-ydata,0-xdata,phase_units)
    cdata=sqrt(xdata^2+ydata^2)/adata
    list(pdata,cdata)
}

find_seasonal_concentration_and_phase_vector <- function(mval,phase_units="degrees") {
	
	xdata=rep(0,dim(mval)[1])
	ydata=xdata
	adata=xdata
	for (k in 1:12) {
		angle=2*pi*(12-k+1)/12
		xdata=xdata+mval[,k]*cos(angle)
		ydata=ydata+mval[,k]*sin(angle)
		adata=adata+mval[,k]
	}
	pdata=atans(0-ydata,0-xdata,phase_units)
	cdata=sqrt(xdata^2+ydata^2)/adata
	return(list(pdata,cdata))
}

atans <- function(x,y,units) {
    if (class(x)=="RasterLayer") phase_out=x[[1]]
    
    if (class(x)=="RasterLayer") x=as.matrix(x)
    if (class(y)=="RasterLayer") y=as.matrix(y)
    
    phase=atan(x/y)
    
    test=(x>0)+(y<0);
    phase[which(test==2)]=pi+phase[which(test==2)];

    test=(x==0)+(y<0);
    phase[which(test==2)]=pi;

    test=(x<0)+(y<0);
    phase[which(test==2)]=-pi+phase[which(test==2)];

    test=(x<0)+(y==0);
    phase[which(test==2)]=-pi/2;

    if (units=='months') {
      phase=6*(phase/pi)+6;
    } else if (units=='degrees') { 
      phase=phase+pi
      phase=phase*360/(2*pi)
    }
            
    if (class(x)=="RasterLayer") {
    	values(phase_out)=phase
    	return(phase_out)
    } else {
    	return(phase)
    }
}
