function open_and_plot_annual_climate_data(filename,plot_names,varname,...
    limits,colours,missing_value)
        
    addpath ../libs/export_fig
    load mask2
    load Aus_land_shape
    
    
    adata=open_netcdf_variable(0,filename,varname,0,0);
    adata = flipdim(adata,2);
    adata(find(adata==missing_value))=0;    
    adata(find(adata<-9E9))=0;
    
    lat=open_netcdf_variable(0,filename,'latitude',0,0);
    lon=open_netcdf_variable(0,filename,'longitude',0,0);

    rlat=[min(lat) max(lat) abs(lat(2)-lat(1))];
    rlat(1)=rlat(1)-2.5*rlat(3);

    rlon=[min(lon) max(lon) abs(lon(2)-lon(1))];
    rlon(2)=rlon(2)+5*rlon(3);

    name=[plot_names '-annual_average.tif'];
    plot_with_lims_dres_cline11(rlat,rlon,...
        adata,'same window',...
        'colour',colours,...
        'limits',limits,...
        'line thickness',2.5,...
        'boundry lines',landmask_vector,...
        'xtick',[0],'ytick',[0],...
        'turn off find missing values',...
        'boundry line width',1,'axis',[100 160 -45 -10]) 
    
    export_fig(name,'-transparent')
        
end