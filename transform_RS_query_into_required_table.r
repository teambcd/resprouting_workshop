script_info <- function () {
	
	## define table inputs
	filename_plants_abundances <<- "data/Single-stemmed tree.csv"	
	filename_plants_species <<- "data/species_count.csv"	
	filename_climate        <<- 'outputs/Site_bioclimate.csv'
    
	filename_plants_abundances <<- "data/Total abundance by site and attribute without grass.txt" 
	filename_plants_species <<- "data/Number of species by traits without grass.csv"    
	filename_climate        <<- 'outputs/Site_bioclimate.csv'
	  
    
    ## define column headers
    header_BBscore          <<- 'SumOfAbundance.score'
    header_cover            <<- 'SumOf..Cover'
    header_ncover           <<- 'SumOfNormalised.cover'
    header_pc_species       <<- 'percentage'
    header_no_species       <<- 'count'
    
    ## define what want outputting
    BBscore                 <<- TRUE
    coverT                  <<- TRUE
    ncover                  <<- TRUE
    pc_species              <<- TRUE
    no_species              <<- TRUE
    
    ## define groups
    allow_repeats           <<- TRUE
    groups                  <<- list(
        Arial.RS            = c("Apical.resprouter","Epicormic.resprouter",
        						"Basal.collar.resprouter"),
        Other.RS            = c("Underground.resprouter","Seeder"),
        None.RS             = c("Non.resprouter"))
    groups          <<- list(
        Resprouter      = c("Resprouter"),
        Apical          = c("Apical.resprouter"),
        Epicormic       = c("Epicormic.resprouter"),
        Basal.or.Collar = c("Basal.collar.resprouter"),
        Underground     = c("Underground.resprouter"),
        Non.Resporouter = c("Non.resprouter"),
        Seeder          = c('Seeder'))
	
	## define output table
	filename_out_BBscore    <<- 'outputs/Site_RS_abundance_no_grass.csv'
	filename_out_cover      <<- 'outputs/Site_RS_cover_no_grass.csv'
	filename_out_ncover     <<- 'outputs/Site_RS_ncover_no_grass.csv'
	filename_out_pc_species <<- 'outputs/Site_RS_pc_of_species_no_grass.csv'
	filename_out_no_species <<- 'outputs/Site_RS_no_of_species_no_grass.csv'
	

}

transform_RS_query_into_required_table <- function() {
	
	## contain required information t run these functions##
    script_info()
    
    if (BBscore) {
        gubbins(filename_plants_abundances,filename_out_BBscore,
            header_BBscore)
    }
    if (coverT) {
        gubbins(filename_plants_abundances,filename_out_cover,
            header_cover)
    }
    if (ncover) {
        gubbins(filename_plants_abundances,filename_out_ncover,
            header_ncover)
    }
    if (pc_species) {
        gubbins(filename_plants_species,filename_out_pc_species,
            header_pc_species)
    }
    if (no_species) {
        gubbins(filename_plants_species,filename_out_no_species,
            header_no_species)
    }
    
}

gubbins <- function(filename_plants,filename_out,header) {
	## load plant breadown and climate tables
	pdata <<- read.csv(filename_plants)

    assigned <<- rep(FALSE,dim(pdata)[1])
    
	cdata=read.csv(filename_climate)
	
	#find unique site IDs and new site locations
	a=dim(pdata)
	SiteIDp=pdata[,1]
	
	index_new=c((SiteIDp[2:a[1]]!=SiteIDp[1:a[1]-1]),TRUE)
	index_new=which(index_new)
	
	SiteID=SiteIDp[index_new]
	
	## find climates ##
	indexs=match(SiteID,cdata$SiteID)

	## find resprouter info
	rdata=find_RS_abundance(index_new,header)	
	## define outputs table
    outputs=data.frame(
        SiteID,
        lat=pdata$Latitude..decimal.[index_new],
        lon=pdata$Longitude..decimal.[index_new],
        MAT=cdata$MAT[indexs],
        MTWA=cdata$MTWA[indexs],
        MTCO=cdata$MTCO[indexs],
        Tdry=cdata$Tdry[indexs],
        GDD0=cdata$GDD0[indexs],
        MAP=cdata$MAP[indexs],
        MPW=cdata$MPW[indexs],
        MPD=cdata$MPD[indexs],
        PC=cdata$PC[indexs],
        PP=cdata$PP[indexs],
        IAVpr=cdata$IAVpr[indexs],
        DP=cdata$DP[indexs],
        alpha=cdata$alpha[indexs],
        alpha_max=cdata$alpha_max[indexs],
        alpha_min=cdata$alpha_min[indexs],
        alpha_phase=cdata$alpha_phase[indexs],
        alpha_conc=cdata$alpha_conc[indexs],
        MI=cdata$MI[indexs],
        fapar=cdata$fapar[indexs],
        fapar_max=cdata$fapar_max[indexs],
        fapar_min=cdata$fapar_min[indexs],
        faparC=cdata$faparC[indexs],
        faparP=cdata$faparP[indexs],
        PAR0=cdata$PAR0[indexs],
        NPP=cdata$NPP[indexs])
        
    for (i in 1:length(groups)) {
        outputs[names(groups)[i]] <- rdata[[i]][,1] # That creates the new column named "MY_NEW_COLUMN" filled with "NA"
        #outputs$(names(groups)[i]) <- rdata[[i]][,1]
    }
		
	## write outputs ##
	write.csv(outputs,filename_out,row.names=FALSE)
}

find_RS_abundance <- function(index_new,header) {
		
	index_new=c(1,index_new)
	
	a=length(index_new)	
	
    g=1:length(groups)
    rdata=lapply(g,find_RS_abundance_group,index_new,header)  
    return(rdata)

	
}

find_RS_abundance_group <- function(g,index_new,header) {

    a=length(index_new)	
    Rs=matrix(0,a-1)

    for (i in 1:(a-1)) {
		si=index_new[i]+1
		ei=index_new[i+1]
		
		Rs[i]=find_site_abundance_for_ind_type(
            groups[[g]],si,ei,header)			
		
	}
    return(Rs)
}


find_site_abundance_for_ind_type <- function(colname,si,ei,header)
{
    out_data=pdata
    nss=ei+1-si
    i=rep(0,nss)
    for (j in colname) {
        add=(pdata[[j]][si:ei]==1)
        add[is.na(add)]=0
        i=i+add
        
	}
    i[i>1]=1
    if (allow_repeats==FALSE) {
        i[assigned[si:ei]]=0
        assigned[si:ei][i==1]   <<- TRUE        
    }
    out_data=sum(i*pdata[[header]][si:ei])
    
	return(out_data)
}