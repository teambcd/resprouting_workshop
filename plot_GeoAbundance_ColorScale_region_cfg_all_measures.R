##############################################################################
## script set up                                                           ##
##############################################################################
## libraries ##
source("plotting/plot_topomaps_with_regions.R")

## define inputs ##  
Filenames_in 	<<- c(	'outputs/Site_RS_abundance.csv',
                  	  	'outputs/Site_RS_cover.csv',
                	  	'outputs/Site_RS_ncover.csv',
                  	  	'outputs/Site_RS_no_of_species.csv',
                  	  	'outputs/Site_RS_pc_of_species.csv')

Filenames_out 	<<- c(	'geoplotAbundance_colScale.png',
                      	'geoplotCover_colScale.png',
                   	  	'geoplotnCover_colScale.png',
                   	  	'geoplotSpecies_no_colScale.png',
	                   	'geoplotSpecies_pc_colScale.png')


## define plots outputs ##
fig_path 		<<- 	'figs/'

     
## which traits need plotting?
traits			<<- c(	'Resprouter','Apical','Epicormic','Basal.or.Collar','Underground')     
## limits of file 1 for each attrbite
## Abundances
limits1			<- list(5, # Resprouter
             			5,          # Aprical
             			5,          # Epicormic
             			5,          # Basal.or.Collar
             			5)          # Underground

## Cover
limits2			<- list(5, # Resprouter
             			5,          # Aprical
             			5,          # Epicormic
             			5,          # Basal.or.Collar
             			5)          # Underground

## nCover
limits3			<- list(5, # Resprouter
             			5,          # Aprical
             			5,          # Epicormic
             			5,          # Basal.or.Collar
             			5)          # Underground

## No of species
limits_type		<- c(	1,5,10,15,20,25,30,35)
limits4			<- list(c(10,20,30,40,50), # Resprouter
             			limits_type,          # Aprical
             			limits_type,          # Epicormic
             			limits_type,          # Basal.or.Collar
             			limits_type)          # Underground

## PC of species
limits5			<- list(5, # Resprouter
             			5,          # Aprical
             			5,          # Epicormic
             			5,          # Basal.or.Collar
             			5)          # Underground

limits_all		<<-list(limits1,limits2,limits3,limits4,limits5)
units_label		<<-	c(	'','','','no. of species','')
col_choices		<<- c( 'red','blue','red','red','red','red')

nplots_across 	<<- 	2
top_blank 		<<- 	TRUE

    
# choose the colors here to indicate five levels of abundance for traits
plot_random 	<<- TRUE
plot_density 	<<- TRUE
cex_scaling		<<- 1
test_draw_box	<<- TRUE

desaturate_map_colours <<- TRUE
test_abundance_gradient <<- TRUE

plot_topomaps_with_regions()