source("plotting/plot_ClimateMatrix_FocusSites.R")

filename_plot	<<- 'figs/climate_space2'

xy_held			<<-FALSE

## Define what climate variables want plotting and how they should be labelled
xClimVars 		<<- matrix(c('faparP','DP'),2,1)
yClimVars 		<<- matrix(c('faparC','PC'),2,1)
ClimVars_labs	<<-matrix(c(expression(paste("fAPAR"[Phi]," vs. ","fAPAR"[Theta],sep="")),
				 		    expression(paste("Dry"[Phi]," vs. ","Pr"[Theta],sep=""))),2,1)

## All of these are polar				 		    
xClimVars_polar <<- matrix(c(TRUE,TRUE),2,1)
cex_general		<<- 1.33

oma				<<- c(6, 14, 12, 6)
plot_ClimateMatrix_FocusSites()